rootFolder = 'C:\Users\student\3dpose\output';
mkdir(rootFolder)

numJoints = 18;
net=vl_simplenn_move(net,'gpu');
bodyParts = {...
    'Chest','LFoot','RFoot','LElbow','RElbow',...
    'LWrist','RWrist','Head','Bellybutton','LFingers',...
    'RFingers','Neck','LKnee','RKnee','LHip',...
    'RHip','LShoulder','RShoulder'}';
bodyPartIDs = 1:18;

iView = 1;
iModel = 1;
iSeq = 1;
viewNames = {'angle_000','angle_036','angle_072','man_tall_muscular','man_very_fat','woman_lean','woman_lean_tall','woman_short_fat'};
modelNames = {'man_fat','man_lean','man_short_skinny','man_tall_muscular','man_very_fat','woman_lean','woman_lean_tall','woman_short_fat'};
sequenceNames = {'02_06','13_30','15_12','26_07','28_01','28_15','49_02'};
testSequences_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{iSeq}));
testSamples_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,modelNames{iModel})) & ...
        testSequences_bool;
testSamples_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,viewNames{iView})) & ...
        testSamples_bool;

% Define test set
testIndices = find(testSamples_bool);

iFrame = 1;
% Generate estimations, store to file
for iSample = testIndices
    im = imdb.images.data(:,:,:,iSample);
    lbl = imdb.images.labels(:,:,:,iSample);
    lbl = cat(3,lbl,ones(1,1,130-54,'single'));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_f(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', false, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x(1:54)));   
    bbox = imdb.meta.bbox(iSample,:);
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
    joints = joints.*boxMultiplier;
    boxOffset = repmat([bbox(1);bbox(3);bbox(5)],numJoints,1);
    joints = joints+boxOffset;
    % scale in meters
    joints = joints / 1000;
    % reshape
    joints = reshape(joints,3,[]);
    
    filename = sprintf('%s%sestimatedJoints_%s_%s_%s_%06d.txt',rootFolder,filesep,viewNames{iView},sequenceNames{iSeq},modelNames{iModel},iFrame);
    fID = fopen(filename,'w');
    fprintf(fID,'%d\n',numJoints);
    for iJ = bodyPartIDs
        fprintf(fID,'%s %f %f %f\n',bodyParts{iJ},joints(1,iJ),joints(2,iJ),joints(3,iJ));
    end
    fclose(fID);
    
    iFrame = iFrame+1;

end


