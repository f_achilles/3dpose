numJoints = 17;
net=vl_simplenn_move(net,'gpu');
makeMovie = false;
% Define test set
testIndices = find(imdb.images.set == 2);
poseFig = figure('Position',[50 50 800 600]);
[az,el]=view(3);
ratio = 2;
if makeMovie
    movObj = VideoWriter('3DposeSynth_demo.avi');
    open(movObj)
end
for iSample = testIndices
    im = imdb.images.data(:,:,:,iSample);
%     lbl = imdb.images.labelsOverParam(:,:,:,iSample);
    lbl = imdb.images.labels(:,:,:,iSample);
%     lbl = cat(3,lbl,ones(1,1,130-54,'single'));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_f(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', false, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x(1:(3*numJoints))));
    GTjoints = squeeze(lbl(1:(3*numJoints)));
    estSParams = squeeze(gather(res(end-1).x(3*numJoints+(1:18))));
    GTSParams = squeeze(lbl(3*numJoints+(1:18)));
    disp(sum(abs(estSParams-GTSParams))/18)
    % depth map
    subplot(1,2,1)
    imagesc(im(:,:,3),[0 1]);
    axis image
    title('depth map')
    % ground truth pose
    subplot(1,2,2,'align')
    
    bbox = imdb.meta.bbox(iSample,:);
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
    GTjoints = GTjoints.*boxMultiplier;
    
    GTjoints = reshape(GTjoints,3,[]);
%     drawEvalSkel(reshape(GTjoints',[],1),'*g','3d','Synth');
    plot3(GTjoints(1:3:end),GTjoints(3:3:end),1-GTjoints(2:3:end),'g*')
    GTjoints = reshape(GTjoints,[],1);
    text(double(GTjoints(1:3:end)),double(GTjoints(3:3:end)),double(1-GTjoints(2:3:end))...
        ,num2str((1:numJoints)'));
%     axis image vis3d
%     view(2)
    title('ground truth pose (green) vs. estimated pose (blue)')
    % estimated pose
    subplot(1,2,2)
    joints = joints.*boxMultiplier;
    joints = reshape(joints,3,[]);
    hold on
%     drawEvalSkel(reshape(joints',[],1),'*b','3d','Synth');
    plot3(joints(1:3:end),joints(3:3:end),1-joints(2:3:end),'b*')
    joints = reshape(joints,[],1);
%     text(double(joints(1:3:end)),double(joints(2:3:end)),double(joints(3:3:end))...
%         ,num2str((1:20)'));
    hold off
    
%     XZYratio = dY/dX;
%     % adjust aspect ratio for axis lengths
%     if XZYratio<ratio %deltaY too short
%         increase = ratio*deltaX-deltaY;
%         minY = minY-increase/2;
%         maxY = maxY+increase/2;
%     elseif XZYratio>ratio %deltaX too short
%         increase = deltaY/ratio-deltaX;
%         minX = minX-increase/2;
%         maxX = maxX+increase/2;
%     else
%         % nothing
%     end
%     axis vis3d
%     axis equal
%     axis([-100 1200 -100 900 -1800 100])
    axis([0 dX 0 dZ -dY 0])
    
%     view(az+iSample, el);
    view([0,-1,0])
    set(gca,'units','pixels','position',[425 100 300 400])
%     axis off
    drawnow
pause(0.1)
if makeMovie
    writeVideo(movObj, getframe(poseFig))
end

end
if makeMovie
    close(movObj)
end

