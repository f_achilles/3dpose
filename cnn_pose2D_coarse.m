function [net, info] = cnn_pose2D_coarse(imdb)

opts.dataDir = fullfile('C:\Projects\poseFromRGBD\data\2DposeCoarse') ;
opts.expDir = fullfile('C:\Projects\poseFromRGBD\data\2DposeCoarse') ;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 100 ;
opts.train.numEpochs = 350 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
opts.train.learningRate = 0.00001 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
% opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------


f=1/100 ;


net.layers = {} ;
%20, 10
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(20,10,1,512, 'single'), ...
                           'biases', zeros(1, 512, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,512,1024, 'single'), ...
                           'biases', zeros(1, 1024, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,1024,512, 'single'), ...
                           'biases', zeros(1, 512, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;

% net.layers{end+1} = struct('type', 'conv', ...
%                            'filters', f*randn(1,1,1024,512, 'single'), ...
%                            'biases', zeros(1, 512, 'single'), ...
%                            'stride', 1, ...
%                            'pad', 0) ;
% net.layers{end+1} = struct('type', 'relu') ;

net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,512,40, 'single'), ...
                           'biases', zeros(1, 40, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;

net.layers{end+1} = struct('type', 'euclid') ;


% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

[net, info] = cnn_train_scnseg(net, imdb, @getBatch, ...
    opts.train) ;

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(:,:,:,batch) ;
