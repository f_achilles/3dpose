%% compute 1024-element feature vectors
modelNames = {'man_fat','man_lean','man_short_skinny','man_tall_muscular','man_very_fat','woman_lean','woman_lean_tall','woman_short_fat'};
sequenceNames = {'02_06','13_30','15_12','26_07','28_01','28_15','49_02'};
views = {'angle_000','angle_036','angle_072','angle_108','angle_144','angle_180','angle_216','angle_252','angle_288','angle_324','center'};
testSequences_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{1})) | ...
        ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{2})) | ...
        ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{3}));
featureDir = 'C:\Projects\PoseEstimation\data\';
training_features = cell(1);
for iModel = 1 %[2 3 4 5 7 8] % woman_lean is not a unique prefix
    imdb.images.set(:) = 3;
    trainSamples_bool = cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        ~testSequences_bool;
    testSamples_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        testSequences_bool;
    imdb.images.set(trainSamples_bool) = 1;
    imdb.images.set(testSamples_bool) = 2;
%     imdb.images.set(BMHADsamples_bool) = 3;
    opts.dataDir = ['C:\Projects\PoseEstimation\data\networks\Synth\3Dpose_withAugCorr_Alex4OverParam130_testOnModel_' num2str(iModel)] ;
    load([opts.dataDir filesep 'net-epoch-100.mat'])
    % in this CNN, the number of joints was 18
    numJoints = 18;
    
    batchSize = 1;
    trainIdx = find(imdb.images.set == 1);
    testIdx = find(imdb.images.set == 2);
    evaluate_with_limb_lengths
%     % move network to GPU
%     net = vl_simplenn_move(net, 'gpu') ;
% 
% %%     % calculate all features from the TRAIN set
% %     cDir = [featureDir 'train'];
% %     mkdir(cDir);
% %     cID = '';
% %     for iTrainSample = 120444:ceil(numel(trainIdx)/batchSize) %1:ceil(numel(trainIdx)/batchSize)
% %         firstIdx = (iTrainSample-1)*batchSize+1;
% %         lastIdx = min((iTrainSample)*batchSize,numel(trainIdx));
% %         im = imdb.images.data(:,:,:,trainIdx(firstIdx:lastIdx));
% % %         lbl = imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
% %         lbl = imdb.images.labelsOverParam(:,:,:,trainIdx(firstIdx:lastIdx));
% %         net.layers{end}.class = gpuArray(lbl);
% %         res = vl_simplenn_f(net, gpuArray(im), [], [], ...
% %           'disableDropout', true, ...
% %           'conserveMemory', false, ...
% %           'sync', false) ;
% %         feature = squeeze(gather(res(end-2).x));
% %         
% %         if strcmp(cID,imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)})
% %             frameNr = frameNr+1;
% %         else
% %             frameNr = 1;
% %             cID = imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)};
% %         end
% %         IDparts = regexp(imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)},'_','split');
% %         if ~strcmp(IDparts{1},'center')
% %             cView = [IDparts{1} '_' IDparts{2}];
% %             cSeq = [IDparts{3} '_' IDparts{4}];
% %             cSubj = [IDparts{5} '_' IDparts{6}];
% %             if numel(IDparts)==7
% %                 cSubj = [cSubj '_' IDparts{7}];
% %             end
% %         else
% %             cView = IDparts{1};
% %             cSeq = [IDparts{2} '_' IDparts{3}];
% %             cSubj = [IDparts{4} '_' IDparts{5}];
% %             if numel(IDparts)==6
% %                 cSubj = [cSubj '_' IDparts{6}];
% %             end
% %         end
% %         featFileName = sprintf('view%d_seq%d_subj%d_frame%d.txt',...
% %             find(strcmp(views,cView)),...
% %             find(strcmp(sequenceNames,cSeq)),...
% %             find(strcmp(modelNames,cSubj)),...
% %             frameNr);
% %         fid = fopen(fullfile(cDir,featFileName),'w');
% %         fwrite(fid,feature,'single');
% %         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==feature)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% % 
% %         fprintf('written training feature to file %s\n',featFileName);
% %     end
% %     
% %%    % calculate all features from the TEST set
%     cDir = [featureDir 'test'];
%     mkdir(cDir);
%     cID = '';
%     for iTestSample = 1:ceil(numel(testIdx)/batchSize)
%         firstIdx = (iTestSample-1)*batchSize+1;
%         lastIdx = min((iTestSample)*batchSize,numel(testIdx));
%         iSample = testIdx(firstIdx:lastIdx);
%         im = imdb.images.data(:,:,:,testIdx(firstIdx:lastIdx));
% %         lbl = imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
%         lbl = imdb.images.labelsOverParam(:,:,:,testIdx(firstIdx:lastIdx));
%         net.layers{end}.class = gpuArray(lbl);
%         res = vl_simplenn_f(net, gpuArray(im), [], [], ...
%           'disableDropout', true, ...
%           'conserveMemory', false, ...
%           'sync', false) ;
%         feature = squeeze(gather(res(end-2).x));
%         joints = squeeze(gather(res(end-1).x(1:3*numJoints)));   
%         bbox = imdb.meta.bbox(iSample,:);
%         dX = abs(bbox(1)-bbox(2));
%         dY = abs(bbox(3)-bbox(4));
%         dZ = abs(bbox(5)-bbox(6));
%         boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
%         joints = joints.*boxMultiplier;
% 
%         if strcmp(cID,imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)})
%             frameNr = frameNr+1;
%         else
%             frameNr = 1;
%             cID = imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)};
%         end
%         IDparts = regexp(imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)},'_','split');
%         if ~strcmp(IDparts{1},'center')
%             cView = [IDparts{1} '_' IDparts{2}];
%             cSeq = [IDparts{3} '_' IDparts{4}];
%             cSubj = [IDparts{5} '_' IDparts{6}];
%             if numel(IDparts)==7
%                 cSubj = [cSubj '_' IDparts{7}];
%             end
%         else
%             cView = IDparts{1};
%             cSeq = [IDparts{2} '_' IDparts{3}];
%             cSubj = [IDparts{4} '_' IDparts{5}];
%             if numel(IDparts)==6
%                 cSubj = [cSubj '_' IDparts{6}];
%             end
%         end
%         featFileName = sprintf('view%d_seq%d_subj%d_frame%d.txt',...
%             find(strcmp(views,cView)),...
%             find(strcmp(sequenceNames,cSeq)),...
%             find(strcmp(modelNames,cSubj)),...
%             frameNr);
%         fid = fopen(fullfile(cDir,featFileName),'w');
%         fwrite(fid,feature,'single');
%         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==feature)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% 
%         fprintf('written testing feature to file %s\n',featFileName);
%     end

%%     % generate ground truth output of the TRAIN set
%     cDir = [featureDir 'train'];
%     cID = '';
%     for iTrainSample = 1:ceil(numel(trainIdx)/batchSize) %1:ceil(numel(trainIdx)/batchSize)
%         firstIdx = (iTrainSample-1)*batchSize+1;
%         lastIdx = min((iTrainSample)*batchSize,numel(trainIdx));
%         lbl = squeeze(imdb.images.labels(:,:,:,trainIdx(firstIdx:lastIdx)));
%         
%         if strcmp(cID,imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)})
%             frameNr = frameNr+1;
%         else
%             frameNr = 1;
%             cID = imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)};
%         end
%         IDparts = regexp(imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)},'_','split');
%         if ~strcmp(IDparts{1},'center')
%             cView = [IDparts{1} '_' IDparts{2}];
%             cSeq = [IDparts{3} '_' IDparts{4}];
%             cSubj = [IDparts{5} '_' IDparts{6}];
%             if numel(IDparts)==7
%                 cSubj = [cSubj '_' IDparts{7}];
%             end
%         else
%             cView = IDparts{1};
%             cSeq = [IDparts{2} '_' IDparts{3}];
%             cSubj = [IDparts{4} '_' IDparts{5}];
%             if numel(IDparts)==6
%                 cSubj = [cSubj '_' IDparts{6}];
%             end
%         end
%         featFileName = sprintf('GTjoints_view%d_seq%d_subj%d_frame%d.txt',...
%             find(strcmp(views,cView)),...
%             find(strcmp(sequenceNames,cSeq)),...
%             find(strcmp(modelNames,cSubj)),...
%             frameNr);
%         fid = fopen(fullfile(cDir,featFileName),'w');
%         fwrite(fid,lbl,'single');
%         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==lbl)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% 
%         fprintf('written training GT to file %s\n',featFileName);
%     end
%     
%     % generate ground truth output of the TEST set
%     cDir = [featureDir 'test'];
%     mkdir(cDir);
%     cID = '';
%     for iTestSample = 1:ceil(numel(testIdx)/batchSize)
%         firstIdx = (iTestSample-1)*batchSize+1;
%         lastIdx = min((iTestSample)*batchSize,numel(testIdx));
%         lbl = squeeze(imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx)));
% 
%         if strcmp(cID,imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)})
%             frameNr = frameNr+1;
%         else
%             frameNr = 1;
%             cID = imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)};
%         end
%         IDparts = regexp(imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)},'_','split');
%         if ~strcmp(IDparts{1},'center')
%             cView = [IDparts{1} '_' IDparts{2}];
%             cSeq = [IDparts{3} '_' IDparts{4}];
%             cSubj = [IDparts{5} '_' IDparts{6}];
%             if numel(IDparts)==7
%                 cSubj = [cSubj '_' IDparts{7}];
%             end
%         else
%             cView = IDparts{1};
%             cSeq = [IDparts{2} '_' IDparts{3}];
%             cSubj = [IDparts{4} '_' IDparts{5}];
%             if numel(IDparts)==6
%                 cSubj = [cSubj '_' IDparts{6}];
%             end
%         end
%         featFileName = sprintf('GTjoints_view%d_seq%d_subj%d_frame%d.txt',...
%             find(strcmp(views,cView)),...
%             find(strcmp(sequenceNames,cSeq)),...
%             find(strcmp(modelNames,cSubj)),...
%             frameNr);
%         fid = fopen(fullfile(cDir,featFileName),'w');
%         fwrite(fid,lbl,'single');
%         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==feature)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% 
%         fprintf('written testing GT to file %s\n',featFileName);
%     end

% generate bbox output of the TRAIN set
%     cDir = [featureDir 'train'];
%     mkdir(cDir);
%     cID = '';
%     for iTrainSample = 1:ceil(numel(trainIdx)/batchSize) %1:ceil(numel(trainIdx)/batchSize)
%         firstIdx = (iTrainSample-1)*batchSize+1;
%         lastIdx = min((iTrainSample)*batchSize,numel(trainIdx));
% %         lbl = squeeze(imdb.images.labels(:,:,:,trainIdx(firstIdx:lastIdx)));
%         bbox = squeeze(imdb.meta.bbox(trainIdx(firstIdx:lastIdx),:));
%         if strcmp(cID,imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)})
%             frameNr = frameNr+1;
%         else
%             frameNr = 1;
%             cID = imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)};
%         end
%         IDparts = regexp(imdb.meta.sequenceID{trainIdx(firstIdx:lastIdx)},'_','split');
%         if ~strcmp(IDparts{1},'center')
%             cView = [IDparts{1} '_' IDparts{2}];
%             cSeq = [IDparts{3} '_' IDparts{4}];
%             cSubj = [IDparts{5} '_' IDparts{6}];
%             if numel(IDparts)==7
%                 cSubj = [cSubj '_' IDparts{7}];
%             end
%         else
%             cView = IDparts{1};
%             cSeq = [IDparts{2} '_' IDparts{3}];
%             cSubj = [IDparts{4} '_' IDparts{5}];
%             if numel(IDparts)==6
%                 cSubj = [cSubj '_' IDparts{6}];
%             end
%         end
%         featFileName = sprintf('bbox_view%d_seq%d_subj%d_frame%d.txt',...
%             find(strcmp(views,cView)),...
%             find(strcmp(sequenceNames,cSeq)),...
%             find(strcmp(modelNames,cSubj)),...
%             frameNr);
%         fid = fopen(fullfile(cDir,featFileName),'w');
%         fwrite(fid,bbox,'single');
%         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==lbl)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% 
%         fprintf('written training bbox to file %s\n',featFileName);
%     end
    
%     % generate bbox output of the TEST set
%     cDir = [featureDir 'test'];
%     mkdir(cDir);
%     cID = '';
%     for iTestSample = 1:ceil(numel(testIdx)/batchSize)
%         firstIdx = (iTestSample-1)*batchSize+1;
%         lastIdx = min((iTestSample)*batchSize,numel(testIdx));
%         %         lbl = squeeze(imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx)));
%         bbox = squeeze(imdb.meta.bbox(testIdx(firstIdx:lastIdx),:));
% 
%         if strcmp(cID,imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)})
%             frameNr = frameNr+1;
%         else
%             frameNr = 1;
%             cID = imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)};
%         end
%         IDparts = regexp(imdb.meta.sequenceID{testIdx(firstIdx:lastIdx)},'_','split');
%         if ~strcmp(IDparts{1},'center')
%             cView = [IDparts{1} '_' IDparts{2}];
%             cSeq = [IDparts{3} '_' IDparts{4}];
%             cSubj = [IDparts{5} '_' IDparts{6}];
%             if numel(IDparts)==7
%                 cSubj = [cSubj '_' IDparts{7}];
%             end
%         else
%             cView = IDparts{1};
%             cSeq = [IDparts{2} '_' IDparts{3}];
%             cSubj = [IDparts{4} '_' IDparts{5}];
%             if numel(IDparts)==6
%                 cSubj = [cSubj '_' IDparts{6}];
%             end
%         end
%         featFileName = sprintf('bbox_view%d_seq%d_subj%d_frame%d.txt',...
%             find(strcmp(views,cView)),...
%             find(strcmp(sequenceNames,cSeq)),...
%             find(strcmp(modelNames,cSubj)),...
%             frameNr);
%         fid = fopen(fullfile(cDir,featFileName),'w');
%         fwrite(fid,bbox,'single');
%         fclose(fid);
% %         fid = fopen(fullfile(cDir,featFileName));
% %         test=fread(fid,1024,'single');
% %         if all(test==feature)
% %             disp('Yay!')
% %         else
% %             disp('Noooo')
% %         end
% %         fclose(fid);
% 
%         fprintf('written testing bbox to file %s\n',featFileName);
%     end
    
end
% output:
% tested model 1 of 7, avg dist: 93.990mm
% tested model 2 of 7, avg dist: 86.397mm
% tested model 3 of 7, avg dist: 81.243mm
% tested model 4 of 7, avg dist: 107.529mm
% tested model 5 of 7, avg dist: 86.652mm
% tested model 7 of 7, avg dist: 94.676mm
% tested model 8 of 7, avg dist: 76.517mm