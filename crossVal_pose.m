% train cnn_pose3D in various configurations

% exclude BMHAD from testing
BMHADsamples_bool = strncmp('s',imdb.meta.sequenceID,1); % check for "s" in the sequence name
% split EPFLsynth dataset into train/test
% --> use different Models doing different things! Leave-1-out is probably
% good in performance, otherwise something is missing as we only have 8
% models so far
%
% seq nr / #files
% 1 / 8000
% 2 / 9000
% 3 / 36.000
% 4 / 31.000
% 5 / 13.000
% 6 / 14.000
% 7 / 8.000
seqSizes = [8000, 9000, 36000, 31000, 13000, 14000, 8000];
modelNames = {'man_fat','man_lean','man_short_skinny','man_tall_muscular','man_very_fat','woman_lean','woman_lean_tall','woman_short_fat'};
sequenceNames = {'02_06','13_30','15_12','26_07','28_01','28_15','49_02'};
testSequences_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{1})) | ...
        ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{2})) | ...
        ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID,sequenceNames{3}));
%% run training
for iModel = 4 %[1 2 3 4 5 7 8] % woman_lean is not a unique prefix
    imdb.images.set(:) = 3;
    trainSamples_bool = cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        ~testSequences_bool;
    testSamples_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        testSequences_bool;
    imdb.images.set(trainSamples_bool) = 1;
    imdb.images.set(testSamples_bool) = 2;
    imdb.images.set(BMHADsamples_bool) = 3;
    opts.dataDir = ['C:\Projects\PoseEstimation\data\networks\Synth\3Dpose_withAugCorr_Alex4OverParam130_3xfc_lessWeights_testOnModel_' num2str(iModel)] ;
    [net, info] = cnn_pose3D(imdb,opts);
end

%% validate on all leave one out runs

for iModel = [1 2 3 4 5 7 8] % woman_lean is not a unique prefix
    imdb.images.set(:) = 3;
    trainSamples_bool = cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        ~testSequences_bool;
    testSamples_bool = ~cellfun(@(x) isempty(x),strfind(imdb.meta.sequenceID, modelNames{iModel})) & ...
        testSequences_bool;
    imdb.images.set(trainSamples_bool) = 1;
    imdb.images.set(testSamples_bool) = 2;
    imdb.images.set(BMHADsamples_bool) = 3;
    opts.dataDir = ['C:\Projects\PoseEstimation\data\networks\Synth\3Dpose_withAugCorr_Alex4OverParam130_testOnModel_' num2str(iModel)] ;
    
    load([opts.dataDir filesep 'net-epoch-100.mat'])
    
    batchSize = 1;
    testIdx = find(imdb.images.set == 2);
    perJointCollector   = cell(1);
    avgErrCollector     = cell(1);
    precisionCollector  = cell(1);
    diffCollector       = cell(1);
    % Heltens proposed evaluation
    fakeEval = false;
    % move network to GPU
    net = vl_simplenn_move(net, 'gpu') ;

    % randTrainIdx = randperm(numel(trainIdx),numSamples);
    for iTrainSample = 1:ceil(numel(testIdx)/batchSize)
        firstIdx = (iTrainSample-1)*batchSize+1;
        lastIdx = min((iTrainSample)*batchSize,numel(testIdx));
        im = imdb.images.data(:,:,:,testIdx(firstIdx:lastIdx));
    %     lbl = imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
        lbl = imdb.images.labelsOverParam(:,:,:,testIdx(firstIdx:lastIdx));
        net.layers{end}.class = gpuArray(lbl);
        res = vl_simplenn_f(net, gpuArray(im), [], [], ...
          'disableDropout', true, ...
          'conserveMemory', false, ...
          'sync', false) ;
        joints = squeeze(gather(res(end-1).x));
        GTjoints = squeeze(lbl);
        % retrieve bbox measurements
        bbox = imdb.meta.bbox(testIdx(firstIdx:lastIdx),:);
        dX = abs(bbox(1)-bbox(2));
        dY = abs(bbox(3)-bbox(4));
        dZ = abs(bbox(5)-bbox(6));
        boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
        % calc per joint error
        diff = joints-GTjoints;
        % only compare pose, not length or angles
        diff = diff(1:54);
        diffsq = (diff.*(boxMultiplier)).^2;

        diffCollector{end+1}=diff;
        sumPerJoint = diffsq(1:3:end) + diffsq(2:3:end) + diffsq(3:3:end);
        perJointCollector{end+1} = sumPerJoint;
        avgPrecision = nnz(sqrt(sumPerJoint(~isnan(sumPerJoint)))<100)/nnz(~isnan(sumPerJoint));
        precisionCollector{end+1} = avgPrecision;
        avgErr = sum(sqrt(sumPerJoint(~isnan(sumPerJoint))))/nnz(~isnan(sumPerJoint));
        avgErrCollector{end+1} = avgErr;
%         fprintf('tested frame %d of %d, avg dist: %5.3fmm\n',iTrainSample,numel(testIdx),avgErr);
    end

    avgErrCollector=avgErrCollector(2:end);
    perJointCollector=perJointCollector(2:end);
    precisionCollector=precisionCollector(2:end);
    diffCollector=diffCollector(2:end);
    
    avgErrVec(iModel) = mean([avgErrCollector{:}]);
    avgPrecisionVec(iModel) = mean([precisionCollector{:}]);
    perPartAverage = mean(sqrt([perJointCollector{:}]),2);
    perPartPrecision = mean(sqrt([perJointCollector{:}])<100,2);
fprintf('tested model %d of %d, avg dist: %5.3fmm\n',iModel,7,avgErr(iModel));
end
% output:
% tested model 1 of 7, avg dist: 93.990mm
% tested model 2 of 7, avg dist: 86.397mm
% tested model 3 of 7, avg dist: 81.243mm
% tested model 4 of 7, avg dist: 107.529mm
% tested model 5 of 7, avg dist: 86.652mm
% tested model 7 of 7, avg dist: 94.676mm
% tested model 8 of 7, avg dist: 76.517mm
%% plot precision curve (pcp-similar)
figure
for i=1:250
    hold on
    plot(i,mean(mean(sqrt([perJointCollector{:}])<i,2),1),'r+')
    hold off
end
axis tight