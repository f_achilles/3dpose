function label = overparamXYZpose(labelOld,limbIdx)
% compute limb-lengths AND limb-angular representations
numJoints = max(limbIdx(:));
joints = labelOld(1:(3*numJoints));
nLimbs = size(limbIdx,2);
limbLengths = zeros(1,1,nLimbs,1,'single');
limbAngles = zeros(1,1,3*nLimbs,1,'single');
for iLimb = 1:nLimbs
    limbVec = ...
        -joints((limbIdx(1,iLimb)-1)*3+1:limbIdx(1,iLimb)*3)...
        +joints((limbIdx(2,iLimb)-1)*3+1:limbIdx(2,iLimb)*3);
    limbLengths(iLimb) = norm(limbVec(:))+eps;
    limbAngles((iLimb-1)*3+1:iLimb*3) = limbVec/limbLengths(iLimb);
end
% stack the representations
label = cat(3,labelOld,limbLengths,limbAngles);
end