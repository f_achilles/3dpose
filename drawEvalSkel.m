function handleVec = drawEvalSkel(joints,str,optdim,optDataset,J,handleVec)
if exist('optdim','var')
    if strcmp(optdim,'3d')
        make3d = true;
    elseif strcmp(optdim,'2d')
        make3d = false;
    else
        error('Put dimension option to either ''3d'' or ''2d''.')
    end
else
    make3d=false;
end
if exist('optDataset','var')
    if strcmp(optDataset,'EVAL')
    J=[1     1     3     4     1    7     8     1     1     12     16 ;
       2     3     4     5     7    8     9    12    16     13     17 ];
    elseif strcmp(optDataset,'PDT')
    J=[4     3     3     9     5    10     6    11    7     3     2  1  1  17 13 18 14 19 15 ;
       3     9     5     10    6    11     7    12    8     2     1  17 13 18 14 19 15 20 16];
    elseif strcmp(optDataset,'Synth')
    J=[8     12     12     17   4    6     12    18   5    7     18 17  1  9 15 13 9 16 14 ;
       12     1     17     4    6    10    18    5    7   11     1   1  9 15 13  2 16 14 3];
    elseif strcmp(optDataset,'mocap') || strcmp(optDataset,'mocap_tubes')
        J = [4 2 4 4 5 6 7 8 5 6 2 3 11 12;1 3 5 6 7 8 9 10 2 3 11 12 13 14];
    else
    end
else
J=[20     1     2     1     8    10     2     9    11     3     4     7     7     5     6    14    15    16    17;
    3     3     3     8    10    12     9    11    13     4     7     5     6    14    15    16    17    18    19];
end

if strcmp(optDataset,'Synth')
    numJoints = 18;
elseif strcmp(optDataset,'Synth17')
%     limbIdx = [11,2,2,1,11,11,9,16,6,13,2,2,4,4,10,17,8,15;3,11,1,4,9,16,6,13,7,14,9,16,10,17,8,15,5,12];
    numJoints = 17;
elseif strcmp(optDataset,'PDT')
    numJoints = 20;
elseif strcmp(optDataset,'mocap') || strcmp(optDataset,'mocap_tubes')
    numJoints = 14;
else
    numJoints = 19;
end
if make3d
S = [reshape(joints(1:numJoints),[],1) reshape(joints((numJoints+1):2*numJoints),[],1) reshape(joints((2*numJoints+1):3*numJoints),[],1)];
    %     S = [joints(1:numJoints) joints((2*numJoints+1):3*numJoints) 1-joints((numJoints+1):2*numJoints)];
% S = [joints(1:numJoints) joints((2*numJoints+1):3*numJoints) -joints((numJoints+1):2*numJoints)];
else
    S = [joints(1:20) joints(21:40)];
end
% if make3d
%     handleVec=plot3(S(:,1),S(:,2),S(:,3),str);
% else
%     handleVec=plot(S(:,1),S(:,2),str);
% end
%rotate(h,[0 45], -180);
% set(gca,'DataAspectRatio',[1 1 1])
% axis([0 400 0 400 0 400])
[xS,yS,zS] = sphere(20);
partDiam = 40;
if strcmp(optDataset,'mocap_tubes')
    for j = 1:size(J,2)
        c1=J(1,j);
        c2=J(2,j);
        if isnan(S(c1,1)) || isnan(S(c2,1)) %just check the x, the others will also be NaN
            if ~isnan(S(c1,1)) %only end is NaN
                if exist('handleVec','var') && ~isempty(handleVec) && numel(handleVec) >= j
                    handleVec(j).delete;
                end
                old_x_end = get(handleVec(c2+size(J,2)),'xdata');
                old_y_end = get(handleVec(c2+size(J,2)),'ydata');
                old_z_end = get(handleVec(c2+size(J,2)),'zdata');
                handleVec(j) = Cylinder(S(c1,:)',[mean(old_x_end(:));mean(old_y_end(:));mean(old_z_end(:))],partDiam/2,30,'b',0,0);
%                 set(handleVec(j),'xdata',[S(c1,1) old_x(2)],'ydata',[S(c1,2) old_y(2)],'zdata',[S(c1,3) old_z(2)]);
            elseif ~isnan(S(c2,1)) %only begin is Nan
                if exist('handleVec','var') && ~isempty(handleVec) && numel(handleVec) >= j
                    handleVec(j).delete;
                end
                old_x_begin = get(handleVec(c2+size(J,2)),'xdata');
                old_y_begin = get(handleVec(c2+size(J,2)),'ydata');
                old_z_begin = get(handleVec(c2+size(J,2)),'zdata');
                handleVec(j) = Cylinder([mean(old_x_begin(:));mean(old_y_begin(:));mean(old_z_begin(:))],S(c2,:)',partDiam/2,30,'b',0,0);
%                 set(handleVec(j),'xdata',[old_x(1) S(c2,1)],'ydata',[old_y(1) S(c2,2)],'zdata',[old_z(1) S(c2,3)]);
            else %both are NaN
                % do nothing (old tube remains)
            end
        else
            if exist('handleVec','var') && ~isempty(handleVec) && numel(handleVec) >= j
                handleVec(j).delete;
            end
            handleVec(j) = Cylinder(S(c1,:)',S(c2,:)',partDiam/2,30,'b',0,0);
        end
        set(handleVec(j),'facealpha',0.3);
    end
    for jointInd = 1:numJoints
        if isnan(S(jointInd,1))
            % dont change anything
        else
            if exist('handleVec','var') && ~isempty(handleVec) && numel(handleVec) >= j+jointInd
                handleVec(j+jointInd).delete;
            end
            handleVec(j+jointInd) = surf(...
                bsxfun(@plus,xS*partDiam,S(jointInd,1)),...
                bsxfun(@plus,yS*partDiam,S(jointInd,2)),...
                bsxfun(@plus,zS*partDiam,S(jointInd,3)),...
                'facecolor','blue','edgecolor','none',...
                'facealpha',0.3);
        end
    end
        
else
    if exist('handleVec','var') && ~isempty(handleVec)
        % update figure
        for j=1:size(J,2)%19
            c1=J(1,j);
            c2=J(2,j);
            if isnan(S(c1,1)) || isnan(S(c2,1)) %just check the x, the others will also be NaN
                old_x = get(handleVec(j),'xdata');
                old_y = get(handleVec(j),'ydata');
                old_z = get(handleVec(j),'zdata');
                if ~isnan(S(c1,1)) %only end is NaN
                    set(handleVec(j),'xdata',[S(c1,1) old_x(2)],'ydata',[S(c1,2) old_y(2)],'zdata',[S(c1,3) old_z(2)]);
                elseif ~isnan(S(c2,1)) %only begin is Nan
                    set(handleVec(j),'xdata',[old_x(1) S(c2,1)],'ydata',[old_y(1) S(c2,2)],'zdata',[old_z(1) S(c2,3)]);
                else %both are NaN
                    %dont update
                end
                set(handleVec(j),'color','k');
            else
                set(handleVec(j),'xdata',[S(c1,1) S(c2,1)],'ydata',[S(c1,2) S(c2,2)],'zdata',[S(c1,3) S(c2,3)]);
                set(handleVec(j),'color',str(2));
            end
        end
    else
        % draw new figure
        for j=1:size(J,2)%19
            c1=J(1,j);
            c2=J(2,j);
            if make3d
                handleVec(j) = line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)], [S(c1,3) S(c2,3)],'marker',str(1),'color',str(2),'linewidth',3);
            else
                handleVec(j) = line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)],'marker',str(1),'color',str(2),'linewidth',3);
            end
        end
    end
end
% axis image vis3d
% view(3)
end