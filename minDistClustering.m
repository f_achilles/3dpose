% based on previous keyposes, only accept new poses that fulfil a minimal
% distance contraint

% in theory, we have to set up all samples in a fully connected graph,
% noting the (squared) distance to every other sample. samples whose
% edge with the smalles weight has a weight below a certain threshold, are
% sequentially removed from the graph, starting with the samples with the
% smallest weight.
% for setting up this graph, we need to compute n*(n-1)/2 weights. For a
% pose-database with 150.000 samples this is 11e9, so 44GB of
% single-precision values. this is not feasible.

% approximation: poses are temporally consistent in our database, so that
% most redundant poses will come up sequentially. we only accept the next
% pose if its minimal L1-distance in any of its components (xyz for each
% joint) is above a threshold "d".

listOfGoodIndices = cell(1);
d = 0.05; %normalized units

lastPose = squeeze(imdb.images.labels(:,:,:,1));
listOfGoodIndices{1} = 1;

idx = 1;
label1 = lastPose;
data1 = squeeze(imdb.images.data(:,:,:,1));
save('C:\Projects\PoseEstimation\data\imdb_alex3.mat','data1','label1','-v7.3');
clear(sprintf('data%d',idx),sprintf('label%d',idx))
tic
for i=2:500 %size(imdb.images.labels,4)

    cPose = squeeze(imdb.images.labels(:,:,:,i));
    
    % good frame?
    if max(abs(cPose-lastPose)) > d
        listOfGoodIndices{end+1} = i;   % save index
        lastPose = cPose;               % update keyframe
        % append to db
        idx = idx+1;
        assignin('base',sprintf('data%d',idx),squeeze(imdb.images.data(:,:,:,i)));
        assignin('base',sprintf('label%d',idx),squeeze(imdb.images.labels(:,:,:,i)));
        save('C:\Projects\PoseEstimation\data\imdb_alex3.mat',sprintf('data%d',idx),sprintf('label%d',idx),'-append');
        clear(sprintf('data%d',idx),sprintf('label%d',idx))
    end
% disp(i)
end
t=toc;
fprintf('time required per stored sample: %d ms\n',1000*t/idx);