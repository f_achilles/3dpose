% refine the EPFLCNN with mocap data

% load pretrained CNN
% load('C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat')

load imdb
imdb.images.data = imdb.images.data/1000;
imdb.images.labels = imdb.images.labels/1000;
N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);
imdb.images.set(21601:end) = 2;

%% maximum distance clustering to remove average pose bias
trainInds = find(imdb.images.set == 1);
validInds = 1;
for iT = 2:numel(trainInds)
    cPose = imdb.images.labels(1,1,:,trainInds(iT));
    vPoses = imdb.images.labels(1,1,:,trainInds(validInds));
    cPose = reshape(cPose,3,13);
    vPoses = reshape(vPoses,3,13,[]);
    diff = bsxfun(@minus,vPoses,cPose);
    dist = sqrt(sum(diff.^2,1));
    maxD = max(dist,[],2);
    maxD = min(maxD,[],3);
    if maxD > 0.05
        validInds = cat(1,validInds,iT);
    else
        imdb.images.set(trainInds(iT)) = 3;
    end
end
fprintf('Result of max dist clustering: %d of %d samples survived!\n',numel(validInds),numel(trainInds));

%% visualize valid training images
trainInds = find(imdb.images.set == 2);
figure;
subplot(121)
plot(trainInds); axis image
subplot(122)
lbls = squeeze(imdb.images.labels(:,:,:,trainInds));
plot3(lbls(1:3:end,:)',lbls(2:3:end,:)',lbls(3:3:end,:)','g-'); axis image
xlabel('x');ylabel('y');zlabel('z')
figure;
hIm = imagesc(imdb.images.data(:,:,1,1)); axis image;
colormap(hot(1500))
caxis([1 2.5])
for iT = 1:numel(trainInds)
    set(hIm,'cdata',imdb.images.data(:,:,1,trainInds(iT)));
    pause
end

%% perform the training

opts.dataDir = 'E:\mocapPose\trainedCNN\nonpretrain_nonclustered_lr1e-2_batch1000_NN';%'E:\mocapPose\trainedCNN\pretrain_nonrendered_nonclustered_lr1e-4';
opts.train.numEpochs = 1000 ;
opts.train.learningRate = 1e-2 ;
opts.train.numSubBatches = 10 ;
opts.train.batchSize = 1000 ;
opts.train.gpus = 1;

% opts.pretrainNet = 'C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat';

[net, info] = cnn_pose3D_symm(imdb,opts);

%% evaluate refined network
networks = dir(fullfile(opts.dataDir,'*epoch-290*.mat'));
load(fullfile(opts.dataDir,networks(end).name));
net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);
net.layers = net.layers(1:(end-1));
testInds = find(imdb.images.set==1);
figure
res = vl_simplenn_f(net,imdb.images.data(:,:,:,testInds(1)));
estimJoints = squeeze(res(end).x);
gtJoints = squeeze(imdb.images.labels(:,:,:,testInds(1)));
hEst = plot3(estimJoints(1:3:end),estimJoints(2:3:end),estimJoints(3:3:end),'b*');
hold on
hGt = plot3(gtJoints(1:3:end),gtJoints(2:3:end),gtJoints(3:3:end),'g*');
hold off
axis equal
axis([-1 1 -1 1 -1 1])
view(2)
for iT = 1:nnz(imdb.images.set==1)
    res = vl_simplenn_f(net,imdb.images.data(:,:,:,testInds(iT)));
    estimJoints = squeeze(res(end).x);
    gtJoints = squeeze(imdb.images.labels(:,:,:,testInds(iT)));
    set(hEst,'xdata',estimJoints(1:3:end),'ydata',estimJoints(2:3:end),'zdata',estimJoints(3:3:end))
    set(hGt,'xdata',gtJoints(1:3:end),'ydata',gtJoints(2:3:end),'zdata',gtJoints(3:3:end))
    pause(0.1)
end

%% write features to files
% opts.dataDir = 'E:\mocapPose\trainedCNN\pretrain_nonclustered_lr1e-4';
% networks = dir(fullfile(opts.dataDir,'*epoch-126*.mat'));
% load(fullfile(opts.dataDir,networks(end).name));
% net = vl_simplenn_move(net,'gpu');
% net.layers = net.layers(1:(end-1));
% imdb.images.data = single(imdb.images.data);
% imdb.images.labels = single(imdb.images.labels);

featFormat = repmat('%f ',1,1024);
featFormat = [featFormat(1:end-1) '\n'];
lblFormat = repmat('%f ',1,39);
lblFormat = [lblFormat(1:end-1) '\n'];
mkdir(fullfile(opts.dataDir,'train'));
mkdir(fullfile(opts.dataDir,'test'));

N = size(imdb.images.data,4);
iSeq = 1;
for iT = 1:N
%     featFileName = sprintf('featureVec1024_%s_%s_view%d_frame%d.txt',imdb.meta.info(iT).sequence,imdb.meta.info(iT).actor,imdb.meta.info(iT).angle,imdb.meta.info(iT).frame);
    featFileName = sprintf('feature_seq%d_frame%d.txt',iSeq,imdb.meta.info(iT).frame);
    res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,iT)));
    feature = squeeze(gather(res(end-1).x));
    if imdb.images.set(iT) == 1
        fid = fopen(fullfile(opts.dataDir,'train',featFileName),'w');
    elseif imdb.images.set(iT) == 2
        fid = fopen(fullfile(opts.dataDir,'test',featFileName),'w');
    end
    fprintf(fid,featFormat,feature);
    fclose(fid);
%     fprintf('%s written\n',featFileName);

%     labelFileName = sprintf('label_%s_%s_view%d_frame%d.txt',imdb.meta.info(iT).sequence,imdb.meta.info(iT).actor,imdb.meta.info(iT).angle,imdb.meta.info(iT).frame);
    labelFileName = sprintf('label_seq%d_frame%d.txt',iSeq,imdb.meta.info(iT).frame);
    label = squeeze(imdb.images.labels(:,:,:,iT));
    if any(isnan(label))
        return
    end
    if imdb.images.set(iT) == 1
        fid = fopen(fullfile(opts.dataDir,'train',labelFileName),'w');
    elseif imdb.images.set(iT) == 2
        fid = fopen(fullfile(opts.dataDir,'test',labelFileName),'w');
    end
    fprintf(fid,lblFormat,label);
    fclose(fid);
    
    if imdb.meta.info(iT).frame == 1800
        iSeq = 1+iSeq;
        fprintf('%s written\n',featFileName);
        fprintf('%s written\n',labelFileName);
    end
    
end

%% read labels from files and visualize
lblFormat = repmat('%f ',1,42);
lblFormat = [lblFormat(1:end-1) '\n'];
readDir = 'E:\mocapPose\erd';

fid = fopen(fullfile(readDir,sprintf('label_seq14_frame%d.txt',1)));
label = cell2mat(textscan(fid,lblFormat));
fclose(fid);
figure;
h_j = plot3(label(1:3:end),label(2:3:end),label(3:3:end),'g*');
axis image
axis([-0.4 0.4 -0.7 0.7 -0.1 0.4])
for iS = 1:1000
    fid = fopen(fullfile(readDir,sprintf('label_seq13_frame%d.txt',iS)));
    label = cell2mat(textscan(fid,lblFormat));
    fclose(fid);
    set(h_j,'xdata',label(1:3:end),'ydata',label(2:3:end),'zdata',label(3:3:end))
    pause(0.03)
%     jointOne(:,iS) = label(1:3);
end
figure;
subplot(311)
plot(jointOne(1,:))
xlabel('x coordinate of joint 1')
subplot(312)
plot(jointOne(2,:))
xlabel('y coordinate of joint 1')
subplot(313)
plot(jointOne(3,:))
xlabel('z coordinate of joint 1')
%% make RF dataset: extract rendered images matching to imdb and features
rootroot = 'E:\mocapPose\March10';
train = false;
rndPermute = false;

dataRoot = rootroot; %[rootroot filesep 'Meng'];
mocapDir =  [dataRoot filesep 'syncedMocap'];
kinectSrc = [dataRoot filesep 'kinectData'];
kinectDir = [kinectSrc filesep 'depthFrames'];

rfTarget = [rootroot filesep 'RFtrain'];
mkdir(rfTarget);

lblFormat = repmat('%f ',1,42);
lblFormat = [lblFormat(1:end-1) '\n'];

bedcenterFile = dir(fullfile(kinectSrc,'bedCenter_*.mat'));
load(fullfile(kinectSrc,bedcenterFile.name));
bedCenter = reshape(bedCenter,3,1);

if train
    targetDir = [rfTarget filesep 'RFtrain'];
    sampleRatio = 10/144;
else
    targetDir = [rfTarget filesep 'RFtest'];
    sampleRatio = 36/36;
end
offsetIndex = 0;
mkdir(targetDir)

desirKinAngle = 360;

% select for actor, sequence, kinect angle, and frame number range
desirSeq = '*';
suitableFiles = dir(fullfile(kinectDir,sprintf('*_%s_*_%d.png',desirSeq,desirKinAngle)));
mocapFiles = dir(fullfile(mocapDir,sprintf('*.txt')));

numExtractFiles = floor(sampleRatio*numel(suitableFiles));
if rndPermute
    extrIndices = randperm(numel(suitableFiles),numExtractFiles);
else
    extrIndices = 1:numExtractFiles;
end

fprintf('Extracting %d images from %s ...\n',numExtractFiles,dataRoot)
tic
for iFr = 1:numExtractFiles
    fn = suitableFiles(extrIndices(iFr)).name;
    if exist(fullfile(targetDir,fn),'file') && exist(fullfile(targetDir,[fn(1:end-3) 'txt']),'file')
        continue
    end
%     fn

    % parse fn:
    fnParts = regexp(fn,'mdata_(?<sequence>\w+)_(?<actor>\w+)_frame(?<ifr>\d+)_step_0_pitch_360.png','names');
%     fnParts = regexp(fn,'withBlanket_(?<actor>\w+)_(?<sequence>\w+)_frame(?<ifr>\d+)_step_0_pitch_360.png','names');
    mocapFile = dir(fullfile(mocapDir,['joints_sync_' fnParts.sequence '_' fnParts.actor '_frame' fnParts.ifr '.txt']));
    
    fid = fopen(fullfile(mocapDir,mocapFile.name));
    mocap_in = cell2mat(textscan(fid,lblFormat));
    fclose(fid);
    label = single(reshape(mocap_in,3,[]));
    for iJ = 1:size(label,2)
        if norm(label(:,iJ)+bedCenter) < 10
            label(:,iJ) = NaN;
%             disp('A NaN was generated!')
        end
    end
    mocap_in = reshape(label,size(mocap_in));
    fid = fopen(fullfile(targetDir,sprintf('%d.txt',iFr)),'w');
    fprintf(fid,lblFormat,mocap_in);
% get label from IMDB
%     actorBool = strcmp({imdb.meta.info.actor},fnParts.actor);
%     frameBool = [imdb.meta.info.frame] == str2double(fnParts.ifr);
%     seqBool = strcmp({imdb.meta.info.sequence},'withBlanket_movementsH');
%     ixd = find(seqBool&actorBool&frameBool)
%     fid = fopen(fullfile(targetDir,[fn(1:end-3) 'txt']),'w');
%     fprintf(fid,lblFormat,reshape(imdb.images.labels(:,:,:,ixd),1,[]));
    fclose(fid);
    
    kinect_in = imread(fullfile(kinectDir,fn));
    %     imwrite(flipud(kinect_in),fullfile(targetDir,sprintf('%d.png',iFr+offsetIndex)));
    imwrite(flipud(kinect_in),fullfile(targetDir,sprintf('%d.png',iFr)));
end
fprintf('DONE! duration: %f seconds.\n',toc);

%% quick rename of files

root = 'E:\mocapPose\renderedBlankets\rfTest_withBlanket\RFtest';
labels = dir([root filesep '*.txt']);
images = dir([root filesep '*.png']);
for i=1:numel(labels)
if strcmp(images(i).name(1:end-3),labels(i).name(1:end-3))
movefile(fullfile(root,labels(i).name),fullfile(root,sprintf('%d.txt',i)));
movefile(fullfile(root,images(i).name),fullfile(root,sprintf('%d.png',i)));
else
error('What? but Why?')
end
end



