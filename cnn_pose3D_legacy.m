function [net, info] = cnn_pose3D_legacy(imdb)

opts.dataDir = fullfile('C:\Projects\CombinedSegmentationAndPoseEstimation\data\networks\PDT\3DposePDTNormed256sBbox_vgg_f_flipAugment') ;
opts.expDir = opts.dataDir;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 25 ;
opts.train.numEpochs = 50 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
opts.train.learningRate = 1e-2 ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = false;
% opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

% network_pose3D_120x120_medium
netOld=load('C:\Users\Felix\Downloads\imagenet-vgg-f.mat');
for iLayer = 1:16
    net.layers{iLayer} = netOld.layers{iLayer};
    if isfield(net.layers{iLayer},'weights')
        net.layers{iLayer}.filters  = net.layers{iLayer}.weights{1};
        net.layers{iLayer}.biases   = net.layers{iLayer}.weights{2};
        net.layers{iLayer}.weights  = [];
    end
end
f=1/100 ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,4096,1024, 'single'), ...
                           'biases', zeros(1, 1024, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,1024,256, 'single'), ...
                           'biases', zeros(1, 256, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,256,3*20, 'single'), ...
                           'biases', zeros(1, 3*20, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'euclid') ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

[net, info] = cnn_train(net, imdb, @getBatchImagenet, ...
    opts.train) ;
end

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(:,:,:,batch) ;
end

% --------------------------------------------------------------------
function [im, labels] = getBatchImagenet(imdb, batch)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch);
% flip the first N/2 images
im(:,:,:,1:floor(numel(batch)/2)) = fliplr(im(:,:,:,1:floor(numel(batch)/2)));

im = imresize(im,[224 224]) ;
labels = imdb.images.labels(:,:,:,batch) ;
% flip x-coordinate of the first N/2 labels
labels(:,:,1:3:end,1:floor(numel(batch)/2)) =  1-labels(:,:,1:3:end,1:floor(numel(batch)/2));
end