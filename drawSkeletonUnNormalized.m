%% draw skeleton in normalized and in non-normalized bounding box
i=1;
drawEvalSkel([S(1:3:end);S(2:3:end);S(3:3:end)],'*g','3d','PDT')
axis equal
axis([0 1 0 1 0 1])

cBox = imdb.meta.bbox(i,:);
drawEvalSkel([...
    cBox(1)+(cBox(2)-cBox(1))*S(1:3:end);...
    cBox(3)+(cBox(4)-cBox(3))*S(2:3:end);...
    cBox(5)+(cBox(6)-cBox(5))*S(3:3:end)],'*g','3d','PDT')
axis equal
axis([cBox(1:2) cBox(5:6) -cBox(4) -cBox(3)])