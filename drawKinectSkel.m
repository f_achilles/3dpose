function handleVec = drawKinectSkel(joints,str,optdim)
if exist('optdim','var')
    if strcmp(optdim,'3d')
        make3d = true;
    elseif strcmp(optdim,'2d')
        make3d = false;
    else
        error('Put dimension option to either ''3d'' or ''2d''.')
    end
else
    make3d=false;
end
J=[20     1     2     1     8    10     2     9    11     3     4     7     7     5     6    14    15    16    17;
    3     3     3     8    10    12     9    11    13     4     7     5     6    14    15    16    17    18    19];

if make3d
    S = [joints(1:20) joints(21:40) joints(41:60)];
else
    S = [joints(1:20) joints(21:40)];
end
if make3d
    handleVec=plot3(S(:,1),S(:,2),S(:,3),str);
else
    handleVec=plot(S(:,1),S(:,2),str);
end
%rotate(h,[0 45], -180);
set(gca,'DataAspectRatio',[1 1 1])
% axis([0 400 0 400 0 400])

for j=1:19
    c1=J(1,j);
    c2=J(2,j);
    if make3d
        line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)], [S(c1,3) S(c2,3)]);
    else
        line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)]);
    end
end
axis image vis3d
view(3)
end