f=1/100 ;
net.layers = {} ;
% CONVT 1
% input: 1x1x17*3
net.layers{end+1} = struct('type', 'convt', ...
                           'weights', {{f*randn(5,5,64,69, 'single'), zeros(1, 64, 'single')}}, ...
                           'upsample', 1, ...
                           'numGroups', 1, ...
                           'crop', 0) ; % 8x8x64
%     - layer.type = 'convt'
%     - layer.weights = {filters, biases}
%     - layer.upsample: the upsampling factor.
%     - layer.crop: the amount of output cropping.

net.layers{end+1} = struct('type', 'relu') ;

% CONVT 2
% input: 5x5x64
net.layers{end+1} = struct('type', 'convt', ...
                           'weights', {{f*randn(5,5,128,64, 'single'), zeros(1, 128, 'single')}}, ...
                           'upsample', [4 2], ...
                           'numGroups', 1, ...
                           'crop', 0) ; % 21x13x128
%     - layer.type = 'convt'
%     - layer.weights = {filters, biases}
%     - layer.upsample: the upsampling factor.
%     - layer.crop: the amount of output cropping.

net.layers{end+1} = struct('type', 'relu') ;

% CONVT 3
% input: 21x13x128
net.layers{end+1} = struct('type', 'convt', ...
                           'weights', {{f*randn(5,5,16,128, 'single'), zeros(1, 16, 'single')}}, ...
                           'upsample', 3, ...
                           'numGroups', 1, ...
                           'crop', 0) ; % 65x41x16
%     - layer.type = 'convt'
%     - layer.weights = {filters, biases}
%     - layer.upsample: the upsampling factor.
%     - layer.crop: the amount of output cropping.

net.layers{end+1} = struct('type', 'relu') ;

% CONVT 4
% input: 65x41x16
net.layers{end+1} = struct('type', 'convt', ...
                           'weights', {{f*randn(5,5,16,16, 'single'), zeros(1, 16, 'single')}}, ...
                           'upsample', 2, ...
                           'numGroups', 1, ...
                           'crop', [5 5 11 11]) ; % 123x63x16
%     - layer.type = 'convt'
%     - layer.weights = {filters, biases}
%     - layer.upsample: the upsampling factor.
%     - layer.crop: the amount of output cropping.

net.layers{end+1} = struct('type', 'relu') ;

% CONV 1
% input: 123x63x16
net.layers{end+1} = struct('type', 'conv', ...
                           'weights', {{f*randn(5,5,16,3, 'single'), zeros(1, 3, 'single')}}, ...
                           'stride', 1, ...
                           'pad', [1 0 1 0]) ; % 120x60x3
%     - layer.type = 'conv'
%     - layer.weights = {filters, biases}
%     - layer.stride: the sampling stride (usually 1).
%     - layer.pad: the padding (usually 0).

net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'euclid') ;


%% prepare mock-up imdb

% imdb.images.data = randn(1,1,51,10,'single');
% imdb.images.labels = randn(120,60,3,10,'single');
% imdb.images.labels(imdb.images.labels<0) = 0;
% imdb.images.set = ones(10,1,'single');
% imdb.images.set(1) = 2;






