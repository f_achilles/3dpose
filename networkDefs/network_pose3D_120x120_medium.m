f=1/100 ;
net.layers = {} ;
% 120 120
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(7,7,3,16, 'single'), ...
                           'biases', zeros(1, 16, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ; % 114 114
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% 57; 57
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(5,5,16,64, 'single'), ...
                           'biases', zeros(1, 64, 'single'), ...
                           'stride', 1, ...
                           'pad', [1 0 1 0]) ;% 54 54
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% 27; 27
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(3,3,64,128, 'single'), ...
                           'biases', zeros(1, 128, 'single'), ...
                           'stride', 1, ...
                           'pad', [1 0 1 0]) ;% 26 26
net.layers{end+1} = struct('type', 'relu') ;
net.layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;
% 13; 13
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(13,13,128,256, 'single'), ...
                           'biases', zeros(1, 256, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
net.layers{end+1} = struct('type', 'relu') ;
% 1,1,256
net.layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.25) ;
net.layers{end+1} = struct('type', 'conv', ...
                           'filters', f*randn(1,1,256,3*20, 'single'), ...
                           'biases', zeros(1, 3*20, 'single'), ...
                           'stride', 1, ...
                           'pad', 0) ;
% loss
net.layers{end+1} = struct('type', 'euclid') ;
% net.layers{end+1} = struct('type', 'tukey') ;