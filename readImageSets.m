%% find images that are available in the Pose AND Segmentation dataset [VAL2009]
load('C:\Projects\CombinedSegmentationAndPoseEstimation\data\pose\voc2009_keypoints.mat')
valImageSetPose = a_val09.image_stem;

valImageSetSegm_file = fopen('C:\Projects\CombinedSegmentationAndPoseEstimation\data\VOCdevkit\VOC2009\ImageSets\Segmentation\val.txt');
valImageSetSegm = textscan(valImageSetSegm_file,'%s');
valImageSetSegm = valImageSetSegm{1};

commonImageSetVal = cell(1);
for iImg = 1:numel(valImageSetPose)
    if any(strcmp(valImageSetPose{iImg},valImageSetSegm))
        commonImageSetVal{end+1} = valImageSetPose{iImg};
    end
end
commonImageSetVal = unique(commonImageSetVal(2:end));
numCommonVal = numel(commonImageSetVal);

disp(['PascalVOC 2009 Validation set has ' num2str(numCommonVal) ' images with Pose AND Segmentation annotation.'])

%% read Hariharan Pascal VOC 2011 images and annotations,
% resize to 500x500 and store in imdb format

% !! TOO Big !! --> resize to 200x200 and store in imdb format :/
resizeRes = 200;

% data
dataFolder = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\VOC_2011_Hariharan\dataset\img';
dataFiles = dir([dataFolder filesep '*.jpg']);
imdb.images.data = zeros(resizeRes,resizeRes,3,numel(dataFiles),'single');
for iDat = 1:numel(dataFiles)
    im = imread([dataFolder filesep dataFiles(iDat).name]);
    imdb.images.data(:,:,:,iDat) = single(imresize(im,[resizeRes resizeRes]));
    imdb.meta.filenames{iDat} = dataFiles(iDat).name;
    disp(iDat)
end
% labels
imdb.images.labels = zeros(resizeRes,resizeRes,1,numel(dataFiles),'single');
lblFolder = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\VOC_2011_Hariharan\dataset\cls';
lblFiles = dir([lblFolder filesep '*.mat']);
for iDat = 1:numel(lblFiles)
    lbl = load([lblFolder filesep lblFiles(iDat).name]);
    imdb.images.labels(:,:,1,iDat) = single(imresize(lbl.GTcls.Segmentation,[resizeRes resizeRes], 'nearest'));
    disp(iDat)
end
% sets
trainImageSetSegm_file = fopen('C:\Projects\CombinedSegmentationAndPoseEstimation\data\VOC_2011_Hariharan\dataset\train.txt');
trainImageSetSegm = textscan(trainImageSetSegm_file,'%s');
trainImageSetSegm = trainImageSetSegm{1};
imdb.images.set = 2*ones(1,numel(imdb.meta.filenames));
for iSample = 1:numel(imdb.meta.filenames)
if any(strcmp(imdb.meta.filenames{iSample}(1:(end-4)),trainImageSetSegm))
    imdb.images.set(iSample) = 1;
end
end

%% count class prevalence and make inverse weight-vector
numPx = numel(imdb.images.labels);
for cl = 1:21
    prev(cl)= nnz(imdb.images.labels==cl)/numPx;
end
prev = 1./prev;
prev = prev/sum(prev);
prev = prev*21;

%% read VOC 2012 keypoint annotations and compare with available GT segmentations (only class)
keypointsDir = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\Keypoints_VOC_2012\VOC2012_person_annotations';
keypointFiles = dir([keypointsDir filesep ])