%% load dataset
load('E:\mocapPose\imdb_blanket_10_subjects.mat')

%% load trained CNN
load('E:\mocapPose\trainedCNN\CNNp\CNNp_miccaiposepaper.mat')
net.layers = net.layers(1:(end-1));
net = vl_simplenn_move(net,'gpu');
% find out which CNN we have here >D
[meanErr, meanPerJoint] = testCNNwithParametersBatchBlanket(imdb,{'Meng','Leslie'},net);
% --> non-blanket CNN should have 9.05cm error
% --> blanket-CNN should have 8.61cm error

%% Video generation:
% check how blanket video looks like
% get label from IMDB
actor = 'Meng';
actorBool = strcmp({imdb.meta.info.actor},actor);
frameBool = [imdb.meta.info.frame] == 1;
seqBool = strcmp({imdb.meta.info.sequence},'withBlanket_movementsH');
% seqBool = strcmp({imdb.meta.info.sequence},'objectsS');
ixd = find(seqBool&actorBool&frameBool);

numFrames = 999; %999;
testvideo = squeeze(imdb.images.data(:,:,:,ixd+(0:numFrames-1)));

% show which frames are valid and which ones are not
validLabelsB = all(~isnan(squeeze(imdb.images.labels(:,:,:,ixd+(0:numFrames-1)))),1);
figure; plot(validLabelsB)

% good frames for Leslie
% blanket sequence
%  98 - right leg moves a little under the blanket
% 120 - right leg is stretched a bit more outwards
% 445 - left leg is lifted under the blanket
% 686 - lifting left arm
% 745 - the same
% 862 - lifting both arms high up
% 928 - stretching both arms upwards
% 989 - arms stretched behind the back

% load Kinect Video
% results in mdata
load('E:\mocapPose\March15\kinectData\mdata_movementsH_Meng.mat')
% load('E:\mocapPose\March15\kinectData\mdata_objectsS_Meng.mat')
% load extCalib
load('E:\mocapPose\March15\extCalib\calibrationData.mat')

%% good frames for Meng
% blanket sequence
% 391 - left leg is lifted under the blanket
% 464 - left knee is lifted under the blanket <-- preferred
% 879 - right arm is lifted a bit <-- preferred
% 999 - left arm is lifted a bit <-- preferred

% objectsS
% 580 - grabs an object on the table <-- preferred
% 635 - reads sth with both hands
% 820 - reads sth with both hands, right knee up

% getUpH
% 227 - sitting on right side of the bed
% 589 - getting out on the left side of the bed <-- preferred

% eatSO
% 250 - drinks from cup and turns to right side (nice)
% 939 - both arms are down, one is occluded, but very well estimated
% 1012 - good estimation
% 1120 - like 250 but even the occluded arm is good <-- preferred

smpl = 999;
% perform CNN estimation
res = vl_simplenn_f(net,gpuArray(bsxfun(@minus,imdb.images.data(:,:,:,ixd -1 + smpl),net.stats.average)),...
    [],[],'disableDropout', true, 'conserveMemory', true);
% load CNN estimation, GT and RF estimation

% replay in 3D, make video
kinectDir = 'E:\mocapPose\March15\kinectData';
fx = 364.175959919669;
fy = 364.336624832025;
cx = 259.042005353602;
cy = 209.715659858387;
nCol=512;
nRow=424;
[Xinds,Yinds] = meshgrid(1:nCol,1:nRow);
makeVideo = false;
if makeVideo
    v=VideoWriter('MocapAndEstimation_Meng.avi');
    v.Quality = 95;
    open(v)
end
syncFig = figure('Renderer','OpenGL','position',[100 100 820 768]);
% subplot(121)
% h_k = imagesc(mdata.depth{2});%imagesc(rot90(flipud(mdata.depth{2})));
Z = medfilt2(double(flipud(mdata.depth{1+smpl})),[5 5]);
% Z = double(flipud(mdata.depth{1+smpl}));
Z(Z==0) = NaN;
X = Z .* (Xinds-cx) / fx;
Y = Z .* (Yinds-cy) / fy;
% rotate pointcloud into MoCap
%H_kintoMocap.R is calibration.mat in excalib
pc_in_mocapFrame = bsxfun(@plus,H_KinToMocap.R * [X(:) Y(:) Z(:)]',H_KinToMocap.t);
hold on
h_k = surf('xdata', reshape(pc_in_mocapFrame(1,:),[nRow nCol]), 'ydata',reshape(pc_in_mocapFrame(2,:),[nRow nCol]),'zdata', reshape(pc_in_mocapFrame(3,:),[nRow nCol]), ...
    'linestyle', 'none', ...
    'marker', '.', ...
    'FaceColor', 'interp',...
    'EdgeColor', 'none',...
    'cdata', reshape(Z(:),[nRow nCol]),...
    'facealpha', 1);
axis vis3d image off
set(gca,'cameraposition',[H_KinToMocap.t],'cameraviewangle',70,'projection','perspective','cameratarget',[H_KinToMocap.t]+[H_KinToMocap.R(:,3)])
% title('3D view from Kinect position')
camlight
lighting gouraud
axis image off;
% caxis([-700 1300])
caxis([1600 3000])
colormap(gray(2000))
hold off

bedCenterFile = dir([kinectDir filesep 'bedCenter*']);
load([kinectDir filesep bedCenterFile.name]);

estimJoints = bsxfun(@plus,1000*squeeze(gather(res(end).x)),repmat(bedCenter',14,1));
offs_to_cam = bsxfun(@minus,repmat(H_KinToMocap.t,14,1),estimJoints);
offs_to_cam = reshape(offs_to_cam,3,[]);
for joint = 1:14
    offs_to_cam(:,joint) = offs_to_cam(:,joint)/norm(offs_to_cam(:,joint));
end
offs_to_cam = reshape(offs_to_cam,[],1);
estimJoints = bsxfun(@plus,estimJoints, 500*offs_to_cam);
h_e = drawEvalSkel([estimJoints(1:3:end);estimJoints(2:3:end);estimJoints(3:3:end)],'*r','3d','mocap');

mocap_in = bsxfun(@plus,1000*squeeze(imdb.images.labels(:,:,:,ixd -1 + smpl)),repmat(bedCenter',14,1));
offs_to_cam = bsxfun(@minus,repmat(H_KinToMocap.t,14,1),mocap_in);
offs_to_cam = reshape(offs_to_cam,3,[]);
for joint = 1:14
    offs_to_cam(:,joint) = offs_to_cam(:,joint)/norm(offs_to_cam(:,joint));
end
offs_to_cam = reshape(offs_to_cam,[],1);
mocap_in = bsxfun(@plus,mocap_in, 500*offs_to_cam);
h_m = drawEvalSkel([mocap_in(1:3:end);mocap_in(2:3:end);mocap_in(3:3:end)],'+g','3d','mocap');

%% load obj file and draw it as well
blanket = read_wobj(fullfile('G:\MoCap Data\blankets_meng',sprintf('frame_%0.5d.obj',smpl-1)));
dummy.vertices = bsxfun(@plus,1000*blanket.vertices,bedCenter+[0 0 50]);
dummy.faces = blanket.objects(1).data.vertices;
patch(dummy,'facecolor',[0 0 1], 'edgecolor','none','faceLighting','gouraud');
