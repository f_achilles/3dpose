rootFolder = 'C:\Projects\CombinedSegmentationAndPoseEstimation\code\outputnewIntrinClusterCenter';
mkdir(rootFolder)
mkdir([rootFolder filesep 'joints'])
mkdir([rootFolder filesep 'shapes'])

% load IMDB
% load('C:\Projects\CombinedSegmentationAndPoseEstimation\data\EVAL\EVAL_imdb_dynamicNNrescale.mat')
imdb.images.set(:) = 2;

% load net
% load('\\TESLAWS1\data\networks\Synth\3Dpose_withAug_withSymm_Alex5Shape_OverParam141_testOnSeq_1\net-epoch-75.mat')
% remove last network layer
% net.layers = net.layers(1:end-1);
% net=vl_simplenn_move(net,'gpu');

%%
% body parts for 17 joints:
numJoints = 17;
bodyParts = {...
    'Bellybutton','Chest','Head','Pelvis','LFoot',...
    'LElbow','LWrist','LKnee','LShoulder','LHip',...
    'Neck','RFoot','RElbow','RWrist','RKnee',...
    'RShoulder','RHip'}';
bodyPartIDs = 1:numJoints;

% shape parameters
% numShapeParams = 18; % for old set
numShapeParams = 67; % for cluster set

% Define test set
testIndices = find(imdb.images.set == 2);

iFrame = 1;
% Generate estimations, store to file
for iSample = testIndices
    im = imdb.images.data(:,:,3,iSample);
%     lbl = imdb.images.labels(:,:,:,iSample);
%     lbl = cat(3,lbl,ones(1,1,130-54,'single'));
%     net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_f(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', true, ...
      'sync', false) ;
    joints = squeeze(gather(res(end).x(1:3*numJoints)));   
    bbox = imdb.meta.bbox(iSample,:);
    if any(abs(bbox)>1000)
        bbox = bbox/1000;
    end
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
    joints = joints.*boxMultiplier;
    boxOffset = repmat([bbox(1);bbox(3);bbox(5)],numJoints,1);
    joints = joints+boxOffset;
    % scale in meters (EVAL bbox is already in meters)
%     joints = joints / 1000;
    % reshape
    joints = reshape(joints,3,[]);
    
    filename = sprintf('%s%s%s%sestimatedJoints_EVALframe_%06d.txt',rootFolder,filesep,'joints',filesep,iFrame);
    fID = fopen(filename,'w');
    fprintf(fID,'%d\n',numJoints);
    for iJ = bodyPartIDs
        fprintf(fID,'%s %f %f %f\n',bodyParts{iJ},joints(1,iJ),joints(2,iJ),joints(3,iJ));
    end
    fclose(fID);
    
    % shape
    shapeVec = squeeze(gather(res(end).x(3*numJoints+(1:numShapeParams))));
    filename = sprintf('%s%s%s%sestimatedShape_EVALframe_%06d.txt',rootFolder,filesep,'shapes',filesep,iFrame);
    fID = fopen(filename,'w');
    fprintf(fID,'%d\n',numShapeParams);
    for iS = 1:numShapeParams
        fprintf(fID,'%f\n',shapeVec(iS));
    end
    fclose(fID);
    
    iFrame = iFrame+1

end


