rootFolder = 'E:\evaluation\eval_results_mapped_znoisenewintrinsics_1to1';
markerPosFiles = dir([rootFolder filesep '*.txt']);

% load IMDB
% load('C:\Projects\CombinedSegmentationAndPoseEstimation\data\EVAL\EVAL_imdb_dynamicNNrescale.mat')
N = size(imdb.images.data,4);

numExpectedMarkers = 12;
distances = zeros(N,numExpectedMarkers,'single');
for iFrame = 1:numel(markerPosFiles)
    markerPosFilename        = [rootFolder filesep markerPosFiles(iFrame).name];
    fid = fopen(markerPosFilename);
    numMarkers = str2double(fgetl(fid));
    assert(numMarkers == numExpectedMarkers,...
        ['Number of joints is not ' num2str(numExpectedMarkers)...
        '! Check joint position files.']);
    A = textscan(fid,'%f %f %f\n',numMarkers);
    mPos(1,:) = A{1};
    mPos(2,:) = A{2};
    mPos(3,:) = A{3};
    
    gtPos = squeeze(imdb.images.labels(:,:,1:numExpectedMarkers*3,iFrame));
    gtPos = reshape(gtPos,3,[]);
    
%     bbox = imdb.meta.bbox(iFrame,:);
    bbox = 1000*imdb.meta.bbox(iFrame,:);
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = -abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],1,numMarkers);
    gtPos = gtPos.*boxMultiplier;
    boxOffset = repmat([bbox(1);-bbox(3);bbox(5)],1,numMarkers);
    gtPos = gtPos+boxOffset;
    
    posDiff = mPos - gtPos;
    posDist = sqrt(posDiff(1,:).^2+posDiff(2,:).^2+posDiff(3,:).^2);
%     distances(iFrame,:) = posDist;
    distances(iFrame,:) = posDist/1000;
    fclose(fid);
    iFrame
%     % plot
%     plot3(mPos(1,:),mPos(2,:),mPos(3,:),'b*')
%     hold on
%     plot3(gtPos(1,:),gtPos(2,:),gtPos(3,:),'g*')
%     hold off
%     line([mPos(1,:);gtPos(1,:)],[mPos(2,:);gtPos(2,:)],[mPos(3,:);gtPos(3,:)],'color','k')
%     text(double(gtPos(1,:)),double(gtPos(2,:)),double(gtPos(3,:))...
%         ,num2str(posDist'));
%     title(['GT-markers (green), estimated markers (blue). Average error: ' num2str(mean(posDist)) 'm'])
%     axis image tight
%     view(2)
%     drawnow
%     pause(0.1)
    
    
    
end
%%
cDistances = zeros(size(distances),'single');
validDistances = ~isnan(distances);
cDistances(validDistances) = distances(validDistances);
avgDistPerMarker = sum(cDistances,1)./sum(validDistances,1);

% precision
precisionPerMarker = sum(cDistances < 0.1 & validDistances,1)./sum(validDistances,1);

plot(distances)
% 
% % body parts for 17 joints:
% numJoints = 17;
% bodyParts = {...
%     'Bellybutton','Chest','Head','Pelvis','LFoot',...
%     'LElbow','LWrist','LKnee','LShoulder','LHip',...
%     'Neck','RFoot','RElbow','RWrist','RKnee',...
%     'RShoulder','RHip'}';
% bodyPartIDs = 1:numJoints;
% 
% % shape parameters
% numShapeParams = 18;
% 
% % Define test set
% testIndices = find(imdb.images.set == 2);
% 
% iFrame = 1;
% % Generate estimations, store to file
% for iSample = testIndices
%     im = imdb.images.data(:,:,3,iSample);
% %     lbl = imdb.images.labels(:,:,:,iSample);
% %     lbl = cat(3,lbl,ones(1,1,130-54,'single'));
% %     net.layers{end}.class = gpuArray(lbl);
%     res = vl_simplenn_f(net, gpuArray(im), [], [], ...
%       'disableDropout', true, ...
%       'conserveMemory', true, ...
%       'sync', false) ;
%     joints = squeeze(gather(res(end).x(1:3*numJoints)));   
%     bbox = imdb.meta.bbox(iSample,:);
%     dX = abs(bbox(1)-bbox(2));
%     dY = abs(bbox(3)-bbox(4));
%     dZ = abs(bbox(5)-bbox(6));
%     boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
%     joints = joints.*boxMultiplier;
%     boxOffset = repmat([bbox(1);bbox(3);bbox(5)],numJoints,1);
%     joints = joints+boxOffset;
%     % scale in meters (EVAL bbox is already in meters)
% %     joints = joints / 1000;
%     % reshape
%     joints = reshape(joints,3,[]);
%     
%     filename = sprintf('%s%s%s%sestimatedJoints_EVALframe_%06d.txt',rootFolder,filesep,'joints',filesep,iFrame);
%     fID = fopen(filename,'w');
%     fprintf(fID,'%d\n',numJoints);
%     for iJ = bodyPartIDs
%         fprintf(fID,'%s %f %f %f\n',bodyParts{iJ},joints(1,iJ),joints(2,iJ),joints(3,iJ));
%     end
%     fclose(fID);
%     
%     % shape
%     shapeVec = squeeze(gather(res(end).x(3*numJoints+(1:numShapeParams))));
%     filename = sprintf('%s%s%s%sestimatedShape_EVALframe_%06d.txt',rootFolder,filesep,'shapes',filesep,iFrame);
%     fID = fopen(filename,'w');
%     fprintf(fID,'%d\n',numShapeParams);
%     for iS = 1:numShapeParams
%         fprintf(fID,'%f\n',shapeVec(iS));
%     end
%     fclose(fID);
%     
%     iFrame = iFrame+1
% 
% end


