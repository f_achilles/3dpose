%% Evaluate on the test set
testInds = find(imdb.images.set == 2);
N = numel(testInds);
pixelAcc = zeros(1,N);
t = zeros(1,N);
net.layers{end}.type = 'softmax';
for i=1:N
    im = imdb.images.data(:,:,:,testInds(i)) - net.meta.meanImage;
    tic
    res=vl_simplenn_scnseg(net, gpuArray(im));
    softmaxOut = gather(res(end).x);
    t(i)=toc;
    [~,classRes]=max(softmaxOut,[],3);
%     classRes=classRes-1;
    % calculate pixel accuracy
    pixelAcc(i) = sum(sum(classRes==imdb.images.labels(:,:,1,testInds(i)) & imdb.images.labels(:,:,1,testInds(i))~=0))/sum(sum(imdb.images.labels(:,:,1,testInds(i))~=0));
    disp([num2str(i) ' of ' num2str(N)])
end
disp(['Mean pixel accuracy: ' num2str(100*mean(pixelAcc)) '%.'])
disp(['Mean time per frame: ' num2str(mean(t)) ' seconds.'])

figure;
plot(sort(pixelAcc)); axis tight

[sortedAcc, sortedIndices] = sort(pixelAcc);

[maxval,maxind]=max(pixelAcc);
maxind = gather(sortedIndices(end-400));

overlapMap = jet(21);
im = imdb.images.data(:,:,:,testInds(maxind)) - net.meta.meanImage;
res=vl_simplenn_scnseg(net, gpuArray(im));
softmaxOut = gather(res(end).x);
[~,classRes]=max(softmaxOut,[],3);
figure; imshow(imdb.images.data(:,:,:,testInds(maxind))/255);
hold on
h = imshow(reshape(overlapMap(classRes(:),:), [size(classRes) 3]));
hold off
alphaM = 0.5*ones(size(imdb.images.data,1));
alphaM(classRes==1)=0;
set(h, 'AlphaData', alphaM);

figure; imagesc(classRes); axis image; caxis([0 21])
figure; imagesc(imdb.images.labels(:,:,:,testInds(maxind))); axis image; caxis([0 21])