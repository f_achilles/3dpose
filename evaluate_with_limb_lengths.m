%% test on all images: compute per-joint average absolute distance, based
% on redundant formulation with limb lengths and angles
batchSize = 1;
testIdx = find(imdb.images.set == 2);
perJointCollector   = cell(1);
avgErrCollector     = cell(1);
precisionCollector  = cell(1);
diffCollector        = cell(1);
gtJointsCollector = cell(1);
estJointsCollector = cell(1);
bboxCollector = cell(1);

% move network to GPU
net = vl_simplenn_move(net, 'gpu') ;

% % bild up limb connections
% limbDefinitions

% randTrainIdx = randperm(numel(trainIdx),numSamples);
for iTrainSample = 1:ceil(numel(testIdx)/batchSize)
    firstIdx = (iTrainSample-1)*batchSize+1;
    lastIdx = min((iTrainSample)*batchSize,numel(testIdx));
    im = imdb.images.data(:,:,:,testIdx(firstIdx:lastIdx));
%     lbl = imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
    lbl = imdb.images.labelsOverParam(:,:,:,testIdx(firstIdx:lastIdx));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_f(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', false, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x));
    GTjoints = squeeze(lbl);
    % retrieve bbox measurements
    bbox = imdb.meta.bbox(testIdx(firstIdx:lastIdx),:);
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
    % calc per joint error
    diff = joints-GTjoints;
    % only compare pose, not length or angles
    diffPose = diff(1:54);
    diffsq = (diffPose.*(boxMultiplier)).^2;
    
    % from length and angles, regress pose, compare to GT
    
    % multiply each limb with its length
    
    % call Breitensuche algorithm recursively, starting at the root joint,
    % calculating its limbs' end joints' positions, storing their indices
    % as new root joints. Is there a list-object in matlab?
    
%     diffCollector{end+1}=diffPose;
%     sumPerJoint = diffsq(1:3:end) + diffsq(2:3:end) + diffsq(3:3:end);
%     perJointCollector{end+1} = sumPerJoint;
%     avgPrecision = nnz(sqrt(sumPerJoint(~isnan(sumPerJoint)))<100)/nnz(~isnan(sumPerJoint));
%     precisionCollector{end+1} = avgPrecision;
%     avgErr = sum(sqrt(sumPerJoint(~isnan(sumPerJoint))))/nnz(~isnan(sumPerJoint));
%     avgErrCollector{end+1} = avgErr;
    
    % store estimated joints
    estJointsCollector{end+1} = joints(1:3*numJoints);
    gtJointsCollector{end+1} = GTjoints(1:3*numJoints);
    bboxCollector{end+1} = bbox;
    fprintf('tested frame %d of %d, avg dist: %5.3fmm\n',iTrainSample,numel(testIdx),avgErr);
    
end

estJointsCollector=estJointsCollector(2:end);
gtJointsCollector=estJointsCollector(2:end);
bboxCollector=estJointsCollector(2:end);
save('skeletonTestData','bboxCollector','estJointsCollector','gtJointsCollector')
% http://de.mathworks.com/help/matlab/ref/cameratoolbar.html

% avgErrCollector=avgErrCollector(2:end);
% perJointCollector=perJointCollector(2:end);
% precisionCollector=precisionCollector(2:end);
% diffCollector=diffCollector(2:end);
% 
% avgErr = mean([avgErrCollector{:}])
% avgPrecision = mean([precisionCollector{:}])
% perPartAverage = mean(sqrt([perJointCollector{:}]),2);
% perPartPrecision = mean(sqrt([perJointCollector{:}])<100,2);
% 
% figure; plot([avgErrCollector{:}]/10); title('Average joint error over the test set in [cm]')
% axis tight
% figure; hist([avgErrCollector{:}]/10,100); title('Histogram of the average joint error in [cm]')
% axis tight
% 
% for thr = 1:30;
%     inside(thr) = nnz([avgErrCollector{:}]/10<=thr);
% end
% figure; plot(inside/numel(testIdx)); title('Percentage of the average joint error below ''thr''[cm]')
% xlabel('thr')
% ylabel('Percentage of frames in which average error is below ''thr''')
% axis tight