%% test code for image coordinate directions

N = size(imdb.images.data,4);

figure
for i=1:N
    subplot(231)
    imagesc(imdb.images.data(:,:,1,i));
    axis image
    title('X coordinate')
    subplot(232)
    imagesc(imdb.images.data(:,:,2,i));
    axis image
    title('Y coordinate')
    subplot(233)
    imagesc(imdb.images.data(:,:,3,i));
    axis image
    title('Z coordinate')
    
    subplot(234)
    S = squeeze(imdb.images.labels(:,:,:,i));
    plot3(S(1:3:end),1-S(3:3:end),S(2:3:end),'r*');
    axis image
    view([0,-1,0])
    title('Z inwards')
    
    subplot(235)
    S = squeeze(imdb.images.labels(:,:,:,i));
    plot3(S(1:3:end),S(3:3:end),1-S(2:3:end),'r*');
    axis image
    view([0,-1,0])
    title('Z outwards')
    
    pause
end

%% flip Y and Z images so to have Z coming out of the camera
% necessary for EVAL#done and PDT#done
%
% not necessary for SMMC, BMHAD, Synth

FGpixels = imdb.images.data(:,:,3,:) ~= 0;
imdb.images.data(:,:,2:3,:)         = bsxfun(@times,(1-imdb.images.data(:,:,2:3,:)),FGpixels);
% this formula is correct but produces very (!) ugly borders (same for
% augmentation-flipping, either fix this or generate flipped maps at beginning or store masks or only use Z-channel, no problem there)

imdb.images.labels(:,:,3:3:end,:)   = 1+imdb.images.labels(:,:,3:3:end,:);
imdb.images.labels(:,:,2:3:end,:)   = 1+imdb.images.labels(:,:,2:3:end,:);