function [XYZs,joints] = plotRedundantPose(label,limbIdx)
    numJoints = 17;
    label = reshape(label,[],1);
    joints = label(1:3*numJoints);
    joints = reshape(joints,3,[]);
    % plot the regular skeleton
%     drawEvalSkel(reshape(joints',[],1),'*b','3d','Synth17',limbIdx);
    % build up the angles+lenghts-skeleton
    lengths = label(70:87);
    angles = label(88:141);
    cLL = reshape(reshape(repmat(lengths',3,1),[],1) .* angles,3,[]);
    % find root joint
    rootNotFound = true;
    cRootIdx = limbIdx(1,1); % root ends are in row 1
    while rootNotFound
        cLimb = find(cRootIdx == limbIdx(2,:),1);
        if ~isempty(cLimb)
            cRootIdx = limbIdx(1,cLimb);
        else
            rootNotFound = false;
        end
    end
    % cRootIdx was now found, it is the index of the root joint
    limbs = find(cRootIdx == limbIdx(1,:));
    childrenJointInds = limbIdx(2,limbs);
    XYZs = repmat([0;0;0],1,numJoints);
    while ~isempty(childrenJointInds)
        XYZs(:,childrenJointInds) = XYZs(:,limbIdx(1,limbs))+cLL(:,limbs);
%         for iLimb = limbs
%             hold on
%             line([XYZs(1,limbIdx(1,iLimb)), XYZs(1,limbIdx(2,iLimb))],...
%                  [XYZs(2,limbIdx(1,iLimb)), XYZs(2,limbIdx(2,iLimb))],...
%                  [XYZs(3,limbIdx(1,iLimb)), XYZs(3,limbIdx(2,iLimb))]);
%             hold off
%         end
        limbs = [];
        for iChild = childrenJointInds
            limbs = [limbs find(iChild == limbIdx(1,:))];
        end
        childrenJointInds = limbIdx(2,limbs);
    end
%     hold on
%     drawEvalSkel(reshape(XYZs',[],1),'+b','3d','Synth17',limbIdx);
%     hold off

end