function imdbAll = imdbAppendBlankets(imdbAll, imdb)

% while we are at it, correct the "seizure" of Felix to movementsH

szIndsBool = find(strcmp({imdbAll.meta.info.sequence},'seizure'));
for iCorr = 1:numel(szIndsBool)
    imdbAll.meta.info(szIndsBool(iCorr)).sequence = 'movementsH';
end

imdb.images.data = imdb.images.data/1000;
imdb.images.labels = zeros(1,1,42,9990);
% fill labels of new imdb
movementIndsBool = strcmp({imdbAll.meta.info.sequence},'movementsH');
subjects  = {'Leslie','Felix','Severine','Huseyin','Julia','Philipp','Ashley','Dario','Mona','Meng'};
for iSubj = 1:10
    subjIndsBool = strcmp({imdbAll.meta.info.actor},subjects{iSubj});
    validInds = find(subjIndsBool & movementIndsBool);
    numel(validInds)
%     imdb.images.labels(:,:,:,(iSubj-1)*999 + (1:999)) = imdbAll.images.labels(:,:,:,validInds(1:999));
    subjIndsBoolNew = strcmp({imdb.meta.info.actor},subjects{iSubj});
    imdb.images.labels(:,:,:,subjIndsBoolNew) = imdbAll.images.labels(:,:,:,validInds(1:999));
end

% combine old and new IMDB
imdbAll.images.data     = cat(4, imdbAll.images.data, imdb.images.data);
imdbAll.images.labels   = cat(4, imdbAll.images.labels, imdb.images.labels);
% imdbAll.images.set      = cat(2, imdbAll.images.set, imdb.images.set);

% imdbAll.meta.bbox           = cat(1, imdbAll.meta.bbox, imdb.meta.bbox);
% imdbAll.meta.sequenceID     = cat(2, imdbAll.meta.sequenceID, imdb.meta.sequenceID);

imdbAll.meta.info = cat(2, imdbAll.meta.info, imdb.meta.info);
end