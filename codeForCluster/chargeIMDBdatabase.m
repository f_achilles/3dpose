function imdbSep = chargeIMDBdatabase(IPaddress,IPs)
% load imdb if not defined
sepImdbs    =   {
                'imdb_view_center','imdb_view_angle_000','imdb_view_angle_036';
                'imdb_view_angle_072','imdb_view_angle_108','imdb_view_angle_144';
                'imdb_view_angle_180','imdb_view_angle_216','imdb_view_angle_252'
                };
% sharedImdbs =   {'imdb_view_angle_288','imdb_view_angle_324'};
whoAmI = find(strcmp(IPs,IPaddress));

% load shared imdbs, clip part that is used by this client
if exist('sharedImdbs','var')
    for iShr = 1:numel(sharedImdbs)
        load(fullfile('~/data',sharedImdbs{iShr}));
        fprintf('loaded %s (shared), %d frames\n',...
            sharedImdbs{iShr},size(imdb.images.data,4));
        if iShr == 1
            imdbShr = imdb;
        else
            imdbShr = imdbAppend(imdbShr, imdb);
        end
    end
    clear imdb
    imdbShr.images.set(:) = 3;
    N = size(imdbShr.images.data,4);
    sizeChunks = floor(N/numel(IPs));
    myStart = 1+sizeChunks*(whoAmI-1);
    myEnd = myStart+sizeChunks-1;
    imdbShr.images.set(myStart:myEnd) = 1;
    sharedPart_bl = imdbShr.images.set == 1;
    %reduce imdbShr size
    imdbShr.images.data     = imdbShr.images.data(:,:,:,sharedPart_bl);
    imdbShr.images.labels   = imdbShr.images.labels(:,:,:,sharedPart_bl);
    imdbShr.images.set      = imdbShr.images.set(sharedPart_bl);
    imdbShr.meta.bbox       = imdbShr.meta.bbox(sharedPart_bl,:);
    imdbShr.meta.sequenceID = imdbShr.meta.sequenceID(sharedPart_bl);
    fprintf('Size of clipped shared imdb: %d frames\n',...
        size(imdbShr.images.data,4));
end
% load separate imdbs
for iSep = 1:size(sepImdbs,2)
    load(fullfile('~/data',sepImdbs{whoAmI,iSep}));
    fprintf('loaded %s (separate), %d frames\n',...
        sepImdbs{whoAmI,iSep},size(imdb.images.data,4))
    if iSep == 1
        imdbSep = imdb;
    else
        imdbSep = imdbAppend(imdbSep, imdb);
    end
end
clear imdb
imdbSep.images.set(:) = 1;
N = size(imdbSep.images.data,4);
valRate = 0.05;
indVal = floor(valRate*N);
imdbSep.images.set(1:indVal) = 2;
fprintf('Size of separate imdb: %d frames\n',...
    size(imdbSep.images.data,4));
if exist('sharedImdbs','var')
    imdbSep = imdbAppend(imdbSep, imdbShr);
    clear imdbShr
end

end