function [meanErr, meanPerJoint] = testCNNwithParametersBatchBlanket(imdb,testSubjects,net)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

%% what is this section 01 for?
% imdb.images.set(strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;
% 
% if iscell(testSubjects)
%     validSubj1 = strcmp({imdb.meta.info.actor},testSubjects{1});
%     validSubj2 = strcmp({imdb.meta.info.actor},testSubjects{2});
%     imdb.images.set(validSubj1) = 2;
%     imdb.images.set(validSubj2) = 2;
% else
%     testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
%     if ~any(testVecBinary)
%         error('test subject not found')
%     end
%     imdb.images.set(testVecBinary) = 2;
% end
% 
% % subtract average
% net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
% imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);
% 
% % find too high frames
% invalidFramesBool = [imdb.meta.info.frame]>999;
% % make too high frames not tested for the actors
% imdb.images.set(invalidFramesBool & (validSubj1 | validSubj2)) = 3;
% 
% % only test on movements
% invalidSeqsBool = ~strcmp({imdb.meta.info.sequence},'movementsH');
% imdb.images.set(invalidSeqsBool) = 3;
% 
% % test on NON blanket only
% imdb.images.set(strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;
%% what is this section 02 for?

if iscell(testSubjects)
    validSubj1 = strcmp({imdb.meta.info.actor},testSubjects{1});
    validSubj2 = strcmp({imdb.meta.info.actor},testSubjects{2});
    imdb.images.set(validSubj1) = 2;
    imdb.images.set(validSubj2) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

% only train/test on blanket movements, not the normal ones
invalidSeqsBool = strcmp({imdb.meta.info.sequence},'movementsH');
imdb.images.set(invalidSeqsBool) = 3;

% subtract average
% net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
% imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);
% low memory version
blocksz = 1000;
trainInds = find(imdb.images.set == 1);
numBlocks = (ceil(numel(trainInds)/blocksz));
meanStack = zeros(120,60,numBlocks);
for iBlock = 1:numBlocks
    batch = (iBlock-1)*blocksz + (1:min(blocksz,numel(trainInds)-(iBlock-1)*blocksz));
    meanStack(:,:,iBlock) = squeeze(mean(imdb.images.data(:,:,:,trainInds(batch)),4));
end
net.stats.average = mean(meanStack,3);

%%%
% uncomment below for non-blanket movementsH-evaluation
%%%
% % only train/test on blanket movements, not the normal ones
% invalidSeqsBool = strcmp({imdb.meta.info.sequence},'movementsH');
% imdb.images.set(invalidSeqsBool & (validSubj1 | validSubj2)) = 2;
% 
% % find too high frames
% invalidFramesBool = [imdb.meta.info.frame]>999;
% % make too high frames not tested for the actors
% imdb.images.set(invalidFramesBool & (validSubj1 | validSubj2)) = 3;
% 
% % only test on movements
% invalidSeqsBool = ~strcmp({imdb.meta.info.sequence},'movementsH');
% imdb.images.set(invalidSeqsBool) = 3;
% 
% % test on NON blanket only
% imdb.images.set(strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;
%%%
% comment until here
%%%

%%%
% comment below for non-blanket movementsH-evaluation
%%%
% test on blanket only
imdb.images.set(~strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;
%%%
% comment until here
%%%

net.layers = net.layers(1:(end-1));
net = vl_simplenn_move(net,'gpu');
testInds = find(imdb.images.set==2);

batchSz = 1;

jointsErrors = NaN(14,numel(testInds));

exectime = zeros(1,ceil(numel(testInds)/batchSz));

for iB = 1:(ceil(numel(testInds)/batchSz))
    batch = (iB-1)*batchSz + (1:min(batchSz,numel(testInds)-(iB-1)*batchSz));
    numel(batch)
%     res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,testInds(batch))),...
%         [],[],'disableDropout', true, 'conserveMemory', true);
tic
    res = vl_simplenn_f(net,gpuArray(bsxfun(@minus,imdb.images.data(:,:,:,testInds(batch)),net.stats.average)),...
        [],[],'disableDropout', true, 'conserveMemory', true);
exectime(iB) = toc;
    estimJoints = gather(res(end).x);
    estimJoints = reshape(estimJoints,3,14,numel(batch));
    gtJoints =imdb.images.labels(:,:,:,testInds(batch));
    gtJoints = reshape(gtJoints,3,14,numel(batch));
    resid = estimJoints-gtJoints;
    sssd = sqrt(sum(resid.^2,1)); %1x14xbatchSz
    
    jointsErrors(:,batch) = squeeze(sssd);
    
    validJoints = ~isnan(sssd);
    err(iB) = mean(reshape(sssd(validJoints),1,[]));
    fprintf('%d frames processed... average error: %f cm.\n',max(batch),mean(err)*100)
end

meanErr = mean(err);

Joints = {'Head','LeftHip','RightHip','Neck','LeftShoulder','RightShoulder','LeftElbow','RightElbow','LeftHand','RightHand','LeftKnee','RightKnee','LeftFoot','RightFoot'};
for iJoint = 1:14
    meanPerJoint(iJoint) = mean(jointsErrors(iJoint,~isnan(jointsErrors(iJoint,:))));
end

JointError = meanPerJoint;

table(reshape(JointError,[],1),'RowNames',reshape(Joints,[],1))


fprintf('Average execution time: %d milliseconds.', mean(exectime)*1000)
% result was 1.65 ms on June 16th, 2016
end