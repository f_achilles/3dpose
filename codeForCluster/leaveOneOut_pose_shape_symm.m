% train cnn_pose3D with shape estimation in various configurations and
% leave-one-out on the sequences
function net = leaveOneOut_pose_shape_symm(IPaddress,imdb)
	IPs = {'10.90.40.3','10.90.40.2','10.90.40.4'};%, '10.90.40.4'};
	whoAmI = find(strcmp(IPs,IPaddress));
    valRate = 0.05;
	N = size(imdb.images.data,4);
	imdb.images.set(:) = 3;
	indVal = floor(valRate*N);
	imdb.images.set(1:indVal) = 2;
	sizeChunks = floor((N-indVal)/numel(IPs));
	myStart = indVal+1+sizeChunks*(whoAmI-1);
	myEnd = myStart+sizeChunks-1;
    imdb.images.set(myStart:myEnd) = 1;
    opts.dataDir = fullfile('~','trainOnCenterView224k_Z_Aug_better');
	
	opts.train.IPlist = IPs;
	opts.train.expDir = opts.dataDir;
    opts.train.numEpochs = 75 ;
    opts.train.learningRate = 1e-1 ;
    opts.train.numSubBatches = 1 ;
    opts.train.batchSize = 1000 ;
	opts.train.continue = true ;
	if ischar(IPaddress)
	opts.train.IP = IPaddress;
	else
	error('Enter an IP-Address as String')
	end
    [net, info] = cnn_pose3D_symm(imdb,opts);
end

