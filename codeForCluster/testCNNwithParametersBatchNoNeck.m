function meanErr = testCNNwithParametersBatchNoNeck(imdb,testSubjects,net)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

if iscell(testSubjects)
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{1})) = 2;
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{2})) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);
net.layers = net.layers(1:(end-1));
net=vl_simplenn_move(net,'gpu');
testInds = find(imdb.images.set==2);

batchSz = 1000;

for iB = 1:(ceil(numel(testInds)/batchSz)-1)
    batch = (iB-1)*batchSz + (1:min(batchSz,numel(testInds)-(iB-1)*batchSz));
    res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,testInds(batch))),...
        [],[],'disableDropout', true, 'conserveMemory', true);
    estimJoints = gather(res(end).x);
    estimJoints = reshape(estimJoints,3,14,numel(batch));
    gtJoints =imdb.images.labels(:,:,:,testInds(batch));
    gtJoints = reshape(gtJoints,3,14,numel(batch));
    resid = estimJoints-gtJoints;
    sssd = sqrt(sum(resid.^2,1)); %1x14xbatchSz
    % remove joint number 4
    sssd = sssd(1,[1:3 5:14],:);
    validJoints = ~isnan(sssd);
    err(iB) = mean(reshape(sssd(validJoints),1,[]));
    fprintf('%d frames processed... average error: %f cm.\n',max(batch),mean(err)*100)
end

meanErr = mean(err);

end