% train cnn_pose3D with shape estimation in various configurations and
% leave-one-out on the sequences
function net = distribCNNtraining_pose_shape(IPaddress,imdb)
    IPs = {'10.90.40.2','10.90.40.3','10.90.40.4'};
% 	IPs = {'10.90.40.2','127.0.0.1','127.0.0.1','10.90.40.3','10.90.40.4'};
% 	% if the ports 2&3 are blocked by another training
    
% load imdb if not defined
if ~exist('imdb','var')
imdb = chargeIMDBdatabase(IPaddress,IPs);
end

    opts.dataDir = fullfile('~','trainOn9views_2M_Z_flip_noise');
    opts.train.gpus = [1];
	opts.train.IPlist = IPs;
	opts.train.expDir = opts.dataDir;
    opts.train.numEpochs = 75 ;
    opts.train.learningRate = 1e-1 ;
    opts.train.numSubBatches = 1 ;
    opts.train.batchSize = 1000 ;
	opts.train.continue = true ;
	if ischar(IPaddress)
	opts.train.IP = IPaddress;
	else
	error('Enter an IP-Address as String')
	end
    [net, info] = cnn_pose3D_symm2(imdb,opts);
end

