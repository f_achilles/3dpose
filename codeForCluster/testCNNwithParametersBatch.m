function meanErr = testCNNwithParametersBatch(imdb,testSubjects,net)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

if iscell(testSubjects)
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{1})) = 2;
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{2})) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

% test on NON blanket only
imdb.images.set(strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;

net.meta.averageFrame = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.meta.averageFrame);
net.layers = net.layers(1:(end-1));
net=vl_simplenn_move(net,'gpu');
testInds = find(imdb.images.set==2);

batchSz = 1000;

jointsErrors = NaN(14,numel(testInds));

for iB = 1:(ceil(numel(testInds)/batchSz))
    batch = (iB-1)*batchSz + (1:min(batchSz,numel(testInds)-(iB-1)*batchSz));
    res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,testInds(batch))),...
        [],[],'disableDropout', true, 'conserveMemory', true);
    estimJoints = gather(res(end).x);
    estimJoints = reshape(estimJoints,3,14,numel(batch));
    gtJoints =imdb.images.labels(:,:,:,testInds(batch));
    gtJoints = reshape(gtJoints,3,14,numel(batch));
    resid = estimJoints-gtJoints;
    sssd = sqrt(sum(resid.^2,1)); %1x14xbatchSz
    
    jointsErrors(:,batch) = squeeze(sssd);
    
    validJoints = ~isnan(sssd);
    err(iB) = mean(reshape(sssd(validJoints),1,[]));
    fprintf('%d frames processed... average error: %f cm.\n',max(batch),mean(err)*100)
end

meanErr = mean(err);

% Joints = {'Head','LeftHip','RightHip','Neck','LeftShoulder','RightShoulder','LeftElbow','RightElbow','LeftHand','RightHand','LeftKnee','RightKnee','LeftFoot','RightFoot'};
% for iJoint = 1:14
%     meanPerJoint(iJoint) = mean(jointsErrors(iJoint,~isnan(jointsErrors(iJoint,:))));
% end

maxPerFrame = zeros(1,numel(testInds));
for iFr = 1:numel(testInds)
    maxPerFrame(iFr) = max(jointsErrors(:,iFr));
end

% JointError = meanPerJoint;
JointError = maxPerFrame;

save('/home/achilles/eval/maxJointErrorPerFrame_nonBlanket_CNN.mat','JointError')
% table(reshape(JointError,[],1),'RowNames',reshape(Joints,[],1))

end