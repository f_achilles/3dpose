% calculate angles and limb lengths

% fake skeleton:
lbl = rand(1,1,18*3,1,'single');

% define limbs {root->distal}:
%center
NeckHead = {'Neck','Head'};
Thorax = {'Chest','Neck'};
Abdomen = {'Chest','Bellybutton'};
AbdomenLow = {'Bellybutton','Pelvis'};
%left
LCollarbone = {'Neck','LShoulder'};
LUpperarm = {'LShoulder','LElbow'};
LForearm = {'LElbow','LWrist'};
LHand = {'LWrist','LFingers'};
LLat = {'Chest','LShoulder'};
LPsoas = {'Bellybutton','LHip'};
LPsoas17 = {'Pelvis','LHip'};
LThigh = {'LHip','LKnee'};
LShin = {'LKnee','LFoot'};
%right
RCollarbone = {'Neck','RShoulder'};
RUpperarm = {'RShoulder','RElbow'};
RForearm = {'RElbow','RWrist'};
RHand = {'RWrist','RFingers'};
RLat = {'Chest','RShoulder'};
RPsoas = {'Bellybutton','RHip'};
RPsoas17 = {'Pelvis','RHip'};
RThigh = {'RHip','RKnee'};
RShin = {'RKnee','RFoot'};

% % body parts for 18 joints:
% numJoints = 18;
% bodyParts = {...
%     'Chest','LFoot','RFoot','LElbow','RElbow',...
%     'LWrist','RWrist','Head','Bellybutton','LFingers',...
%     'RFingers','Neck','LKnee','RKnee','LHip',...
%     'RHip','LShoulder','RShoulder'}';
% bodyPartIDs = 1:numJoints;

% limbs when we have 18 joints
% limbs = {...
%     'NeckHead','Thorax','Abdomen','LCollarbone','RCollarbone',...
%     'LUpperarm','RUpperarm','LForearm','RForearm','LHand','RHand',...
%     'LLat','RLat','LPsoas','RPsoas','LThigh','RThigh','LShin','RShin'};
% 19 limbs

% body parts for 17 joints:
numJoints = 17;
bodyParts = {...
    'Bellybutton','Chest','Head','Pelvis','LFoot',...
    'LElbow','LWrist','LKnee','LShoulder','LHip',...
    'Neck','RFoot','RElbow','RWrist','RKnee',...
    'RShoulder','RHip'}';
bodyPartIDs = 1:numJoints;

% limbs when we have 17 joints
limbs = {...
    'NeckHead','Thorax','Abdomen','AbdomenLow',...
    'LCollarbone','RCollarbone','LUpperarm','RUpperarm','LForearm','RForearm',...
    'LLat','RLat','LPsoas17','RPsoas17','LThigh','RThigh','LShin','RShin'};
% (18 limbs)

% it is easy to calculate the length of each limb, for each GT-Sample, and
% to update the IMDB-label with the calculated values
%
% as a second step, it is necessary to compute a penalty for non-symmetric
% bodies. the euclidean distance objective function needs to be altered
% accordingly.
%
% thirdly, the limb-angles need to be well-defined. first we define them
% based on the camera XYZ coordinate system and as numbers between -1 and 1
% so to cope with circular coordinates by wrapping the output into cos/sine
% functions. each limb is rotated in R2 by phi and theta. phi lies in the
% xy plane and in a naive setting is 0 in xaxis, 1 in yaxis, 0 in -xaxis,
% -1 in -yaxis direction by putting a sine(phi360) over it. this assignment
% is however not unique, such that we quantify phi360 over one sine() and
% one cos() function. theta rotates into zaxis 90 or against zaxis -90,
% which when put to a sine(theta) function results in values between -1 and
% 1, such that no non-uniqueness can occur.
% 
% when calculating the limb-length, the limb has to be computed as a vector
% poiting from root to distal end. this vector can be used an normalized,
% in order to be then converted to the desired representation:
% z_norm = sin(theta)
% y_norm = sin(phi)
% x_norm = cos(phi)

% compute limb-indices
nLimbs = numel(limbs);
limbIdx = zeros(2,nLimbs,'uint8');
for iLimb = 1:nLimbs
    cLimb = evalin('base',limbs{iLimb});
    limbIdx(:,iLimb) = [find(strcmp(cLimb{1},bodyParts));find(strcmp(cLimb{2},bodyParts))];
end
% % compute limb-lengths AND limb-angular representations
% limbLengths = zeros(1,1,nLimbs,1,'single');
% limbAngles = zeros(1,1,3*nLimbs,1,'single');
% for iLimb = 1:nLimbs
%     limbVec = ...
%         -lbl((limbIdx(1,iLimb)-1)*3+1:limbIdx(1,iLimb)*3)...
%         +lbl((limbIdx(2,iLimb)-1)*3+1:limbIdx(2,iLimb)*3);
%     limbLengths(iLimb) = norm(limbVec(:));
%     limbAngles((iLimb-1)*3+1:iLimb*3) = limbVec/limbLengths(iLimb);
% end
% % stack the representations
% lbl = cat(3,lbl,limbLengths,limbAngles);
% 
% % when we have computed the new parameterization, both the XYZ joint
% % positions and the lengths and corresponding R2-angles will form a
% % redundant representation, where from lengths and angles the correct human
% % pose can be built except for a translational component. it might be
% % claimed that the resulting representation is translation-invariant, which
% % makes is a good partner for deep neural networks.
% %
% % a next stage in terms of over-parameterization would be the pose, but
% % lacking global translation as well as rotation. it can be built up by
% % estimating all limb-lengths and all angles inbetween two adjacent limbs.
% % the angles however need to be based on a two-dimensional basis, which
% % needs to be formed by the body joints. as we have no rigid
% % two-dimensional plane in our body joint configuration, this would only be
% % possible as an approximation. NO! what am I saying, it would be possible
% % also without a fixed coordinate system. all that needs to be done is a)
% % store the angle between two adjacent joints. what is left now is the
% % degree of freedom about which the parent joint can rotate. if the child
% % joint is projected on the plane of the parent joint, this degree of
% % freedom is a 360 degree rotation inside that plane. we can allow any
% % anlge here as long as it is consistent with the other angles, and
% % describes the viewed input image. In the end the created parameterization
% % will be correct up to one free parameter, which will be the starting
% % angle on the plane, based on the current camera view.
% 
% do the parameterization batch-wise on a full imdb dataset
N=size(imdb.images.data,4);
for i=1:N
    imdb.images.labelsOverParam(1,1,:,i) = ...
        overparamXYZpose(imdb.images.labels(1,1,:,i),limbIdx);
end
