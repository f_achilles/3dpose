function [net, info] = cnn_train_f_multiclientICT(net, imdb, getBatch, varargin)
% CNN_TRAIN   Demonstrates training a CNN
%    CNN_TRAIN() is an example learner implementing stochastic
%    gradient descent with momentum to train a CNN. It can be used
%    with different datasets and tasks by providing a suitable
%    getBatch function.
%
%    The function automatically restarts after each training epoch by
%    checkpointing.
%
%    The function supports training on CPU or on one or more GPUs
%    (specify the list of GPU IDs in the `gpus` option). Multi-GPU
%    support is relatively primitive but sufficient to obtain a
%    noticable speedup.

% Copyright (C) 2014-15 Andrea Vedaldi.
% All rights reserved.
%
% This file is part of the VLFeat library and is made available under
% the terms of the BSD license (see the COPYING file).

opts.batchSize = 9 ;
opts.numSubBatches = 1 ;
opts.train = [] ;
opts.val = [] ;
opts.numEpochs = 300 ;
opts.gpus = 1 ; % which GPU devices to use (none, one, or more)
opts.learningRate = 0.001 ;
opts.continue = true ;
opts.expDir = sprintf('C:\\Projects\\SeizureDetection\\data\\networks\\motionTubes_LR500'); %fullfile('data','exp') ;
opts.conserveMemory = false ;
opts.backPropDepth = +inf ;
opts.sync = false ;
opts.prefetch = false ;
opts.weightDecay = 0.0005 ;
opts.momentum = 0.9 ;
opts.errorFunction = 'multiclass' ;
opts.errorLabels = {} ;
opts.plotDiagnostics = false ;
opts.memoryMapFile = fullfile(tempdir, 'matconvnet.bin') ;
opts.IP = '127.0.0.1';
opts.IPlist = {'127.0.0.1','127.0.0.2'};
opts = vl_argparse(opts, varargin) ;
% --
% set up TCP/IP communication
% --
% IPs = {'131.159.10.89','131.159.204.3'};
% IPs = {'131.159.10.89','131.159.10.139'};
IPs = opts.IPlist; %{'10.90.40.3','10.90.40.2','10.90.40.4'};%,'10.90.40.4'};
opts.numClients = numel(IPs);
% get my own address
% address = java.net.InetAddress.getLocalHost;
% IPaddress = char(address.getHostAddress);
% TODO: get the IP list from the opts-structure, so that it is configurable
% from the outside

IPaddress = opts.IP;
% am I the master?
opts.master = strcmp(IPaddress,IPs{1});

% establish connections
if opts.master
    disp('I am the master!')
    % chack if parallel pool exists
    if isempty(gcp('nocreate'))
        parpool(opts.numClients-1);
    end
    for iSlave = 1:(opts.numClients-1)
        % enables duplex communication with each slave!
%         opts.sendObj{iSlave} = tcpip(IPs{iSlave+1},42000+iSlave,'NetworkRole','client');
        opts.sendObj{iSlave} = tcpip(IPs{iSlave+1},42000+iSlave);
        set(opts.sendObj{iSlave},'OutputBufferSize',4*9e6);
        set(opts.sendObj{iSlave},'InputBufferSize',4*9e6);
        set(opts.sendObj{iSlave},'Timeout',10);
        % receive connections
        opts.receiveObj{iSlave} = tcpip(IPs{iSlave+1},42000+iSlave,'NetworkRole','server');
%         opts.receiveObj{iSlave} = tcpip(IPs{iSlave+1},42000+iSlave);
        set(opts.receiveObj{iSlave},'OutputBufferSize',4*9e6);
        set(opts.receiveObj{iSlave},'InputBufferSize',4*9e6);
        set(opts.receiveObj{iSlave},'Timeout',10);

        fprintf('Connections to slave %d (%s) established!\n', iSlave, IPs{iSlave+1});
    end
else
    opts.mySlaveNumber = find(strcmp(IPaddress,IPs),1)-1;
    fprintf('I am slave number %d!\n', opts.mySlaveNumber);
    % enables duplex communication with the master!
%     opts.sendObj = tcpip(IPs{1},42000+opts.mySlaveNumber,'NetworkRole','client');
    opts.sendObj = tcpip(IPs{1},42000+opts.mySlaveNumber);
    set(opts.sendObj,'OutputBufferSize',4*9e6);
    set(opts.sendObj,'InputBufferSize',4*9e6);
    set(opts.sendObj,'Timeout',10);
    % receive connections
    opts.receiveObj = tcpip(IPs{1},42000+opts.mySlaveNumber,'NetworkRole','server');
%     opts.receiveObj = tcpip(IPs{1},42000+opts.mySlaveNumber);
    set(opts.receiveObj,'OutputBufferSize',4*9e6);
    set(opts.receiveObj,'InputBufferSize',4*9e6);
    set(opts.receiveObj,'Timeout',10);

    fprintf('Connections to master (%s) established!\n', IPs{1});
	pause(2)
end

if opts.master
if ~exist(opts.expDir, 'dir'), mkdir(opts.expDir) ; end
end
if isempty(opts.train), opts.train = find(imdb.images.set==1) ; end
if isempty(opts.val), opts.val = find(imdb.images.set==2) ; end
if isnan(opts.train), opts.train = [] ; end

% -------------------------------------------------------------------------
%                                                    Network initialization
% -------------------------------------------------------------------------

evaluateMode = isempty(opts.train) ;

if ~evaluateMode
  for i=1:numel(net.layers)
    if isfield(net.layers{i}, 'weights')
      J = numel(net.layers{i}.weights) ;
      for j=1:J
        net.layers{i}.momentum{j} = zeros(size(net.layers{i}.weights{j}), 'single') ;
      end
      if ~isfield(net.layers{i}, 'learningRate')
        net.layers{i}.learningRate = ones(1, J, 'single') ;
      end
      if ~isfield(net.layers{i}, 'weightDecay')
        net.layers{i}.weightDecay = ones(1, J, 'single') ;
      end
    end
    % Legacy code: will be removed
    if isfield(net.layers{i}, 'filters')
      net.layers{i}.momentum{1} = zeros(size(net.layers{i}.filters), 'single') ;
      net.layers{i}.momentum{2} = zeros(size(net.layers{i}.biases), 'single') ;
      if ~isfield(net.layers{i}, 'learningRate')
        net.layers{i}.learningRate = ones(1, 2, 'single') ;
      end
      if ~isfield(net.layers{i}, 'weightDecay')
        net.layers{i}.weightDecay = single([1 0]) ;
      end
    end
  end
end

if opts.master
% send initial (random) network weights to slaves
    MSSG = mapResToVector(net.layers, 'weights');
    success = false(1,opts.numClients-1);
    parfor iSlave = 1:(opts.numClients-1)
        disp(['Opening master client now: [' datestr(now) '] (for slave ' num2str(iSlave) ')'])
        while ~success(iSlave)
            try
                fopen(opts.sendObj{iSlave});
                success(iSlave) = true;
            catch
                pause(0.1)
            end
        end
        disp(['Sending initial weights to slave ' num2str(iSlave)])
        fwrite(opts.sendObj{iSlave},MSSG,'single');
        disp(['Sending successful! (slave ' num2str(iSlave) ')'])
%         fclose(opts.sendObj{iSlave});
    end
else
% receive initial network weights from master
    disp(['Opening slave server (slave ' num2str(opts.mySlaveNumber) ') now: (' datestr(now) ')'])
    fopen(opts.receiveObj);
    disp(['Receiving initial weights from master (slave ' num2str(opts.mySlaveNumber) ')'])
    % wait until input buffer is filled
    bufSz = -1;
    bufDiff = -1;
    while  bufDiff ~= 0 || bufSz == 0
        pause(0.1)
        bufDiff = opts.receiveObj.BytesAvailable-bufSz;
        bufSz = opts.receiveObj.BytesAvailable;
    end
    MSSG_in = fread(opts.receiveObj,bufSz/4,'single');
    disp(['Receiving successful! (slave ' num2str(opts.mySlaveNumber) ')'])
%     fclose(opts.receiveObj);
    net.layers = mapVectorToRes(single(MSSG_in),'weights',net.layers,0);
end

% setup GPUs
numGpus = numel(opts.gpus) ;
if numGpus > 1
  if isempty(gcp('nocreate')),
    parpool('local',numGpus) ;
    spmd, gpuDevice(opts.gpus(labindex)), end
  end
elseif numGpus == 1
  gpuDevice(opts.gpus)
end
if exist(opts.memoryMapFile), delete(opts.memoryMapFile) ; end

% setup error calculation function
if isstr(opts.errorFunction)
  switch opts.errorFunction
    case 'none'
      opts.errorFunction = @error_none ;
    case 'multiclass'
      opts.errorFunction = @error_multiclass ;
      if isempty(opts.errorLabels), opts.errorLabels = {'top1e', 'top5e'} ; end
    case 'binary'
      opts.errorFunction = @error_binary ;
      if isempty(opts.errorLabels), opts.errorLabels = {'bine'} ; end
    otherwise
      error('Uknown error function ''%s''', opts.errorFunction) ;
  end
end

% -------------------------------------------------------------------------
%                                                        Train and validate
% -------------------------------------------------------------------------
try
for epoch=1:opts.numEpochs
  learningRate = opts.learningRate(min(epoch, numel(opts.learningRate))) ;

  % in case the training was paused/aborted at some point:
  % fast-forward to last checkpoint
  modelPath = @(ep) fullfile(opts.expDir, sprintf('net-epoch-%d.mat', ep));
  modelFigPath = fullfile(opts.expDir, 'net-train.pdf') ;
  if opts.continue
    if exist(modelPath(epoch),'file')
      if epoch == opts.numEpochs
        load(modelPath(epoch), 'net', 'info') ;
      end
      continue ;
    end
    if epoch > 1
      fprintf('resuming by loading epoch %d\n', epoch-1) ;
	  if ~opts.master
		  while 2 ~= exist(modelPath(epoch-1),'file')
			pause(1)
		  end
		  pause(3)
		  load(modelPath(epoch-1), 'net', 'info') ;
		  pause(3)
      else
	    load(modelPath(epoch-1),'net','info');
      end
    end
  end


  % give the master time to save the old network
  % better: start listening to net master, he will broadcast when he is finished! %TODO
  if ~opts.master && ~opts.continue
	pause(5)
  end
  
  opts.continue = false;
  
  % move CNN to GPU as needed
  if numGpus == 1
    net = vl_simplenn_move(net, 'gpu') ;
  elseif numGpus > 1
    spmd(numGpus)
      net_ = vl_simplenn_move(net, 'gpu') ;
    end
  end

  % train one epoch and validate
  train = opts.train(randperm(numel(opts.train))) ; % shuffle
  val = opts.val ;
  if numGpus <= 1
    [net,stats.train] = process_epoch(opts, getBatch, epoch, train, learningRate, imdb, net) ;
    [~,stats.val] = process_epoch(opts, getBatch, epoch, val, 0, imdb, net) ;
  else
    spmd(numGpus)
      [net_, stats_train_] = process_epoch(opts, getBatch, epoch, train, learningRate, imdb, net_) ;
      [~, stats_val_] = process_epoch(opts, getBatch, epoch, val, 0, imdb, net_) ;
    end
    stats.train = sum([stats_train_{:}],2) ;
    stats.val = sum([stats_val_{:}],2) ;
  end

  % save
  if evaluateMode, sets = {'val'} ; else sets = {'train', 'val'} ; end
  for f = sets
    f = char(f) ;
    n = numel(eval(f)) ;
    info.(f).speed(epoch) = n / stats.(f)(1) ;
    info.(f).objective(epoch) = stats.(f)(2) / n ;
    info.(f).error(:,epoch) = stats.(f)(3:end) / n ;
  end
  if numGpus > 1
    spmd(numGpus)
      net_ = vl_simplenn_move(net_, 'cpu') ;
    end
    net = net_{1} ;
  else
    net = vl_simplenn_move(net, 'cpu') ;
  end
  if opts.master
  if ~evaluateMode, save(modelPath(epoch), 'net', 'info') ; end
  end

  figure(1) ; clf ;
  hasError = isa(opts.errorFunction, 'function_handle') ;
  subplot(1,1+hasError,1) ;
  if ~evaluateMode
    semilogy(1:epoch, info.train.objective, '.-', 'linewidth', 2) ;
    hold on ;
  end
  semilogy(1:epoch, info.val.objective, '.--') ;
  xlabel('training epoch') ; ylabel('energy') ;
  grid on ;
  h=legend(sets) ;
  set(h,'color','none');
  title('objective') ;
  if hasError
    subplot(1,2,2) ; leg = {} ;
    if ~evaluateMode
      plot(1:epoch, info.train.error', '.-', 'linewidth', 2) ;
      hold on ;
      leg = horzcat(leg, strcat('train ', opts.errorLabels)) ;
    end
    plot(1:epoch, info.val.error', '.--') ;
    leg = horzcat(leg, strcat('val ', opts.errorLabels)) ;
    set(legend(leg{:}),'color','none') ;
    grid on ;
    xlabel('training epoch') ; ylabel('error') ;
    title('error') ;
  end
  drawnow ;
  if opts.master
  print(1, modelFigPath, '-dpdf') ;
  % TODO: broadcast here to slaves, that master is finished storing the old network
  end
end
catch clusterError
    if opts.master
        for iSlave = 1:(opts.numClients-1)
        fclose(opts.sendObj{iSlave});
        delete(opts.sendObj{iSlave});
        
        fclose(opts.receiveObj{iSlave});
        delete(opts.receiveObj{iSlave});
        end
    else
        fclose(opts.sendObj);
        delete(opts.sendObj);
        
        fclose(opts.receiveObj);
        delete(opts.receiveObj);
    end
    rethrow(clusterError)
end

% -------------------------------------------------------------------------
function err = error_multiclass(opts, labels, res)
% -------------------------------------------------------------------------
err(1,1) =  sum(double(gather(res(end).x))) ;
err(2,1) =  sum(double(gather(res(end).x))) ;

% predictions = gather(res(end-1).x) ;
% [~,predictions] = sort(predictions, 3, 'descend') ;
% error = ~bsxfun(@eq, predictions, reshape(labels, 1, 1, 1, [])) ;
% err(1,1) = sum(sum(sum(error(:,:,1,:)))) ;
% err(2,1) = sum(sum(sum(min(error(:,:,1:5,:),[],3)))) ;

% -------------------------------------------------------------------------
function err = error_binaryclass(opts, labels, res)
% -------------------------------------------------------------------------
predictions = gather(res(end-1).x) ;
error = bsxfun(@times, predictions, labels) < 0 ;
err = sum(error(:)) ;

% -------------------------------------------------------------------------
function err = error_none(opts, labels, res)
% -------------------------------------------------------------------------
err = zeros(0,1) ;

% -------------------------------------------------------------------------
function  [net,stats,prof] = process_epoch(opts, getBatch, epoch, subset, learningRate, imdb, net)
% -------------------------------------------------------------------------

% validation mode if learning rate is zero
training = learningRate > 0 ;
if training, mode = 'training' ; else, mode = 'validation' ; end
if nargout > 2, mpiprofile on ; end

numGpus = numel(opts.gpus) ;
if numGpus >= 1
  one = gpuArray(single(1)) ;
else
  one = single(1) ;
end
res = [] ;
mmap = [] ;
stats = [] ;

for t=1:opts.batchSize:numel(subset)
  fprintf('%s: epoch %02d: batch %3d/%3d: ', mode, epoch, ...
          fix(t/opts.batchSize)+1, ceil(numel(subset)/opts.batchSize)) ;
  batchSize = min(opts.batchSize, numel(subset) - t + 1) ;
  batchTime = tic ;
  numDone = 0 ;
  error = [] ;
  for s=1:opts.numSubBatches
    % get this image batch and prefetch the next
    batchStart = t + (labindex-1) + (s-1) * numlabs ;
    batchEnd = min(t+opts.batchSize-1, numel(subset)) ;
    batch = subset(batchStart : opts.numSubBatches * numlabs : batchEnd) ;
    [im, labels] = getBatch(imdb, batch, training) ;

    if opts.prefetch
      if s==opts.numSubBatches
        batchStart = t + (labindex-1) + opts.batchSize ;
        batchEnd = min(t+2*opts.batchSize-1, numel(subset)) ;
      else
        batchStart = batchStart + numlabs ;
      end
      nextBatch = subset(batchStart : opts.numSubBatches * numlabs : batchEnd) ;
      getBatch(imdb, nextBatch, training) ;
    end

    if numGpus >= 1
      im = gpuArray(im) ;
    end
	
    % evaluate CNN
    net.layers{end}.class = labels ;
    if training, dzdy = one; else, dzdy = [] ; end
    res = vl_simplenn_f(net, im, dzdy, res, ...
                      'accumulate', s ~= 1, ...
                      'disableDropout', ~training, ...
                      'conserveMemory', opts.conserveMemory, ...
                      'backPropDepth', opts.backPropDepth, ...
                      'sync', opts.sync) ;

    % accumulate training errors
    error = sum([error, [...
      sum(double(gather(res(end).x))) ;
      reshape(opts.errorFunction(opts, labels, res),[],1) ; ]],2) ;
    numDone = numDone + numel(batch) ;
  end

  % gather and accumulate gradients across labs
  if training
    if numGpus <= 1
%       net = accumulate_gradients(opts, learningRate, batchSize, net, res) ;
    else
      if isempty(mmap)
        mmap = map_gradients(opts.memoryMapFile, net, res, numGpus) ;
      end
      write_gradients(mmap, net, res) ;
      labBarrier() ;
      [~,res] = accumulate_gradients(opts, learningRate, batchSize, net, res, mmap) ;
    end

    %%% insert TCP/IP communication code
      % - if opts.master
      %    wait for receive(all)
      %    for-loop: build average gradient
      %    send out new gradients: send(all)
      %   else
      %    send your res: send(master)
      %    wait for new res: receive(master)
      %   end
      
      if opts.master
        %%%
        % MASTER
        %%%
        % receive gradients from slaves
        tmp = cell(1,(opts.numClients-1));
        parfor iSlave = 1:(opts.numClients-1) % can be set to parfor()
            if ~strcmp(opts.receiveObj{iSlave}.Status,'open')
                disp(' ')
                disp(['Opening master server for slave ' num2str(iSlave) ' now: (' datestr(now) ')'])
                fopen(opts.receiveObj{iSlave});
            else
                disp(' ')
            end
            disp(['Receving gradients from slave ' num2str(iSlave)])
            % wait until input buffer is filled
            bufSz(iSlave) = -1;
            bufDiff(iSlave) = -1;
            while bufDiff(iSlave) ~= 0 || bufSz(iSlave) == 0 || bufSz(iSlave)/4 < 8868369
                pause(0.1)
                bufDiff(iSlave) = opts.receiveObj{iSlave}.BytesAvailable-bufSz(iSlave);
                bufSz(iSlave) = opts.receiveObj{iSlave}.BytesAvailable;
            end
            
%             disp(['debug: ' num2str(bufSz(iSlave)/4) ' elements received from slave ' num2str(iSlave)])
            
            tmp{iSlave} = fread(opts.receiveObj{iSlave},bufSz(iSlave)/4,'single');
            disp(['Receved gradients! (slave ' num2str(iSlave) ')'])
            MSSG_in{iSlave} = single(tmp{iSlave});
%             fclose(opts.receiveObj{iSlave});
        end
        % accumulate gradients
        resCluster = accGradientsFromClients(MSSG_in,res);
        [net,res] = accumulate_gradients(opts, learningRate, batchSize, net, resCluster) ;
        
        % send new weights to slaves
        MSSG = mapResToVector(net.layers,'weights');
        success = false(1,opts.numClients-1);
        parfor iSlave = 1:(opts.numClients-1) % can be set to parfor()
% 			disp(['Opening master client for slave ' num2str(iSlave) ' now: (' datestr(now) ')'])
%             while ~success(iSlave)
%                 try
%                     fopen(opts.sendObj{iSlave});
%                     success(iSlave) = true;
%                 catch
%                     pause(0.1)
%                 end
%             end
            disp(['Sending weights to slave ' num2str(iSlave)])
            while ~success(iSlave)
                try
                    fwrite(opts.sendObj{iSlave},MSSG,'single');
                    success(iSlave) = true;
                catch
                    pause(0.1)
                end
            end
            disp(['Sending successful! (slave ' num2str(iSlave) ')'])
%             fclose(opts.sendObj{iSlave});
        end
      else
        %%%
        % SLAVE
        %%%
        % send my gradients to master
        MSSG = mapResToVector(res);
		%pause(3)
        if ~strcmp(opts.sendObj.Status,'open')
            disp(' ')
            disp(['Opening slave client (slave ' num2str(opts.mySlaveNumber) ') now: (' datestr(now) ')'])
            success = false;
            while ~success
                try
                    fopen(opts.sendObj);
                    success = true;
                catch
                    pause(0.1)
                end
            end
        else
            disp(' ')
        end
        disp(['Sending gradients to master (slave ' num2str(opts.mySlaveNumber) ')'])
        success = false;
        while ~success
            try
                fwrite(opts.sendObj,MSSG,'single');
                success = true;
            catch
                pause(0.1)
            end
        end
		disp(['Sending successful! (slave ' num2str(opts.mySlaveNumber) ')'])
%         fclose(opts.sendObj);

        % receive weights from master
% 		disp(['Opening slave server (slave ' num2str(opts.mySlaveNumber) ') now: (' datestr(now) ')'])
%         fopen(opts.receiveObj);
        disp(['Receiving new weights from master (slave ' num2str(opts.mySlaveNumber) ')'])
		% wait until input buffer is filled
		bufSz = -1;
		bufDiff = -1;
		while  bufDiff ~= 0 || bufSz == 0 || bufSz/4 < 8868369
			pause(0.1)
			bufDiff = opts.receiveObj.BytesAvailable-bufSz;
			bufSz = opts.receiveObj.BytesAvailable;
		end
        MSSG_in = fread(opts.receiveObj,bufSz/4,'single');
        disp(['Receiving successful! (slave ' num2str(opts.mySlaveNumber) ')'])
%         fclose(opts.receiveObj);
        net.layers = mapVectorToRes(single(MSSG_in),'weights',net.layers);

%         % confirm to master
%         jtcp('write',opts.JTCPOBJ,typecast(opts.mySlaveNumber,'int8'));
      end
      %%%
      
  end

  % print learning statistics
  batchTime = toc(batchTime) ;
  stats = sum([stats,[batchTime ; error]],2); % works even when stats=[]
  speed = batchSize/batchTime ;

  fprintf(' %.2f s (%.1f data/s)', batchTime, speed) ;
  n = (t + batchSize - 1) / max(1,numlabs) ;
  fprintf(' obj:%.3g', stats(2)/n) ;
  for i=1:numel(opts.errorLabels)
    fprintf(' %s:%.3g', opts.errorLabels{i}, stats(i+2)/n) ;
  end
  fprintf(' [%d/%d]', numDone, batchSize);
  fprintf('\n') ;

  % debug info
  if opts.plotDiagnostics && numGpus <= 1
    figure(2) ; vl_simplenn_diagnose(net,res) ; drawnow ;
  end
end

if nargout > 2
  prof = mpiprofile('info');
  mpiprofile off ;
end

% --
function mssg = jtcp_readBigVec(jtcpObj)
mssg = jtcp('read',jtcpObj);
newpart = jtcp('read',jtcpObj);
while ~isempty(newpart)
    mssg = cat(2,mssg,newpart);
    newpart = jtcp('read',jtcpObj);
end
% --
function accRes = accGradientsFromClients(MSSG_inCell,resMaster)
accRes = resMaster;
numClients = numel(MSSG_inCell)+1;
for iSlave = 1:numel(MSSG_inCell)
    resSlave = mapVectorToRes(MSSG_inCell{iSlave});
    for l=1:numel(resMaster)
      for j=1:numel(resMaster(l).dzdw)
          if iSlave == 1
            % divide gradient by the number of clients
            accRes(l).dzdw{j} = accRes(l).dzdw{j}/numClients + resSlave(l).dzdw{j}/numClients ;
          else
            accRes(l).dzdw{j} = accRes(l).dzdw{j} + resSlave(l).dzdw{j}/numClients ;
          end
      end
    end
end
% --
function Vector = mapResToVector(res, mode)
if ~exist('mode','var')
    mode = 'gradients';
end
numLayers   = numel(res);
nConvL      = 0;
sizes       = [];
vec         = [];
convLayers  = [];
for l=1:numLayers
    switch mode
        case 'gradients'
        if numel(res(l).dzdw) ~= 0
          nConvL = nConvL+1;
          sizes = cat(2, sizes, [size(res(l).dzdw{1},1) size(res(l).dzdw{1},2) size(res(l).dzdw{1},3) size(res(l).dzdw{1},4)]);
          convLayers = cat(2, convLayers, l);
          for iWeightTypes = 1:numel(res(l).dzdw)
            vec = cat(2, vec, reshape(gather(res(l).dzdw{iWeightTypes}),1,[]));
          end
        end
        case 'weights'
        if strcmp(res{l}.type, 'conv')
            nConvL = nConvL+1;
            convLayers = cat(2, convLayers, l);
            if isfield(res{l}, 'weights')
                sizes = cat(2, sizes, [size(res{l}.weights{1},1) size(res{l}.weights{1},2) size(res{l}.weights{1},3) size(res{l}.weights{1},4)]);
                for iWeightTypes = 1:numel(res{l}.weights)
                    vec = cat(2, vec, reshape(gather(res{l}.weights{iWeightTypes}),1,[]));
                end
            else
                % soon legacy code
                sizes = cat(2, sizes, [size(res{l}.filters,1) size(res{l}.filters,2) size(res{l}.filters,3) size(res{l}.filters,4)]);
                for iWeightTypes = {'filters','biases'}
                    vec = cat(2, vec, reshape(gather(res{l}.(char(iWeightTypes))),1,[]));
                end
            end
        end
    end
end
Vector = cat(2, numLayers, nConvL, convLayers, sizes, vec);
% --
function res = mapVectorToRes(Vector, mode, res, useGPU)
if ~exist('mode','var')
    mode = 'gradients';
end
if ~exist('useGPU','var')
    useGPU = true;
end
numLayers   = Vector(1);
nConvL      = Vector(2);
convLayers  = Vector(2+(1:nConvL));
sizes       = reshape(Vector(2+nConvL+(1:nConvL*4)),1,[]);
vec         = Vector((3+5*nConvL):end);

cConvLayerIdx = 1;
cVecStartPosition = 1;
for l=1:numLayers
    switch mode
        case 'gradients'
            if l == convLayers(cConvLayerIdx)
              cSize = sizes((1+4*(cConvLayerIdx-1)):cConvLayerIdx*4);
              res(l).dzdw{1}    = gpuArray(reshape(vec(-1+cVecStartPosition+(1:prod(cSize))),cSize));
              cVecStartPosition = cVecStartPosition+prod(cSize);
              res(l).dzdw{2}    = gpuArray(reshape(vec(-1+cVecStartPosition+(1:cSize(4))),[1 cSize(4)]));
              cVecStartPosition = cVecStartPosition+cSize(4);
              if cConvLayerIdx < numel(convLayers)
                cConvLayerIdx = cConvLayerIdx+1;
              end
            else
              res(l).dzdw = [];
            end
          case 'weights'
            if l == convLayers(cConvLayerIdx)
                % soon legacy code
%                 res{l}.type = 'conv';
                cSize = sizes((1+4*(cConvLayerIdx-1)):cConvLayerIdx*4);
                if useGPU
                    res{l}.filters    = gpuArray(reshape(vec(-1+cVecStartPosition+(1:prod(cSize))),cSize));
                    cVecStartPosition = cVecStartPosition+prod(cSize);
                    res{l}.biases     = gpuArray(reshape(vec(-1+cVecStartPosition+(1:cSize(4))),[1 cSize(4)]));
                else
                    res{l}.filters    = reshape(vec(-1+cVecStartPosition+(1:prod(cSize))),cSize);
                    cVecStartPosition = cVecStartPosition+prod(cSize);
                    res{l}.biases     = reshape(vec(-1+cVecStartPosition+(1:cSize(4))),[1 cSize(4)]);
                end
                cVecStartPosition = cVecStartPosition+cSize(4);
                if cConvLayerIdx < numel(convLayers)
                    cConvLayerIdx = cConvLayerIdx+1;
                end
            else
%                 res{l}.type = 'other';
            end
    end
end


% -------------------------------------------------------------------------
function [net,res] = accumulate_gradients(opts, lr, batchSize, net, res, mmap)
% -------------------------------------------------------------------------
for l=1:numel(net.layers)
  for j=1:numel(res(l).dzdw)
    thisDecay = opts.weightDecay * net.layers{l}.weightDecay(j) ;
    thisLR = lr * net.layers{l}.learningRate(j) ;

    % accumualte from multiple labs (GPUs) if needed
    if nargin >= 6
      tag = sprintf('l%d_%d',l,j) ;
      tmp = zeros(size(mmap.Data(labindex).(tag)), 'single') ;
      for g = setdiff(1:numel(mmap.Data), labindex)
        tmp = tmp + mmap.Data(g).(tag) ;
      end
      res(l).dzdw{j} = res(l).dzdw{j} + tmp ;
    end

    if isfield(net.layers{l}, 'weights')
      net.layers{l}.momentum{j} = ...
        single(opts.momentum) * net.layers{l}.momentum{j} ...
        - thisDecay * net.layers{l}.weights{j} ...
        - single(1/batchSize) * res(l).dzdw{j} ;
      net.layers{l}.weights{j} = net.layers{l}.weights{j} + thisLR * net.layers{l}.momentum{j} ;
    else
      % Legacy code: to be removed
      if j == 1
        net.layers{l}.momentum{j} = ...
          opts.momentum * net.layers{l}.momentum{j} ...
          - thisDecay * net.layers{l}.filters ...
          - (1 / batchSize) * res(l).dzdw{j} ;
        net.layers{l}.filters = net.layers{l}.filters + thisLR * net.layers{l}.momentum{j} ;
      else
        net.layers{l}.momentum{j} = ...
          opts.momentum * net.layers{l}.momentum{j} ...
          - thisDecay * net.layers{l}.biases ...
          - (1 / batchSize) * res(l).dzdw{j} ;
        net.layers{l}.biases = net.layers{l}.biases + thisLR * net.layers{l}.momentum{j} ;
      end
    end
  end
end

% -------------------------------------------------------------------------
function mmap = map_gradients(fname, net, res, numGpus)
% -------------------------------------------------------------------------
format = {} ;
for i=1:numel(net.layers)
  for j=1:numel(res(i).dzdw)
    format(end+1,1:3) = {'single', size(res(i).dzdw{j}), sprintf('l%d_%d',i,j)} ;
  end
end
format(end+1,1:3) = {'double', [3 1], 'errors'} ;
if ~exist(fname) && (labindex == 1)
  f = fopen(fname,'wb') ;
  for g=1:numGpus
    for i=1:size(format,1)
      fwrite(f,zeros(format{i,2},format{i,1}),format{i,1}) ;
    end
  end
  fclose(f) ;
end
labBarrier() ;
mmap = memmapfile(fname, 'Format', format, 'Repeat', numGpus, 'Writable', true) ;

% -------------------------------------------------------------------------
function write_gradients(mmap, net, res)
% -------------------------------------------------------------------------
for i=1:numel(net.layers)
  for j=1:numel(res(i).dzdw)
    mmap.Data(labindex).(sprintf('l%d_%d',i,j)) = gather(res(i).dzdw{j}) ;
  end
end
