function [meanErr,acc,err,medErr,minErr,maxErr] = testCNNwithParameters(imdb,testSubjects,net)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

if iscell(testSubjects)
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{1})) = 2;
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{2})) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);
net.layers = net.layers(1:(end-1));
net=vl_simplenn_move(net,'gpu');
testInds = find(imdb.images.set==2);
for iT = 1:numel(testInds)
    res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,testInds(iT))),...
        [],[],'disableDropout', true, 'conserveMemory', true);
    estimJoints = squeeze(gather(res(end).x));
    estimJoints = reshape(estimJoints,3,[]);
    gtJoints = squeeze(imdb.images.labels(:,:,:,testInds(iT)));
    gtJoints = reshape(gtJoints,3,[]);
    resid = estimJoints-gtJoints;
    sssd = sqrt(sum(resid.^2,1));
    err(iT) = mean(sssd(~isnan(sssd)));
    if mod(iT,1000) == 0
        fprintf('%d frames processed... average error: %f cm.\n',iT,mean(err)*100)
    end
end

meanErr = mean(err);
acc = nnz(err<0.1)/numel(testInds);

medErr = median(err);
minErr = min(err);
maxErr = max(err);

end