function net = trainCNNwithParametersBlanket(imdb,testSubjects,lr,batchsize,gpuID)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

if iscell(testSubjects)
    validSubj1 = strcmp({imdb.meta.info.actor},testSubjects{1});
    validSubj2 = strcmp({imdb.meta.info.actor},testSubjects{2});
    imdb.images.set(validSubj1) = 2;
    imdb.images.set(validSubj2) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

% only train/test on blanket movements, not the normal ones
invalidSeqsBool = strcmp({imdb.meta.info.sequence},'movementsH');
imdb.images.set(invalidSeqsBool) = 3;

% subtract average
net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);

opts.dataDir = sprintf('/home/achilles/trainedCNN/blanket_nonpretrain_nonclustered_lr%1.d_batch%d_NN',lr,batchsize);%'E:\mocapPose\trainedCNN\pretrain_nonrendered_nonclustered_lr1e-4';
opts.train.numEpochs = 1000 ;
opts.train.learningRate = lr ;
opts.train.numSubBatches = 1;
opts.train.batchSize = batchsize ;
opts.train.gpus = gpuID;

% opts.pretrainNet = 'C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat';

[net, info] = cnn_pose3D_symm(imdb,opts);

end