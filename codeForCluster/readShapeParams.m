function shapeParams = readShapeParams(jointPosFilename,numExpectedParams,shapePos)
if ~exist('numExpectedParams','var')
    numExpectedParams = 19;
end
fid = fopen(jointPosFilename);
if 0 == fseek(fid,shapePos,-1);
    if numExpectedParams == str2double(fgetl(fid))
        A = textscan(fid,'%f',numExpectedParams);
        shapeParams(1,:) = A{1};
    else
        error(['number of expected shape parameters does not match the '...
            'number of parameters present in the file.'])
    end
else
    error('file does not contain shape parameters!')
end
fclose(fid);
end