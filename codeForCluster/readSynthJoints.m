function [jointsPos,endPosition] = readSynthJoints(jointPosFilename,numExpectedJoints)
if ~exist('numExpectedJoints','var')
    numExpectedJoints = 19;
end
fid = fopen(jointPosFilename);
numJoints = str2double(fgetl(fid));
assert(numJoints == numExpectedJoints,...
    ['Number of joints is not ' num2str(numExpectedJoints)...
    '! Check joint position files.']);
A = textscan(fid,'%s %f %f %f\n',numJoints);
jointsPos(1,:) = 1000*A{2};
jointsPos(2,:) = 1000*A{3};
jointsPos(3,:) = 1000*A{4};

endPosition = ftell(fid);
fclose(fid);
end