function net = trainCNNwithParametersPosLabels(imdb,testSubjects,lr,batchsize,gpuID)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

if iscell(testSubjects)
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{1})) = 2;
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{2})) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

opts.dataDir = sprintf('/home/achilles/trainedCNN/nonpretrain_nonclustered_posLabels_lr%1.d_batch%d_NN',lr,batchsize);%'E:\mocapPose\trainedCNN\pretrain_nonrendered_nonclustered_lr1e-4';
opts.train.numEpochs = 1000 ;
opts.train.learningRate = lr ;
opts.train.numSubBatches = 1;
opts.train.batchSize = batchsize ;
opts.train.gpus = gpuID;


% sick! shift all the labels so to be positive
imdb.images.labels = imdb.images.labels/3 + 0.5;

% opts.pretrainNet = 'C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat';

[net, info] = cnn_pose3D_symm(imdb,opts);

end