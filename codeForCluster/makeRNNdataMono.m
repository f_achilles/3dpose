function success = makeRNNdataMono(imdb,testSubjects,net)

N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);

% dont use blanket to build the average
% imdb.images.set(strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;

% dont use movementsH to build the average
imdb.images.set(strcmp({imdb.meta.info.sequence},'movementsH')) = 3;

if iscell(testSubjects)
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{1})) = 2;
    imdb.images.set(strcmp({imdb.meta.info.actor},testSubjects{2})) = 2;
else
    testVecBinary = strcmp({imdb.meta.info.actor},testSubjects);
    if ~any(testVecBinary)
        error('test subject not found')
    end
    imdb.images.set(testVecBinary) = 2;
end

net.stats.average = mean(imdb.images.data(:,:,:,imdb.images.set == 1),4);
imdb.images.data = bsxfun(@minus,imdb.images.data,net.stats.average);

% dont test on movementsH
imdb.images.set(strcmp({imdb.meta.info.sequence},'movementsH')) = 3;

% test only on blankets
imdb.images.set(~strcmp({imdb.meta.info.sequence},'withBlanket_movementsH')) = 3;

net.layers = net.layers(1:(end-1));
net=vl_simplenn_move(net,'gpu');

featFormat = repmat('%f ',1,1024);
featFormat = [featFormat(1:end-1) '\n'];
lblFormat = repmat('%f ',1,42);
lblFormat = [lblFormat(1:end-1) '\n'];
mkdir(fullfile(pwd,'trainM'));
mkdir(fullfile(pwd,'testM'));
fidTrain = fopen(fullfile(pwd,'trainM','trainM.txt'),'w');
fidTest = fopen(fullfile(pwd,'testM','testM.txt'),'w');

N = size(imdb.images.data,4);
iSeq = 1;

trainingInds = find(imdb.images.set == 1);
disp 'I will generate this number of training features:'
numel(trainingInds)
testingInds = find(imdb.images.set == 2);
disp 'I will generate this number of testing features:'
numel(testingInds)

oldFrame = imdb.meta.info(trainingInds(1)).frame;
for iT = [trainingInds testingInds]
    cFrame = imdb.meta.info(iT).frame;
    if cFrame < oldFrame
        iSeq = 1+iSeq;
%     if imdb.meta.info(iT).frame == 1800
        fprintf('%s written\n',featFileName);
        fprintf('%s written\n',labelFileName);
    end
    oldFrame = cFrame;
%     featFileName = sprintf('featureVec1024_%s_%s_view%d_frame%d.txt',imdb.meta.info(iT).sequence,imdb.meta.info(iT).actor,imdb.meta.info(iT).angle,imdb.meta.info(iT).frame);
    featFileName = sprintf('feature_seq%d_frame%d.txt',iSeq,imdb.meta.info(iT).frame);
    res = vl_simplenn_f(net,gpuArray(imdb.images.data(:,:,:,iT)),...
        [],[],'disableDropout', true);
    feature = squeeze(gather(res(end-1).x));
    if imdb.images.set(iT) == 1
        fprintf(fidTrain,'%s\n',featFileName);
        fprintf(fidTrain,featFormat,feature);
    elseif imdb.images.set(iT) == 2
        fprintf(fidTest,'%s\n',featFileName);
        fprintf(fidTest,featFormat,feature);
    end
    
%     labelFileName = sprintf('label_%s_%s_view%d_frame%d.txt',imdb.meta.info(iT).sequence,imdb.meta.info(iT).actor,imdb.meta.info(iT).angle,imdb.meta.info(iT).frame);
    labelFileName = sprintf('label_seq%d_frame%d.txt',iSeq,imdb.meta.info(iT).frame);
    label = squeeze(imdb.images.labels(:,:,:,iT));

    if imdb.images.set(iT) == 1
        fprintf(fidTrain,'%s\n',labelFileName);
        fprintf(fidTrain,lblFormat,label);
    elseif imdb.images.set(iT) == 2
        fprintf(fidTest,'%s\n',labelFileName);
        fprintf(fidTest,lblFormat,label);
    end

end
fprintf('%s written\n',featFileName);
fprintf('%s written\n',labelFileName);
        
fclose(fidTrain);
fclose(fidTest);
zip(fullfile(pwd,'RNNdataM_refinedTestOnBlanket.zip'),{fullfile(pwd,'trainM'), fullfile(pwd,'testM')})
success = true;
end