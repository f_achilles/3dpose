function imdbAll = imdbAppend(imdbAll, imdb)

imdbAll.images.data     = cat(4, imdbAll.images.data, imdb.images.data);
imdbAll.images.labels   = cat(4, imdbAll.images.labels, imdb.images.labels);
imdbAll.images.set      = cat(2, imdbAll.images.set, imdb.images.set);

imdbAll.meta.bbox           = cat(1, imdbAll.meta.bbox, imdb.meta.bbox);
imdbAll.meta.sequenceID     = cat(2, imdbAll.meta.sequenceID, imdb.meta.sequenceID);

end