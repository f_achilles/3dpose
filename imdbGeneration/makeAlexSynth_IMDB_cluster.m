% clear all
% clear imdb

dbpath  = '/home/ichim/Documents/achilles/data1';
folders = dir(dbpath);
folders = folders(3:(end-1));
goodFolders = true(1,numel(folders));
% goodFolders = cellfun(@(x)~isempty(regexp(x,'angle_\d\d\d$','once')),{folders.name});
folders = folders(goodFolders);

numJoints = 17;
numShapeParams = 18;

% store each sample?
minDist = false;
if minDist
    listOfGoodIndices = cell(1);
    % minimum distance between joints in subsequent frames (normalized
    % units), [default: 0.05]
    d = 0.05;
    lastPose = zeros(1,1,3*numJoints,1,'single');
    listOfGoodIndices{1} = 0;
    idx = 0;
end

% imdb in RAM or in single variables, appended to a .mat file?
imdbAppend = false;
if imdbAppend && minDist
    save('C:\Projects\PoseEstimation\data\imdb_alex_8models.mat','d','-v7.3');
end

% limit dataset size?
% allowed number on 120GB RAM is = 120e9/(7200000000/83000) = 1.3million
dbSampleLimit = 1e6;

% normal training
cropH = 120;
cropW = 60;
% comparison to imageNet
% cropH = 224;
% cropW = 224;

ratio = cropH/cropW;
nRow = 480;
nCol = 640;
sampleIdx = 0;
offs = 100; %[mm] %0.1; %[m]
sInd = 0;
staticBboxEdgLen = 2;

% standard intrinsics
cx = 320;
cy = 240;
fx = 525;
fy = 525;
%
Intrinsics = [ fx 0 cx  0 ;
               0 fy cy  0 ;
               0  0  1  0 ;
               0  0  1  0 ];
%
[Xinds,Yinds] = meshgrid(1:nCol,1:nRow);

bboxMode = 'dynamic'; % 'ratio', 'static', 'dynamic'

%yolo

for iView = 1:nnz(goodFolders) %1:nnz(goodFolders)
    % each folder represents one camera view
    viewPath = [dbpath filesep folders(iView).name];
    sequences = dir(viewPath);
    for iSeq = 3:numel(sequences)
        seqPath = [viewPath filesep sequences(iSeq).name];
%         models = dir(seqPath);
%         for iMod = 1:numel(models)
%             modPath = [seqPath filesep models(iMod).name];
            jointPosFiles = dir([seqPath filesep '*.txt']);
            frames = dir([seqPath filesep '*.png']);
            
            % debug
            % figure('Position',[1 41 1600 1083])
            
            for iFrame = 2:numel(frames) %start from frame 2 (exclude T-pose)
                % read depth frame
                DepthFilename = [seqPath filesep frames(iFrame).name];
                Z = single(imread(DepthFilename));
                X = Z .* (Xinds-cx) / fx;
                Y = Z .* (Yinds-cy) / fy;
                FG = Z~=0;
                % read joint positions
                jointPosFilename        = [seqPath filesep jointPosFiles(iFrame).name];
                [jointsPos,filePosition]= readSynthJoints(jointPosFilename,numJoints);
                % clean up joint position outliers
                jointsPos(abs(jointsPos)>1e4) = NaN;
                
                % read shape parameters
                shapeParams             = readShapeParams(jointPosFilename,numShapeParams,filePosition);
                if any(FG(:))
                    sInd = sInd+1;
                    % get bounding box parameters
                    switch bboxMode
                        case 'ratio'
                        % constant ratio bbox
                        minX = min(jointsPos(1,:))-offs;
                        maxX = max(jointsPos(1,:))+offs;
                        minY = min(jointsPos(2,:))-offs;
                        maxY = max(jointsPos(2,:))+offs;
                        deltaX = maxX-minX;
                        deltaY = maxY-minY;

                        XZYratio = deltaY/deltaX;
                        % adjust aspect ratio
                        if XZYratio<ratio %deltaY too short
                            increase = ratio*deltaX-deltaY;
                            minY = minY-increase/2;
                            maxY = maxY+increase/2;
                        elseif XZYratio>ratio %deltaX too short
                            increase = deltaY/ratio-deltaX;
                            minX = minX-increase/2;
                            maxX = maxX+increase/2;
                        else
                            % nothing
                        end
                        minZ = min(jointsPos(3,:))-offs;
                        maxZ = max(jointsPos(3,:))+offs;
                        case 'static'
                        % constant measurements bbox
                        centerX = (max(jointsPos(1,:))+min(jointsPos(1,:)))/2;
                        centerY = (max(jointsPos(2,:))+min(jointsPos(2,:)))/2;
                        centerZ = (max(jointsPos(3,:))+min(jointsPos(3,:)))/2;
                        minX = centerX - staticBboxEdgLen/2;
                        maxX = centerX + staticBboxEdgLen/2;
                        minY = centerY - staticBboxEdgLen/2;
                        maxY = centerY + staticBboxEdgLen/2;
                        minZ = centerZ - staticBboxEdgLen/2;
                        maxZ = centerZ + staticBboxEdgLen/2;
                        case 'dynamic'
                        % dynamic bbox tightly fitted to subject
%                         minX = min(jointsPos(1,:))-offs;
%                         maxX = max(jointsPos(1,:))+offs;
%                         minY = min(jointsPos(2,:))-offs;
%                         maxY = max(jointsPos(2,:))+offs;
%                         minZ = min(jointsPos(3,:))-offs;
%                         maxZ = max(jointsPos(3,:))+offs;
                        minX = min(X(:));
                        maxX = max(X(:));
                        minY = min(Y(:));
                        maxY = max(Y(:));
                        minZ = min(Z(Z>0));
                        maxZ = max(Z(:));
                    end
                    % project bounding box to image plane
                        % X = Z * (Xind-cx)/fx;
                        % Xind = X*fx/Z + cx = (X*fx + Z*cx)/Z;
                    borders=[[minX;minY;minZ] [maxX;maxY;minZ]];
                    % borders(2,:) = -borders(2,:);
                    bordersKinectImgPlane = Intrinsics *...
                        cat(1,borders,ones(1,2));
                    bordersKinectImgPlane = ...
                        bordersKinectImgPlane./...
                        repmat(bordersKinectImgPlane(4,:),4,1);
                    minRowmaxZ = round(bordersKinectImgPlane(2,1));
                    maxRowmaxZ = round(bordersKinectImgPlane(2,2));
                    minColmaxZ = round(bordersKinectImgPlane(1,1));
                    maxColmaxZ = round(bordersKinectImgPlane(1,2));
                    % define projected bounding box 2D size
                    bbox2D = zeros(maxRowmaxZ-minRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
                    % define cropping area inside of original depth image
                    upperRowInOrig = max(1,minRowmaxZ);
                    lowerRowInOrig = min(nRow,maxRowmaxZ);
                    leftColInOrig = max(1,minColmaxZ);
                    rightColInOrig = min(nCol,maxColmaxZ);
                    % calculate offset
                    rowOff = upperRowInOrig-minRowmaxZ;
                    colOff = leftColInOrig-minColmaxZ;
                    % normalize pointcloud to bounding box dimensions
                    X = (X - minX)/abs(minX-maxX);
                    Y = (Y - minY)/abs(minY-maxY);
                    Z = (Z - minZ)/abs(minZ-maxZ);
                    % mask pointcloud, such that it only contains the
                    % subject inside the bounding box
%                     if strcmp(bboxMode,'dynamic')
%                         depth = (cat(3,X,Y,Z)*255-255/2).*repmat(FG,1,1,3);
%                     else
%                         depth = cat(3,X,Y,Z).*repmat(FG,1,1,3);
%                     end
                    depth = cat(3,X,Y,Z).*repmat(FG,1,1,3);
                    % map pointcloud crop to projected bounding box
                    bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...   
                        depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);

                    % normalize joint locations by bounding box dimensions
                    jointsPos(1,:) = (jointsPos(1,:) - minX)/abs(minX-maxX);
                    jointsPos(2,:) = (jointsPos(2,:) - minY)/abs(minY-maxY);
                    jointsPos(3,:) = (jointsPos(3,:) - minZ)/abs(minZ-maxZ);
                    
                    if minDist % kick out frames with too little relative motion
                        cPose = single(reshape(jointsPos,1, 1, [], 1));
                        % good frame?
                        if any(abs(cPose-lastPose) > d)
                            listOfGoodIndices{end+1} = sInd;% save index
                            lastPose = cPose;               % update keyframe
                            idx = idx+1;
                            if idx>dbSampleLimit
                                error('sample limit reached!')
                            end
                            if imdbAppend
                                % append to db
                                assignin('base',sprintf('data%d',idx),single(imresize(bbox2D,[cropH cropW])));
                                assignin('base',sprintf('label%d',idx),cPose);
                                assignin('base',sprintf('bbox%d',idx),single([minX maxX minY maxY minZ maxZ]));
                                assignin('base',sprintf('ID%d',idx),[folders(iView).name '_' sequences(iSeq).name '_' models(iMod).name]);
                                save('C:\Projects\PoseEstimation\data\imdb_alex_8models.mat',sprintf('data%d',idx),sprintf('label%d',idx),...
                                    sprintf('bbox%d',idx),sprintf('ID%d',idx),'-append');
                                clear(sprintf('data%d',idx),sprintf('label%d',idx),sprintf('bbox%d',idx),sprintf('ID%d',idx))
                            else
                                imdb.images.data(:,:,:,idx) = single(imresize(bbox2D,[cropH cropW]));
                                imdb.images.labels(:,:,1:3*numJoints,idx) = single(reshape(jointsPos,1, 1, [], 1));
                                imdb.meta.bbox(idx,:) = [minX maxX minY maxY minZ maxZ];
                                imdb.meta.sequenceID{idx} = [folders(iView).name '_' sequences(iSeq).name '_' models(iMod).name];
                                % train/test split
                                imdb.images.set(idx) = 1;
                            end
                        end
                    else % include all input frames to the imdb
                        if sInd>dbSampleLimit
                            error('sample limit reached!')
                        end
                        try
                        imdb.images.data(:,:,:,sInd) = single(imresize(bbox2D,[cropH cropW],'nearest')); %nearest neighbor for crisp edges (inportant!)
                        catch
                            warning('imresize() did not succeed!')
                            pause
                        end
                        imdb.images.labels(:,:,1:3*numJoints,sInd) = single(reshape(jointsPos,1, 1, [], 1));
                        imdb.images.labels(:,:,3*numJoints+(1:numShapeParams),sInd) = single(reshape(shapeParams,1, 1, [], 1));
                        imdb.meta.bbox(sInd,:) = single([minX maxX minY maxY minZ maxZ]);
                        imdb.meta.sequenceID{sInd} = [folders(iView).name '_' sequences(iSeq).name];
                        % train/test split
                        imdb.images.set(sInd) = 1;
                    end
                    fprintf('Sequence %s, frame %d/%d.\n',[folders(iView).name '_' sequences(iSeq).name],iFrame,numel(frames))
% for debug:
% return
            %         plot3( jointsPos(1,:), jointsPos(2,:), jointsPos(3,:),'ro');
            %         patch('xdata', X(FG), 'ydata',Y(FG),'zdata', Z(FG), ...
            %             'linestyle', 'none', ...
            %             'marker', '.', ...
            %             'FaceColor', 'none',...
            %             'EdgeColor', 'flat',...
            %             'cdata', -Z(FG),...
            %             'markeredgecolor', 'auto');
            %         axis image vis3d
            %         drawnow
                end

            % subplot(1,2,1)

            % subplot(1,2,2)    
            %     imagesc(Z); axis image
            %     hold on
            %     plot(PositionsKinectImgPlane(1,:),PositionsKinectImgPlane(2,:),'r*')
            %     hold off
            %     pause(0.02)
            end %frames
%         end %actors
    end %sequences
    disp(['Finished processing view ' folders(iView).name]);
end %views
if minDist
    listOfGoodIndices = cell2mat(listOfGoodIndices(2:end));
    % listOfGoodIndices = mat2cell(listOfGoodIndices,1,numel(listOfGoodIndices));
end

% save imdb to file
save(fullfile(dbpath,'imdbDynamic.mat'),'imdb','-v7.3');

% %% visualize IMDB
% % plot imdb
% figure
% N=size(imdb.images.data,4);
% for i = 1:N
%     imagesc(imdb.images.data(:,:,:,i)); axis image;
%     pause(0.03)
% end
% %% make movie
% vidObj = VideoWriter('imdbVid_SynthA01_ratioBbox_120x60.avi');
% vidObj.Quality = 100;
% open(vidObj);
% for i = 1:N
%     im = squeeze(imdb.images.data(:,:,:,i));
%     valid = boolean(abs(im));
%     im = im-min(im(:));
%     im = im/max(im(:));
%     writeVideo(vidObj,im.*valid)
% end
% close(vidObj);
% %% test on some images
% 
% numSamples = 20;
% trainIdx = find(imdb.images.set == 2);
% randTrainIdx = randperm(numel(trainIdx),numSamples);
% net=vl_simplenn_move(net,'gpu');
% for iTrainSample = 1:numSamples
%     im = imdb.images.data(:,:,:,trainIdx(randTrainIdx(iTrainSample)));
% %     im = imresize(imdb.images.data(:,:,:,trainIdx(randTrainIdx(iTrainSample))),[224 224]);
%     lbl = imdb.images.labels(:,:,:,trainIdx(randTrainIdx(iTrainSample)));
%     net.layers{end}.class = gpuArray(lbl);
%     res = vl_simplenn(net, gpuArray(im), [], [], ...
%       'disableDropout', true, ...
%       'conserveMemory', false, ...
%       'sync', false) ;
%     joints = squeeze(gather(res(end-1).x));
%     GTjoints = squeeze(lbl);
% %     figure('position',[100 200 1400 800]);
% figure
%     % depth map
%     subplot(1,3,1)
%     imagesc(im(:,:,3));
%     axis image
%     title('depth map')
%     % ground truth pose
%     subplot(1,3,2)
%     
%     % either skeleton plot
% %     GTjoints = reshape(GTjoints,3,[]);
% %     drawEvalSkel(reshape(GTjoints',[],1),'*g','3d','PDT');
% %     GTjoints = reshape(GTjoints,[],1);
%     
%     % or joint-point plot
%     GTjoints=GTjoints(~isnan(GTjoints));
%     plot3(GTjoints(1:3:end),GTjoints(2:3:end),GTjoints(3:3:end),'*g')
% 
%     text(double(GTjoints(1:3:end)),double(GTjoints(2:3:end)),double(GTjoints(3:3:end))...
%         ,num2str((1:numJoints)'));
%     axis image vis3d
%     view(3)
%     title('ground truth pose')
%     % estimated pose
%     subplot(1,3,3)
%     
%     % either skeleton plot
% %     joints = reshape(joints,3,[]);
% %     drawEvalSkel(reshape(joints',[],1),'*b','3d','PDT');
% %     joints = reshape(joints,[],1);
%     
%     % or joint-point plot
%     joints=joints(~isnan(joints));
%     plot3(joints(1:3:end),joints(2:3:end),joints(3:3:end),'*g')
% 
%     text(double(joints(1:3:end)),double(joints(2:3:end)),double(joints(3:3:end))...
%         ,num2str((1:numJoints)'));
% 
% %     joints(isnan(GTjoints))=NaN;
% %     plot3(joints(1:3:end),joints(2:3:end),joints(3:3:end),'*b')
% %     set(gca,'ydir','reverse')
% %     axis(axlimsGT);
%     axis image vis3d
%     view(3)
%     title('estimated pose')
%     drawnow
%     pause
% end
% 
% %% test on all images: compute per-joint average absolute distance
% batchSize = 1;
% testIdx = find(imdb.images.set == 2);
% perJointCollector   = cell(1);
% avgErrCollector     = cell(1);
% precisionCollector  = cell(1);
% diffCollector        = cell(1);
% % Heltens proposed evaluation
% fakeEval = false;
% % move network to GPU
% net = vl_simplenn_move(net, 'gpu') ;
% 
% % randTrainIdx = randperm(numel(trainIdx),numSamples);
% for iTrainSample = 1:ceil(numel(testIdx)/batchSize)
%     firstIdx = (iTrainSample-1)*batchSize+1;
%     lastIdx = min((iTrainSample)*batchSize,numel(testIdx));
%     im = imdb.images.data(:,:,:,testIdx(firstIdx:lastIdx));
%     lbl = 1000*imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
% %     lbl = imdb.images.labelsOverParam(:,:,:,testIdx(firstIdx:lastIdx));
% 
% lbl = cat(3,lbl,ones(1,1,130-54,'single'));
% net.layers{end}.class = gpuArray(lbl);
%     res = vl_simplenn_f(net, gpuArray(im), [], [], ...
%       'disableDropout', true, ...
%       'conserveMemory', false, ...
%       'sync', false) ;
%     joints = squeeze(gather(res(end-1).x));
%     GTjoints = squeeze(lbl);
%     % retrieve bbox measurements
%     bbox = imdb.meta.bbox(testIdx(firstIdx:lastIdx),:);
%     dX = abs(bbox(1)-bbox(2));
%     dY = abs(bbox(3)-bbox(4));
%     dZ = abs(bbox(5)-bbox(6));
%     boxMultiplier = repmat([dX;dY;dZ],numJoints,1);
%     % calc per joint error
%     diff = joints-GTjoints;
%     % only compare pose, not length or angles
%     diff = diff(1:54);
%     diffsq = (diff.*(boxMultiplier)).^2;
%     
%     diffCollector{end+1}=diff;
%     sumPerJoint = diffsq(1:3:end) + diffsq(2:3:end) + diffsq(3:3:end);
%     perJointCollector{end+1} = sumPerJoint;
%     avgPrecision = nnz(sqrt(sumPerJoint(~isnan(sumPerJoint)))<100)/nnz(~isnan(sumPerJoint));
%     precisionCollector{end+1} = avgPrecision;
%     avgErr = sum(sqrt(sumPerJoint(~isnan(sumPerJoint))))/nnz(~isnan(sumPerJoint));
%     avgErrCollector{end+1} = avgErr;
%     fprintf('tested frame %d of %d, avg dist: %5.3fmm\n',iTrainSample,numel(testIdx),avgErr);
% end
% 
% avgErrCollector=avgErrCollector(2:end);
% perJointCollector=perJointCollector(2:end);
% precisionCollector=precisionCollector(2:end);
% diffCollector=diffCollector(2:end);
% 
% if fakeEval == true
%     allDiffs = [diffCollector{:}];
% %     offsetDiff = mean(allDiffs,2);    
% %     allDiffs = bsxfun(@minus,allDiffs,offsetDiff);
%     allDiffs = (bsxfun(@times,allDiffs,boxMultiplier)).^2;
%     allSumPerJoint = allDiffs(1:3:end,:)+allDiffs(2:3:end,:)+allDiffs(3:3:end,:);
%     allSumPerJoint = sqrt(allSumPerJoint);
%     perPartAverage = mean(allSumPerJoint,2);
%     perPartPrecision = mean(allSumPerJoint<100,2);
%     
%     offsetRemDiffs = abs(bsxfun(@minus,allSumPerJoint,perPartAverage));
% %     offsRemSumPerJoint = offsetRemDiffs(1:3:end,:)+offsetRemDiffs(2:3:end,:)+offsetRemDiffs(3:3:end,:);
% %     offsRemSumPerJoint = sqrt(offsRemSumPerJoint);
%     perPartAverage = mean(offsetRemDiffs,2);
%     perPartPrecision = mean(offsetRemDiffs<100,2);
%     
%     
%     avgErr = mean(perPartAverage)
%     avgPrecision = mean(perPartPrecision)
% %     offsetX = mean(diff(1:3:end));
% %     offsetY = mean(diff(2:3:end));
% %     offsetZ = mean(diff(3:3:end));
% %     diffsq = ((diff-repmat([offsetX;offsetY;offsetZ],20,1)).*(boxMultiplier)).^2;
% else
%     avgErr = mean([avgErrCollector{:}])
%     avgPrecision = mean([precisionCollector{:}])
%     perPartAverage = mean(sqrt([perJointCollector{:}]),2);
%     perPartPrecision = mean(sqrt([perJointCollector{:}])<100,2);
% end
% 
% %% calculate error for each bodypart (12 parts)
% % bodyParts = {'Head','Chest','RShoulder','LShoulder','RKnee','LKnee','RAnkle','LAnkle','RWrist','LWrist','RElbow','LElbow'}';
% % bodyPartIDs = [4, 3, 9, 5, 18, 14, 19, 15, 11, 7, 10, 6];
% % 18 parts, SynthEPFL
% bodyParts = {'Chest','LFoot','RFoot','LForearm','RForearm','LHand','RHand','Head','Hips','LMiddle','RMiddle','Neck','LShin','RShin','LThigh','RThigh','LUpperarm','RUpperarm'}';
% bodyPartIDs = 1:18;
% table(bodyParts, perPartAverage(bodyPartIDs), perPartPrecision(bodyPartIDs),'VariableNames',{'bodypart','avgDist','precision'})
% %% debug
% [DepthFrame,Intrinsics,Extrinsics] = loadDepthFrameFromFile('C:\Projects\CombinedSegmentationAndPoseEstimation\data\PDT\F1D1\depth0000.bin');