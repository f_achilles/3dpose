dbpath = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\EVAL';
% skeletonSubdir = 'ScreenSkeleton_20joints';
sequences = dir([dbpath filesep '*.bin']);

nRow = 240;
nCol = 320;
fullJointInds   = [1 2 3 4 5 7 8 9 12 13 16 17];
jointIndLookup  = zeros(1,max(fullJointInds));
jointIndLookup(fullJointInds) = 1:numel(fullJointInds);
sampleIdx = 0;

bboxMode = 'dynamic'; % 'ratio', 'static', 'dynamic'

for iSeq = 1:numel(sequences)
dbFile      = fopen([dbpath filesep sequences(iSeq).name]);
cloudContainer = cell(1);
jointContainer = cell(1);
markerContainer = cell(1);
iFrame = 0;
while true
mNum_frameNum   = fread(dbFile,2,'int32=>single');
if feof(dbFile)
    break
end
iFrame = iFrame+1;
fprintf('Frame %3d\n',iFrame);
tempPCloud = fread(dbFile, [3 nCol*nRow],'*single');
pCloud = zeros([nRow nCol],'single');
pCloud(:,:,1) = rot90(reshape(tempPCloud(1,:),[nCol nRow]),-1);
pCloud(:,:,2) = rot90(reshape(tempPCloud(2,:),[nCol nRow]),-1);
pCloud(:,:,3) = rot90(reshape(tempPCloud(3,:),[nCol nRow]),-1);
cloudContainer{1,1,1,iFrame} = pCloud;

nMarkers        = fread(dbFile,1,'int32=>single');
maxNumMarkers = 32;
markers = struct('name','test','coor',zeros(1,3,'single'));
for i=1:maxNumMarkers
    markers(i).name = fread(dbFile,256,'*char');
    markers(i).coor = fread(dbFile,3,'*single');
end
markerContainer{1,1,1,iFrame} = markers(1:min(nMarkers,maxNumMarkers));

nJoints = fread(dbFile,1,'int32=>single');
maxNumJoints = 32;
joints = struct('id',zeros(1,'single'),'coor',zeros(1,3,'single'));
for i=1:maxNumJoints
    joints(i).id = fread(dbFile,1,'int32=>single');
    joints(i).coor = fread(dbFile,3,'*single');
end
jointContainer{1,1,1,iFrame} = joints(1:nJoints);
end
fclose(dbFile);
disp(['Finished reading ' sequences(iSeq).name]);

% invalidPtsMap = ones([nRow nCol]);
% invalidPtsMap(pCloud(:,:,3) == 0) = NaN;

% figure; imagesc(pCloud(:,:,3).*invalidPtsMap); axis image
% % figure; plot3(pCloud(:,:,1),pCloud(:,:,2),pCloud(:,:,3),'*'); axis image vis3d
% figure; surf(pCloud(:,:,1).*invalidPtsMap,pCloud(:,:,2).*invalidPtsMap,pCloud(:,:,3).*invalidPtsMap,'edgecolor','none','facecolor','interp'); axis image vis3d
% 
%        
% figure
% for i=1:iFrame
% pCloud = cloudContainer{i};
% invalidPtsMap = ones([nRow nCol]);
% invalidPtsMap(pCloud(:,:,3) == 0) = NaN;
% imagesc(pCloud(:,:,3).*invalidPtsMap); axis image
% pause(0.03)
% end

% crop bounding boxes out of Z-frames and resize to [cropH cropW]
cropH = 120;
cropW = 60;
ratio = cropH/cropW;
offs = 0.1;
% f = 5.453e-3;
% figure;

%%%
% For re-calculating the X- and Y- pointcloud coordinates, the
% calibrated-camera paramters of this calibration:
% https://vision.in.tum.de/data/datasets/rgbd-dataset/file_formats
% number "Freiburg 3", could be useful.
% fx: 567.6, fy 570.2, cx 324.7, cy 250.1 (rest: 0!)
%
% Formula: (2*z/fy)*(col-cy/2) = y;
%%%
% fx = 567.6;
% fy = 570.2;
% cx = 324.7;
% cy = 250.1;

% take the standard kinect IR-sensor parameters? What are they anyway?
fx = 575;
fy = 575;
cx = 320;
cy = 240;

% [colIdx,rowIdx] = meshgrid(1:nCol,1:nRow);

for i=1:iFrame
    sInd = i+sampleIdx;
    
    if ~isempty(jointContainer{i})
        
    % remove background
    jointsPos = [jointContainer{i}.coor];
    minX = min(jointsPos(1,:))-offs;
    maxX = max(jointsPos(1,:))+offs;
    minY = min(jointsPos(2,:))-offs;
    maxY = max(jointsPos(2,:))+offs;
    minZ = min(jointsPos(3,:))-offs;
    maxZ = max(jointsPos(3,:))+offs;
    
    pCloud = cloudContainer{i};
    higherThanMinX = pCloud(:,:,1)>minX;
    lowerThanMaxX = pCloud(:,:,1)<maxX;
    higherThanMinY = pCloud(:,:,2)>minY;
    lowerThanMaxY = pCloud(:,:,2)<maxY;
    higherThanMinZ = pCloud(:,:,3)>minZ;
    lowerThanMaxZ = pCloud(:,:,3)<maxZ;
    validBboxPixels = higherThanMinX&lowerThanMaxX&...
                higherThanMinY&lowerThanMaxY&...
                higherThanMinZ&lowerThanMaxZ;
    X = pCloud(:,:,1) .* validBboxPixels;
    Y = pCloud(:,:,2) .* validBboxPixels;
    Z = pCloud(:,:,3) .* validBboxPixels;
    
    switch bboxMode
        case 'ratio'
        % constant ratio bbox
        minX = min(jointsPos(1,:))-offs;
        maxX = max(jointsPos(1,:))+offs;
        minY = min(jointsPos(2,:))-offs;
        maxY = max(jointsPos(2,:))+offs;
        deltaX = maxX-minX;
        deltaY = maxY-minY;

        XZYratio = deltaY/deltaX;
        % adjust aspect ratio
        if XZYratio<ratio %deltaY too short
            increase = ratio*deltaX-deltaY;
            minY = minY-increase/2;
            maxY = maxY+increase/2;
        elseif XZYratio>ratio %deltaX too short
            increase = deltaY/ratio-deltaX;
            minX = minX-increase/2;
            maxX = maxX+increase/2;
        else
            % nothing
        end
        minZ = min(jointsPos(3,:))-offs;
        maxZ = max(jointsPos(3,:))+offs;
        case 'static'
        % constant measurements bbox
        centerX = (max(jointsPos(1,:))+min(jointsPos(1,:)))/2;
        centerY = (max(jointsPos(2,:))+min(jointsPos(2,:)))/2;
        centerZ = (max(jointsPos(3,:))+min(jointsPos(3,:)))/2;
        minX = centerX - staticBboxEdgLen/2;
        maxX = centerX + staticBboxEdgLen/2;
        minY = centerY - staticBboxEdgLen/2;
        maxY = centerY + staticBboxEdgLen/2;
        minZ = centerZ - staticBboxEdgLen/2;
        maxZ = centerZ + staticBboxEdgLen/2;
        case 'dynamic'
        % dynamic bbox tightly fitted to subject
        minX = min(X(:));
        maxX = max(X(:));
        minY = min(Y(:));
        maxY = max(Y(:));
        minZ = min(Z(Z~=0));
        maxZ = max(Z(Z~=0));
    end
    
%     jointsPos = [jointContainer{i}.coor];
%     minX = min(jointsPos(1,:))-offs;
%     maxX = max(jointsPos(1,:))+offs;
%     minY = min(jointsPos(2,:))-offs;
%     maxY = max(jointsPos(2,:))+offs;
%     minZ = min(jointsPos(3,:))-offs;
%     maxZ = max(jointsPos(3,:))+offs;
%     
%     deltaX = maxX-minX;
%     deltaY = maxY-minY;
%     
%     XZYratio = deltaY/deltaX;
%     % adjust aspect ratio
%     if XZYratio<ratio %deltaY too short
%         increase = ratio*deltaX-deltaY;
%         minY = minY-increase/2;
%         maxY = maxY+increase/2;
%     elseif XZYratio>ratio %deltaX too short
%         increase = deltaY/ratio-deltaX;
%         minX = minX-increase/2;
%         maxX = maxX+increase/2;
%     else
%         % nothing
%     end
    

%     xLocsPtCloud    = (2*-pCloud(:,:,3)/fx).*(colIdx-cx/2)+0.05;
%     yLocsPtCloud    = (2*pCloud(:,:,3)/fy).*(rowIdx-cy/2)+0.05;
% %     figure;
% %     subplot(1,3,1)
% %     imagesc(pCloud(:,:,1)); axis image
% %     title('Original X-coordinate')
% %     subplot(1,3,2)
% %     imagesc(xLocsPtCloud); axis image
% %     title('NEW X-coordinate')
% %     subplot(1,3,3)
% %     imagesc(xLocsPtCloud-pCloud(:,:,1)); axis image
% %     title('difference')
%     pCloud(:,:,1) = xLocsPtCloud;
%     pCloud(:,:,2) = yLocsPtCloud;

% %     % plot
%     figure
%     invalidPtsMap = ones([nRow nCol]);
%     invalidPtsMap(pCloud(:,:,3) == 0) = NaN;
%     [cAz, cEl] = view;
%     surf(pCloud(:,:,1).*invalidPtsMap,pCloud(:,:,2).*invalidPtsMap,...
%          pCloud(:,:,3).*invalidPtsMap,'edgecolor','none','facecolor','interp');
%     axis image vis3d
%     camlight
%     lighting gouraud
% %     view(3)
%     joints = jointContainer{i};
%     jointsPos = [joints.coor];
%     markers = markerContainer{i};
%     markersPos = [markers.coor];
%     hold on
%     plot3(jointsPos(1,:),jointsPos(2,:),jointsPos(3,:),'g^')
%     plot3(markersPos(1,:),markersPos(2,:),markersPos(3,:),'r*')
%     hold off
%     
%     % bbox along x-axis
%     line([minX maxX],[minY minY],[minZ minZ],'linewidth',3)
%     line([minX maxX],[minY minY],[maxZ maxZ],'linewidth',3)
%     line([minX maxX],[maxY maxY],[minZ minZ],'linewidth',3)
%     line([minX maxX],[maxY maxY],[maxZ maxZ],'linewidth',3)
%     % bbox along y-axis
%     line([minX minX],[minY maxY],[minZ minZ],'linewidth',3)
%     line([minX minX],[minY maxY],[maxZ maxZ],'linewidth',3)
%     line([maxX maxX],[minY maxY],[minZ minZ],'linewidth',3)
%     line([maxX maxX],[minY maxY],[maxZ maxZ],'linewidth',3)
%     % bbox along z-axis
%     line([minX minX],[minY minY],[minZ maxZ],'linewidth',3)
%     line([maxX maxX],[minY minY],[minZ maxZ],'linewidth',3)
%     line([minX minX],[maxY maxY],[minZ maxZ],'linewidth',3)
%     line([maxX maxX],[maxY maxY],[minZ maxZ],'linewidth',3)
%     view([cAz, cEl])
%     drawnow
    %     pause(0.02)
    %%% This does NOT work (why?-->(un)Distortion!) we dont have the
    %%% parameters--> we could estimate them iteratively but nah
%     % back-project edges to pixel space
%     (z/fy)*(col*2-cy) = y;
    %back-project XYZ of bounding box to the imageplane
    % fx: 567.6, fy 570.2, cx 324.7, cy 250.1 (rest: 0!)


%     %plot
%     figure;
%     subplot(3,2,1)
%     imagesc(higherThanMinX); axis image
%     subplot(3,2,2)
%     imagesc(lowerThanMaxX); axis image
%     subplot(3,2,3)
%     imagesc(higherThanMinY); axis image
%     subplot(3,2,4)
%     imagesc(lowerThanMaxY); axis image
%     subplot(3,2,5)
%     imagesc(higherThanMinZ); axis image
%     subplot(3,2,6)
%     imagesc(lowerThanMaxZ); axis image
%     %
%     figure;
%     imagesc(validBboxPixels); axis image
%     hold on
%     plot(minColminZ,minRowminZ,'g*')
%     plot(maxColminZ,minRowminZ,'g*')
%     plot(minColminZ,maxRowminZ,'g*')
%     plot(maxColminZ,maxRowminZ,'g*')
%     plot(minColmaxZ,minRowmaxZ,'r*')
%     plot(maxColmaxZ,minRowmaxZ,'r*')
%     plot(minColmaxZ,maxRowmaxZ,'r*')
%     plot(maxColmaxZ,maxRowmaxZ,'r*')
%     hold off
%     pause
    % Likely the floor will be at the lower end of the bounding box. So if
    % we get the lowermost, leftmost and rightmost pixel's coordinates, we
    % can construct a 2D-bbox that introduces only little error.
    %
    % X and Y real-world coordinates are shifted to yield (0,0) in the
    % center of the 3D-bbox
%     leftmost  = find(any(validBboxPixels),1,'first');
%     rightmost = find(any(validBboxPixels),1,'last');
%     uppermost = find(any(validBboxPixels,2),1,'first');
%     lowermost = find(any(validBboxPixels,2),1,'last');
%     deltaRow = lowermost-uppermost;
%     deltaCol = rightmost-leftmost;
%     RowColratio = deltaRow/deltaCol;
%     % adjust aspect ratio
%     if RowColratio<ratio %deltaRow too short
%         uppermost = max(1,round(lowermost-ratio*deltaCol));
%     elseif RowColratio>ratio %deltaCol too short
%         increase = deltaRow/ratio-deltaCol;
%         leftmost = max(1, round(leftmost-increase/2));
%         rightmost = min(nCol, round(rightmost+increase/2));
%     else
%         % nothing
%     end
    
%             fy = 570.2; fx = 567.6;
%             cy = 250.1; cx = 324.7;
        % use box parameters to cancel out incorrect pixels
        higherThanMinX = pCloud(:,:,1)>minX;
        lowerThanMaxX = pCloud(:,:,1)<maxX;
        higherThanMinY = pCloud(:,:,2)>minY;
        lowerThanMaxY = pCloud(:,:,2)<maxY;
        higherThanMinZ = pCloud(:,:,3)>minZ;
        lowerThanMaxZ = pCloud(:,:,3)<maxZ;
        validBboxPixels = higherThanMinX&lowerThanMaxX&...
            higherThanMinY&lowerThanMaxY&...
            higherThanMinZ&lowerThanMaxZ;
        %     minRowminZ = round((minY*fy/minZ + cy)/2);
        %     maxRowminZ = round((maxY*fy/minZ + cy)/2);
        minRowmaxZ = round((minY*fy/maxZ + cy)/2);
        maxRowmaxZ = round((maxY*fy/maxZ + cy)/2);
        %     minColminZ = round((minX*fx/-minZ + cx)/2);
        %     maxColminZ = round((maxX*fx/-minZ + cx)/2);
        minColmaxZ = round((minX*fx/-maxZ + cx)/2);
        maxColmaxZ = round((maxX*fx/-maxZ + cx)/2);
        % define bounding box size
        bbox2D = zeros(minRowmaxZ-maxRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
        % define cropping area for depth image
        upperRowInOrig = max(1,maxRowmaxZ);
        lowerRowInOrig = min(nRow,minRowmaxZ);
        leftColInOrig = max(1,minColmaxZ);
        rightColInOrig = min(nCol,maxColmaxZ);
        % calculate offset
        rowOff = upperRowInOrig-maxRowmaxZ;
        colOff = leftColInOrig-minColmaxZ;
        % map original image crop to bounding box image
        %             depth = pCloud(:,:,3).*validBboxPixels;
        pCloud(:,:,1) = (pCloud(:,:,1) - minX)/abs(minX-maxX);
        pCloud(:,:,2) = (pCloud(:,:,2) - minY)/abs(minY-maxY);
        pCloud(:,:,3) = (pCloud(:,:,3) - minZ)/abs(minZ-maxZ);
        %
        % flip y and z depth maps as to generate a coordinate system
        % that points with Z from camera to world frame
        %
        pCloud(:,:,2)=1-pCloud(:,:,2);
        pCloud(:,:,3)=1-pCloud(:,:,3);

        depth = pCloud.*repmat(validBboxPixels,1,1,3);
        bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...   
            depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);

        imdb.images.data(:,:,:,sInd) = single(imresize(bbox2D,[cropH cropW],'nearest'));

%         depth = pCloud(:,:,3).*validBboxPixels;
%         imdb.images.data(:,:,1,sInd) = imresize(depth(uppermost:lowermost,leftmost:rightmost),[cropH cropW]);
        jointIds = ([jointContainer{i}.id]+1);
        tempJoints = [jointContainer{i}.coor];
        % incorporate bounding box here
        tempJoints(1,:) = (tempJoints(1,:) - minX)/abs(minX-maxX);
        tempJoints(2,:) = (tempJoints(2,:) - minY)/abs(minY-maxY);
        tempJoints(3,:) = (tempJoints(3,:) - minZ)/abs(minZ-maxZ);
        %
        % flip y and z coordinates as to generate a coordinate system
        % that points with Z from camera to world frame
        %
        tempJoints(2,:) = 1-tempJoints(2,:);
        tempJoints(3,:) = 1-tempJoints(3,:);
        
        tempLabels = NaN(3,12);
        tempLabels(:,jointIndLookup(jointIds))  = tempJoints;
        imdb.images.labels(:,:,1:3*12,sInd)     = single(reshape(tempLabels,1, 1, [], 1));
        imdb.meta.bbox(sInd,:)                  = [minX maxX -maxY -minY -maxZ -minZ];
        imdb.meta.jointIDs{sInd}                = jointIds;
        [~,seqName] = fileparts(sequences(iSeq).name);
        imdb.meta.seqIDs{sInd}                  = seqName;
        % split via activities
%         imdb.images.set(sInd) = 1+single(str2double(sequences(iSeq).name(7))>3);
        % split via actors
        imdb.images.set(sInd) = 2; %1+strcmp(sequences(iSeq).name(5),'c');
    else
        sampleIdx = sampleIdx-1;
    end
end
sampleIdx = sampleIdx+iFrame;
end
clear cloudContainer
% add the offset to x- and y- again
% N=size(imdb.images.data,4);
% for i=1:N
%     imdb.images.labels(:,:,1:3:end,i) = imdb.images.labels(:,:,1:3:end,i)+...
%         imdb.meta.bboxMinXminY(i,1);
%     imdb.images.labels(:,:,2:3:end,i) = imdb.images.labels(:,:,2:3:end,i)+...
%         imdb.meta.bboxMinXminY(i,2);
%     disp(i)
% end
%%
% plot imdb
figure
N=size(imdb.images.data,4);
for i = 1:N
    imagesc(imdb.images.data(:,:,:,i)); axis image;
    pause(0.03)
end
%% test on some images

numSamples = 20;
trainIdx = find(imdb.images.set == 2);
randTrainIdx = randperm(numel(trainIdx),numSamples);
fullJointInds   = [1 2 3 4 5 7 8 9 12 13 16 17];
jointIndLookup  = zeros(1,max(fullJointInds));
jointIndLookup(fullJointInds) = 1:numel(fullJointInds);
for iTrainSample = 1:numSamples
    im = imdb.images.data(:,:,:,trainIdx(randTrainIdx(iTrainSample)));
    lbl = imdb.images.labels(1,1,:,trainIdx(randTrainIdx(iTrainSample)));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_scnseg(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', true, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x));
    GTjoints = squeeze(lbl);
    figure('position',[100 200 1400 800]);
    % depth map
    subplot(1,3,1)
    imagesc(im(:,:,3));
    axis image
    title('depth map')
    % ground truth pose
    subplot(1,3,2)
    GTjoints = reshape(GTjoints,3,[]);
    dummyJoints = NaN(3,20);
    dummyJoints(:,jointIndLookup~=0) = ...
        GTjoints(:,jointIndLookup(jointIndLookup~=0));
    drawEvalSkel(reshape(dummyJoints',[],1),'*g','3d','EVAL');
    GTjoints = reshape(GTjoints,[],1);
    GTjoints=GTjoints(~isnan(GTjoints));
%     plot3(GTjoints(1:3:end),GTjoints(2:3:end),GTjoints(3:3:end),'*g')
    jointIDs = imdb.meta.jointIDs{trainIdx(randTrainIdx(iTrainSample))};
    text(double(GTjoints(1:3:end)),double(GTjoints(2:3:end)),double(GTjoints(3:3:end))...
        ,num2str(jointIDs'));
%     set(gca,'ydir','reverse')
    axis image vis3d
    view(2)
%     axlimsGT = axis;
    title('ground truth pose')
    % estimated pose
    subplot(1,3,3)
    joints = reshape(joints,3,[]);
    dummyJoints = NaN(3,20);
    dummyJoints(:,jointIndLookup~=0) = ...
        joints(:,jointIndLookup(jointIndLookup~=0));
    drawEvalSkel(reshape(dummyJoints',[],1),'*b','3d','EVAL');
    joints = reshape(joints,[],1);
    joints=joints(~isnan(joints));
%     plot3(GTjoints(1:3:end),GTjoints(2:3:end),GTjoints(3:3:end),'*g')
    jointIDs = imdb.meta.jointIDs{trainIdx(randTrainIdx(iTrainSample))};
    text(double(joints(1:3:end)),double(joints(2:3:end)),double(joints(3:3:end))...
        ,num2str(fullJointInds'));

%     joints(isnan(GTjoints))=NaN;
%     plot3(joints(1:3:end),joints(2:3:end),joints(3:3:end),'*b')
%     set(gca,'ydir','reverse')
%     axis(axlimsGT);
    axis image vis3d
    view(2)
    title('estimated pose')
    drawnow
    pause
end

%% compute per-joint average absolute distance
batchSize = 1;
testIdx = find(imdb.images.set == 2);
perJointCollector   = cell(1);
avgErrCollector     = cell(1);
precisionCollector  = cell(1);
PCPcollector        = cell(1);
% randTrainIdx = randperm(numel(trainIdx),numSamples);
for iTrainSample = 1:ceil(numel(testIdx)/batchSize)
    firstIdx = (iTrainSample-1)*batchSize+1;
    lastIdx = min((iTrainSample)*batchSize,numel(testIdx));
    im = imdb.images.data(:,:,:,testIdx(firstIdx:lastIdx));
    lbl = imdb.images.labels(:,:,:,testIdx(firstIdx:lastIdx));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_scnseg(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', true, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x));
    GTjoints = squeeze(lbl);
    % retrieve bbox measurements
    bbox = imdb.meta.bbox(testIdx(firstIdx:lastIdx),:);
    dX = abs(bbox(1)-bbox(2));
    dY = abs(bbox(3)-bbox(4));
    dZ = abs(bbox(5)-bbox(6));
    boxMultiplier = repmat([dX;dY;dZ],12,1);
    % calc per joint error
    diffsq = ((joints-GTjoints).*(boxMultiplier)).^2;
    sumPerJoint = diffsq(1:3:end) + diffsq(2:3:end) + diffsq(3:3:end);
    perJointCollector{end+1} = sumPerJoint;
    avgPrecision = nnz(sqrt(sumPerJoint(~isnan(sumPerJoint)))<0.1)/nnz(~isnan(sumPerJoint));
    precisionCollector{end+1} = avgPrecision;
    avgErr = sum(sqrt(sumPerJoint(~isnan(sumPerJoint))))/nnz(~isnan(sumPerJoint));
    avgErrCollector{end+1} = avgErr;
    fprintf('tested frame %d of %d, avg dist: %5.3fm\n',iTrainSample,numel(testIdx),avgErr);
end

avgErrCollector=avgErrCollector(2:end);
perJointCollector=perJointCollector(2:end);
precisionCollector=precisionCollector(2:end);

avgErr = mean([avgErrCollector{:}])
avgPrecision = mean([precisionCollector{:}])
%% calculate error for each bodypart (12 parts)
bodyParts = {'Head','Chest','RShoulder','LShoulder','RKnee','LKnee','RAnkle','LAnkle','RWrist','LWrist','RElbow','LElbow'}';
bodyPartIDs = [2, 1, 3, 7, 12, 16, 13, 17, 5, 9, 4, 8];
perPartDists = sqrt([perJointCollector{:}]);
CleanPerPartDists = zeros(size(perPartDists));
CleanPerPartDists(~isnan(perPartDists)) = perPartDists(~isnan(perPartDists));
perPartAverage = sum(CleanPerPartDists,2)./sum(~isnan(perPartDists),2);
perPartPrecision = sum(CleanPerPartDists<0.1,2)./sum(~isnan(perPartDists),2);

table(bodyParts, perPartAverage(jointIndLookup(bodyPartIDs)), perPartPrecision(jointIndLookup(bodyPartIDs)),'VariableNames',{'bodypart','avgDist','precision'})






