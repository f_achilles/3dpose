dbpath = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\EVAL';
% skeletonSubdir = 'ScreenSkeleton_20joints';
sequences = dir([dbpath filesep '*.bin']);

keyfrmIdx=1;

nRow = 240;
nCol = 320;
fullJointInds   = [1 2 3 4 5 7 8 9 12 13 16 17];
jointIndLookup  = zeros(1,max(fullJointInds));
jointIndLookup(fullJointInds) = 1:numel(fullJointInds);
sampleIdx = 0;

bboxMode = 'dynamic'; % 'ratio', 'static', 'dynamic'

for iSeq = 1 %:numel(sequences)
dbFile      = fopen([dbpath filesep sequences(iSeq).name]);
cloudContainer = cell(1);
jointContainer = cell(1);
markerContainer = cell(1);
iFrame = 0;
while true
mNum_frameNum   = fread(dbFile,2,'int32=>single');
if feof(dbFile)
    break
end
iFrame = iFrame+1;
fprintf('Frame %3d\n',iFrame);
tempPCloud = fread(dbFile, [3 nCol*nRow],'*single');
pCloud = zeros([nRow nCol],'single');
pCloud(:,:,1) = rot90(reshape(tempPCloud(1,:),[nCol nRow]),-1);
pCloud(:,:,2) = rot90(reshape(tempPCloud(2,:),[nCol nRow]),-1);
pCloud(:,:,3) = rot90(reshape(tempPCloud(3,:),[nCol nRow]),-1);
cloudContainer{1,1,1,iFrame} = pCloud;

nMarkers        = fread(dbFile,1,'int32=>single');
maxNumMarkers = 32;
markers = struct('name','test','coor',zeros(1,3,'single'));
for i=1:maxNumMarkers
    markers(i).name = fread(dbFile,256,'*char');
    markers(i).coor = fread(dbFile,3,'*single');
end
markerContainer{1,1,1,iFrame} = markers(1:min(nMarkers,maxNumMarkers));

nJoints = fread(dbFile,1,'int32=>single');
maxNumJoints = 32;
joints = struct('id',zeros(1,'single'),'coor',zeros(1,3,'single'));
for i=1:maxNumJoints
    joints(i).id = fread(dbFile,1,'int32=>single');
    joints(i).coor = fread(dbFile,3,'*single');
end
jointContainer{1,1,1,iFrame} = joints(1:nJoints);
end
fclose(dbFile);
disp(['Finished reading ' sequences(iSeq).name]);

% crop bounding boxes out of Z-frames and resize to [cropH cropW]
cropH = 120;
cropW = 60;
ratio = cropH/cropW;
offs = 0.1;
f = 5.453e-3;
% figure;

%%%
% For re-calculating the X- and Y- pointcloud coordinates, the
% calibrated-camera paramters of this calibration:
% https://vision.in.tum.de/data/datasets/rgbd-dataset/file_formats
% number "Freiburg 3", could be useful.
% fx: 567.6, fy 570.2, cx 324.7, cy 250.1 (rest: 0!)
%
% Formula: (2*z/fy)*(col-cy/2) = y;
%%%
fx = 567.6;
fy = 570.2;
cx = 324.7;
cy = 250.1;
% [colIdx,rowIdx] = meshgrid(1:nCol,1:nRow);

for i=1:min(200,iFrame)
    sInd = i+sampleIdx;
    
    if ~isempty(jointContainer{i})
        
    % remove background
    jointsPos = [jointContainer{i}.coor];
    minX = min(jointsPos(1,:))-offs;
    maxX = max(jointsPos(1,:))+offs;
    minY = min(jointsPos(2,:))-offs;
    maxY = max(jointsPos(2,:))+offs;
    minZ = min(jointsPos(3,:))-offs;
    maxZ = max(jointsPos(3,:))+offs;
    
    pCloud = cloudContainer{i};
    higherThanMinX = pCloud(:,:,1)>minX;
    lowerThanMaxX = pCloud(:,:,1)<maxX;
    higherThanMinY = pCloud(:,:,2)>minY;
    lowerThanMaxY = pCloud(:,:,2)<maxY;
    higherThanMinZ = pCloud(:,:,3)>minZ;
    lowerThanMaxZ = pCloud(:,:,3)<maxZ;
    validBboxPixels = higherThanMinX&lowerThanMaxX&...
                higherThanMinY&lowerThanMaxY&...
                higherThanMinZ&lowerThanMaxZ;
    X = pCloud(:,:,1) .* validBboxPixels;
    Y = pCloud(:,:,2) .* validBboxPixels;
    Z = pCloud(:,:,3) .* validBboxPixels;
    
    switch bboxMode
        case 'ratio'
        % constant ratio bbox
        minX = min(jointsPos(1,:))-offs;
        maxX = max(jointsPos(1,:))+offs;
        minY = min(jointsPos(2,:))-offs;
        maxY = max(jointsPos(2,:))+offs;
        deltaX = maxX-minX;
        deltaY = maxY-minY;

        XZYratio = deltaY/deltaX;
        % adjust aspect ratio
        if XZYratio<ratio %deltaY too short
            increase = ratio*deltaX-deltaY;
            minY = minY-increase/2;
            maxY = maxY+increase/2;
        elseif XZYratio>ratio %deltaX too short
            increase = deltaY/ratio-deltaX;
            minX = minX-increase/2;
            maxX = maxX+increase/2;
        else
            % nothing
        end
        minZ = min(jointsPos(3,:))-offs;
        maxZ = max(jointsPos(3,:))+offs;
        case 'static'
        % constant measurements bbox
        centerX = (max(jointsPos(1,:))+min(jointsPos(1,:)))/2;
        centerY = (max(jointsPos(2,:))+min(jointsPos(2,:)))/2;
        centerZ = (max(jointsPos(3,:))+min(jointsPos(3,:)))/2;
        minX = centerX - staticBboxEdgLen/2;
        maxX = centerX + staticBboxEdgLen/2;
        minY = centerY - staticBboxEdgLen/2;
        maxY = centerY + staticBboxEdgLen/2;
        minZ = centerZ - staticBboxEdgLen/2;
        maxZ = centerZ + staticBboxEdgLen/2;
        case 'dynamic'
        % dynamic bbox tightly fitted to subject
        minX = min(X(:));
        maxX = max(X(:));
        minY = min(Y(:));
        maxY = max(Y(:));
        minZ = min(Z(Z~=0));
        maxZ = max(Z(Z~=0));
    end
    
        fy = 570.2; fx = 567.6;
        cy = 250.1; cx = 324.7;
        % use box parameters to cancel out incorrect pixels
        higherThanMinX = pCloud(:,:,1)>minX;
        lowerThanMaxX = pCloud(:,:,1)<maxX;
        higherThanMinY = pCloud(:,:,2)>minY;
        lowerThanMaxY = pCloud(:,:,2)<maxY;
        higherThanMinZ = pCloud(:,:,3)>minZ;
        lowerThanMaxZ = pCloud(:,:,3)<maxZ;
        validBboxPixels = higherThanMinX&lowerThanMaxX&...
            higherThanMinY&lowerThanMaxY&...
            higherThanMinZ&lowerThanMaxZ;
    %     minRowminZ = round((minY*fy/minZ + cy)/2);
    %     maxRowminZ = round((maxY*fy/minZ + cy)/2);
        minRowmaxZ = round((minY*fy/maxZ + cy)/2);
        maxRowmaxZ = round((maxY*fy/maxZ + cy)/2);
    %     minColminZ = round((minX*fx/-minZ + cx)/2);
    %     maxColminZ = round((maxX*fx/-minZ + cx)/2);
        minColmaxZ = round((minX*fx/-maxZ + cx)/2);
        maxColmaxZ = round((maxX*fx/-maxZ + cx)/2);
        % define bounding box size
        bbox2D = zeros(minRowmaxZ-maxRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
        % define cropping area for depth image
        upperRowInOrig = max(1,maxRowmaxZ);
        lowerRowInOrig = min(nRow,minRowmaxZ);
        leftColInOrig = max(1,minColmaxZ);
        rightColInOrig = min(nCol,maxColmaxZ);
        % calculate offset
        rowOff = upperRowInOrig-maxRowmaxZ;
        colOff = leftColInOrig-minColmaxZ;
        % map original image crop to bounding box image
%             depth = pCloud(:,:,3).*validBboxPixels;
        pCloud(:,:,1) = (pCloud(:,:,1) - minX)/abs(minX-maxX);
        pCloud(:,:,2) = (pCloud(:,:,2) - minY)/abs(minY-maxY);
        pCloud(:,:,3) = (pCloud(:,:,3) - minZ)/abs(minZ-maxZ);
        %
        % flip y and z depth maps as to generate a coordinate system
        % that points with Z from camera to world frame
        %
        pCloud(:,:,2)=1-pCloud(:,:,2);
        pCloud(:,:,3)=1-pCloud(:,:,3);

%         depth = pCloud.*repmat(validBboxPixels,1,1,3);
%         bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...   
%             depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);
% 
%         imdb.images.data(:,:,:,sInd) = single(imresize(bbox2D,[cropH cropW],'nearest'));

        depth = cat(3,X,-Y,-Z);

%         depth = pCloud(:,:,3).*validBboxPixels;
%         imdb.images.data(:,:,1,sInd) = imresize(depth(uppermost:lowermost,leftmost:rightmost),[cropH cropW]);
        jointIds = ([jointContainer{i}.id]+1);
        tempJoints = [jointContainer{i}.coor];
        % incorporate bounding box here
%         tempJoints(1,:) = (tempJoints(1,:) - minX)/abs(minX-maxX);
%         tempJoints(2,:) = (tempJoints(2,:) - minY)/abs(minY-maxY);
%         tempJoints(3,:) = (tempJoints(3,:) - minZ)/abs(minZ-maxZ);
        %
        % flip y and z coordinates as to generate a coordinate system
        % that points with Z from camera to world frame
        %
        tempJoints(2,:) = -tempJoints(2,:);
        tempJoints(3,:) = -tempJoints(3,:);

        tempLabels = NaN(3,12);
        tempLabels(:,jointIndLookup(jointIds)) = tempJoints;
        
        [~,seqID] = fileparts(sequences(iSeq).name);
        savepcd(sprintf('%s%simage_kf%03d_%s_frame%d.pcd',dbpath,filesep,keyfrmIdx,seqID,i),depth);
        savepcd(sprintf('%s%smarkers_kf%03d_%s_frame%d.pcd',dbpath,filesep,keyfrmIdx,seqID,i),tempLabels); % rawJointsPos is 3xN
        keyfrmIdx=keyfrmIdx+1;

    else
        sampleIdx = sampleIdx-1;
    end
end
sampleIdx = sampleIdx+iFrame;
end
clear cloudContainer
% add the offset to x- and y- again
% N=size(imdb.images.data,4);
% for i=1:N
%     imdb.images.labels(:,:,1:3:end,i) = imdb.images.labels(:,:,1:3:end,i)+...
%         imdb.meta.bboxMinXminY(i,1);
%     imdb.images.labels(:,:,2:3:end,i) = imdb.images.labels(:,:,2:3:end,i)+...
%         imdb.meta.bboxMinXminY(i,2);
%     disp(i)
% end
%%
% plot imdb
figure
N=size(imdb.images.data,4);
for i = 1:N
    imagesc(imdb.images.data(:,:,:,i)); axis image;
    pause(0.03)
end