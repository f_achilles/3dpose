% clear all
% clear imdb
dataRoots = {'E:\mocapPose\10febRecordings','E:\mocapPose\12febRecordings','E:\mocapPose\18febRecordings'};
actorRounds = [1 1 2];

desirKinAngle = 360;

outFormat = repmat('%f ',1,3*13);
outFormat = [outFormat(1:end-1) '\n'];

imdb = struct('images',[]);
imdb.images.data = single([]);
imdb.images.labels = single([]);
iDB = 1;
for iRoot = 1:numel(dataRoots)
    for iRound = 1:actorRounds(iRoot);
        dataRoot = dataRoots{iRoot};
        mocapDir = [dataRoot filesep 'syncedData'];
        kinectDir = [dataRoot filesep 'kinectData'];
        switch iRoot
            case 1
                sequences = {'bodyCalib','getUp','backcontact'};
                actors = {'Felix'};
            case 2
                sequences = {'calib','getUp','drink','read'};
                actors = {'Huseyin'};
            case 3
                switch iRound
                    case 1
                        sequences = {'read', 'drink', 'bodyCalib', 'sleep', 'seizure'};
                        actors = {'Felix'};
                    case 2
                        sequences = {'sleep', 'drink'};
                        actors = {'Patrick'};
                end
            otherwise
        end
        for iSeq = 1:numel(sequences)
            for iAct = 1:numel(actors)
                mdataFile = dir(fullfile(kinectDir,['mdata_' sequences{iSeq} '*_' actors{iAct} '.mat']));
                load(fullfile(kinectDir,mdataFile.name));
                for iFr = 1:1800
                    mocapFile = dir(fullfile(mocapDir,sprintf('joints_sync_%s*_%s_frame%d.txt',sequences{iSeq},actors{iAct},iFr)));
                    fid = fopen(fullfile(mocapDir,mocapFile.name));
                    mocap_in = fscanf(fid,outFormat);
                    fclose(fid);

                    kinect_in = rot90(single(flipud(mdata.depth{iFr+1})));
                    imdb.images.data(:,:,1,iDB) = single(imresize(kinect_in(75:450,50:350),[120 60],'nearest'));
                    
                    % set 0,0,0-joints to NaN (NO! They won't be 0,0,0 but "0-bedCenter"!)
                    joints = single(reshape(mocap_in,3,[]));
                    bedcenterFile = dir(fullfile(kinectDir,['bedCenter_*' sequences{iSeq} '*_' actors{iAct} '.mat']));
                    load(fullfile(kinectDir,bedcenterFile.name));
                    bedCenter = reshape(bedCenter,3,1);
                    for iJ = 1:size(joints,2)
                        if norm(joints(:,iJ)+bedCenter) < 10
                            joints(:,iJ) = NaN;
                        end
                    end
                    mocap_in = reshape(joints,size(mocap_in));
                    
                    imdb.images.labels(1,1,:,iDB) = single(mocap_in(:));

                    imdb.meta.info(iDB).frame = iFr;
                    imdb.meta.info(iDB).sequence = sequences{iSeq};
                    imdb.meta.info(iDB).angle = desirKinAngle;
                    imdb.meta.info(iDB).actor = actors{iAct};

                    iDB = iDB + 1;

                    fprintf('added frame %d of sequence ''%s'' (actor %s) to IMDB\n',iFr,sequences{iSeq},actors{iAct});
                end
            end
        end
    end
end

% plot
figure; imagesc(imdb.images.data(:,:,1,1)); axis image

% save imdb
save(fullfile(dataRoot,'imdb_all'),'imdb','-v7.3')