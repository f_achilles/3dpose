% % clear all
% % clear imdb
% dataRoot = 'E:\mocapPose\18febRecordings';
% mocapDir = [dataRoot filesep 'syncedData'];
% kinectDir = [dataRoot filesep 'kinectData\depthFrames'];
% 
% % sequences = {'read', 'drink', 'bodyCalib', 'sleep', 'seizure'}; %{'calib','getUp','drink'};
% % actors = {'Felix'}; %{'Huseyin'};
% 
% sequences = {'sleep', 'drink'};
% actors = {'Patrick'};
% 
% desirKinAngle = 360;
% 
% outFormat = repmat('%f ',1,3*13);
% outFormat = [outFormat(1:end-1) '\n'];
% 
% imdb = struct('images',[]);
% imdb.images.data = single([]);
% imdb.images.labels = single([]);
% iDB = 1;
% for iSeq = 1:numel(sequences)
%     for iAct = 1:numel(actors)
%         for iFr = 1:1800
%             mocapFile = dir(fullfile(mocapDir,sprintf('joints_sync_%s*_%s_frame%d.txt',sequences{iSeq},actors{iAct},iFr)));
%             fid = fopen(fullfile(mocapDir,mocapFile.name));
%             mocap_in = fscanf(fid,outFormat);
%             fclose(fid);
%             depthFile = dir(fullfile(kinectDir,['*' sequences{iSeq} '*_' actors{iAct} '_frame' num2str(iFr) '_*' num2str(desirKinAngle) '.png']));
%             kinect_in = imread(fullfile(kinectDir,depthFile.name));
% 
%             imdb.images.data(:,:,1,iDB) = single(imresize(single(flipud(kinect_in(:,101:220))),0.5,'bilinear'));
%             
%             % set 0,0,0-joints to NaN (NO! They won't be 0,0,0 but bedCenter!)
%             joints = reshape(mocap_in,3,[]);
%             imdb.images.labels(1,1,:,iDB) = single(mocap_in(:));
% 
%             imdb.meta.info(iDB).frame = iFr;
%             imdb.meta.info(iDB).sequence = sequences{iSeq};
%             imdb.meta.info(iDB).angle = desirKinAngle;
%             imdb.meta.info(iDB).actor = actors{iAct};
% 
%             iDB = iDB + 1;
% 
%             fprintf('added frame %d of sequence ''%s'' to IMDB\n',iFr,sequences{iSeq});
%         end
%     end
% end
% 
% % plot
% figure; imagesc(imdb.images.data(:,:,1,1)); axis image
% 
% % save imdb
% save(fullfile(dataRoot,'imdb_patrick'),'imdb','-v7.3')
%%
% clear all
% clear imdb
dataRoots = {'E:\mocapPose\March15'}; %,'E:\mocapPose\12febRecordings','E:\mocapPose\18febRecordings'};
actorRounds = [1];

desirKinAngle = 360;

jointFormat = repmat('%f ',1,3*14);
jointFormat = [jointFormat(1:end-1) '\n'];

imdb = struct('images',[]);
imdb.images.data = single([]);
imdb.images.labels = single([]);
iDB = 0;
for iRoot = 1:numel(dataRoots)
    for iRound = 1:actorRounds(iRoot);
        dataRoot = dataRoots{iRoot};
        mocapDir = [dataRoot filesep 'syncedMocap'];
        kinectDir = [dataRoot filesep 'kinectData\depthFrames'];
        kinectSrc = [dataRoot filesep 'kinectData'];
        switch iRoot
            case 1
%                 switch iRound
%                     case 1
                        sequences = {'bodyCalib','eatSO','eatS','readS','sleepS','objectsS','sleepH','getUpH','movementsH','getInBedH'};
                        actors = {'Meng'};
%                     case 2
%                         sequences = {'bodyCalib','eatSO','eatS','readS','sleepS','objectsS','getInBedH'};
%                         actors = {'Felix'};
%                 end
                
            case 2
                sequences = {'calib','getUp','drink','read'};
                actors = {'Huseyin'};
            case 3
                switch iRound
                    case 1
                        sequences = {'read', 'drink', 'bodyCalib', 'sleep', 'seizure'};
                        actors = {'Felix'};
                    case 2
                        sequences = {'sleep', 'drink'};
                        actors = {'Patrick'};
                end
            otherwise
        end
        bedcenterFile = dir(fullfile(kinectSrc,['bedCenter_*.mat']));
        load(fullfile(kinectSrc,bedcenterFile.name));
        bedCenter = reshape(bedCenter,3,1);
        for iSeq = 1:numel(sequences)
            for iAct = 1:numel(actors)
                tic
                imBlock = cell(1);
                lblBlock = cell(1);
                parfor iFr = 1:1800
                    mocapFile = dir(fullfile(mocapDir,sprintf('joints_sync_%s_%s_frame%d.txt',sequences{iSeq},actors{iAct},iFr)));
                    fid = fopen(fullfile(mocapDir,mocapFile.name));
                    mocap_in = fscanf(fid,jointFormat);
                    fclose(fid);
                    
                    depthFile = dir(fullfile(kinectDir,['*_' sequences{iSeq} '_' actors{iAct} '_frame' num2str(iFr) '_*_' num2str(desirKinAngle) '.png']));
                    kinect_in = imread(fullfile(kinectDir,depthFile.name));
                    imBlock{iFr} = single(imresize(single(flipud(kinect_in(:,101:220))),0.5,'nearest'));
                    
                    % set 0,0,0-joints to NaN (NO! They won't be 0,0,0 but "0-bedCenter"!)
                    joints = single(reshape(mocap_in,3,[]));
%                     bedcenterFile = dir(fullfile(kinectSrc,['bedCenter_*' sequences{iSeq} '*_' actors{iAct} '.mat']));
                    
                    for iJ = 1:size(joints,2)
                        if norm(joints(:,iJ)+bedCenter) < 10
                            joints(:,iJ) = NaN;
                        end
                    end
                    mocap_in = reshape(joints,size(mocap_in));
                    
                    lblBlock{iFr} = single(mocap_in(:));
        
                    info(iFr).frame = iFr;
                    info(iFr).sequence = sequences{iSeq};
                    info(iFr).angle = desirKinAngle;
                    info(iFr).actor = actors{iAct};
                end
                imdb.images.data(:,:,:,iDB+(1:1800)) = reshape(cell2mat(imBlock),[120 60 1 1800]);
                imdb.images.labels(:,:,:,iDB+(1:1800)) = reshape(cell2mat(lblBlock),[1 1 42 1800]);
                for iFr = 1:1800
                    imdb.meta.info(iDB+iFr).frame = iFr;
                    imdb.meta.info(iDB+iFr).sequence = sequences{iSeq};
                    imdb.meta.info(iDB+iFr).angle = desirKinAngle;
                    imdb.meta.info(iDB+iFr).actor = actors{iAct};
                end
                iDB = iDB + 1800;
                toc
                fprintf('added sequence ''%s'' (actor %s) to IMDB\n',sequences{iSeq},actors{iAct});
            end
        end
    end
end

% save imdb
save(fullfile(dataRoot, 'imdb_rendered_mar15_Meng_NN_withNaN.mat'),'imdb','-v7.3')

% plot
figure; imagesc(imdb.images.data(:,:,1,1)); axis image