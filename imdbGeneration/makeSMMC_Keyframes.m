% important keyframes: [seq-nr, frame-nr; ...
keyfrms = [ 0, 60;
0, 90;
1, 100;
10,290;
10,380;
12,120;
12,300;
13,310;
17,240;
20,160;
20,200;
23,150;
23,345;
24, 70;
24,100;
24,130;
24,170];
keyfrmIdx = 1;
%%
%% conversion of SMMC/1-database to IMDB format of MatConvNet
%
% database URL: http://ai.stanford.edu/~varung/cvpr10/
%
% camera used?
% intrinsics?
% // from readme:
% float intrinsic[] = {
%   2.844376,0.000000,0.000000,0.000000,
%   0.000000,2.327216,0.000000,0.000000,
%   0.000000,0.000000,-1.000020,-1.000000,
%   0.000000,0.000000,-0.200002,0.000000
% };
% OpenGL projection matrix: http://www.songho.ca/opengl/gl_projectionmatrix.html
dbpath = 'C:\Projects\CombinedSegmentationAndPoseEstimation\data\SMMC';
sequences = dir([dbpath filesep '*.bin']);
numJoints = 18;
bboxMode = 'ratio';
nRow = 176;
nCol = 144;
% maps from full 64 SMMC-10 indices to imdb indices
% IMDB body parts:
bodyParts = {...
'Chest','LFoot','RFoot','LElbow','RElbow',...
'LWrist','RWrist','Head','Bellybutton','LFingers',...
'RFingers','Neck','LKnee','RKnee','LHip',...
'RHip','LShoulder','RShoulder'}';
fullJointInds   = [11 36 44 20 29 18 26 2 11 17 25 4 38 42 15 16 22 27];
jointIndLookup  = zeros(1,64);
jointIndLookup(fullJointInds) = 1:numel(fullJointInds);
sInd = 1;
for iSeq = 1:numel(sequences)
dbFile          = fopen([dbpath filesep sequences(iSeq).name]);
cloudContainer  = cell(1);
markerContainer = cell(1);
iFrame = 0;
seqname = regexp(sequences(iSeq).name,'\.','split');
seqname = seqname{1};
seqID = regexp(seqname,'_','split');
seqID = str2double(seqID{2});
while true
% read magic nuber
magicNumber   = fread(dbFile,1,'int32=>single');
% read frame number
mNum_frameNum   = fread(dbFile,1,'int32=>single');
% read timestamp
timestamp       = fread(dbFile,1,'int64=>double');
iFrame = iFrame+1;
fprintf('Frame %3d\n',iFrame);
% read pointcloud
% put negative sign for positive depth values and right hand coordinate
% system
tempPCloud = -fread(dbFile, [3 nCol*nRow],'*single');
if feof(dbFile)
break
end
pCloud = zeros([nRow nCol 3],'single');
pCloud(:,:,1) = reshape(tempPCloud(1,:),[nRow nCol]);
pCloud(:,:,2) = reshape(tempPCloud(2,:),[nRow nCol]);
pCloud(:,:,3) = reshape(tempPCloud(3,:),[nRow nCol]);
cloudContainer{1,1,1,iFrame} = pCloud;
% % plot
% figure;
% plot3(reshape(pCloud(:,:,1),1,[]),reshape(pCloud(:,:,2),1,[]),...
%     reshape(pCloud(:,:,3),1,[]),'*b');
% axis vis3d image
% read depth
depth       = fread(dbFile,nCol*nRow,'*uint16');
depth       = reshape(depth,[nRow nCol]);
% read intensity
IRamp       = fread(dbFile,nCol*nRow,'*uint16');
% read depth confidence
confDepth       = fread(dbFile,nCol*nRow,'*uint16');
% read the number of attached markers
nMarkers        = fread(dbFile,1,'int32=>single');
maxNumMarkers   = 64;
markers         = struct('id',0,'frame',0,'coor',zeros(1,3,'single'),'cond',1,'flag',1);
for i=1:maxNumMarkers
markers(i).id   = fread(dbFile,1,'int32=>single');
markers(i).frame= fread(dbFile,1,'int32=>single');
% put negative sign for positive depth values and right hand coordinate
% system
markers(i).coor = -fread(dbFile,3,'*single');
markers(i).cond = fread(dbFile,1,'*single');
markers(i).flag = fread(dbFile,1,'uint32=>single');
end
markerContainer{1,1,1,iFrame} = markers; %(1:min(nMarkers,maxNumMarkers));
%         % plot
%         for i=1:64
%             hold on; plot3(markers(i).coor(1),markers(i).coor(2),markers(i).coor(3),'r*'); hold off;
%             hold on; text(double(markers(i).coor(1)),double(markers(i).coor(2)),double(markers(i).coor(3)),num2str(i)); hold off;
%         end; axis vis3d image
% read some variable which might be a pointer or so in the original
% C-struct
dummy        = fread(dbFile,1,'int32=>single');
end
fclose(dbFile);
disp(['Finished reading ' sequences(iSeq).name]);
numFrames = iFrame-1;
% crop bounding boxes out of Z-frames and resize to [cropH cropW]
cropH = 120;
cropW = 60;
ratio = cropH/cropW;
offs = 0.1;
% Mesa Imaging SR-4000 intrinsics
% taken from https://support.dce.felk.cvut.cz/mediawiki/images/1/18/Dp_2011_smisek_jan.pdf
% fx = fy = 257.6px
fy = 257.6; fx = 257.6;
cy = 176/2; cx = 144/2;
Intrinsics = [ fx 0 cx  0 ;
0 fy cy  0 ;
0  0  1  0 ;
0  0  1  0 ];
for iFrame=1:numFrames
validJoints = [markerContainer{iFrame}.cond] ~= -1;
jointIds = ([markerContainer{iFrame}.id]+1);
jointIds = jointIds(validJoints);
rawJointsPos = [markerContainer{iFrame}.coor];
% filter for interesting coordinates
invalFilteredJoints = ~validJoints(fullJointInds);
jointsPos = rawJointsPos(:,fullJointInds);
% throw out invalid joints
jointsPos(:,invalFilteredJoints) = NaN;
rawJointsPos(:,~validJoints) = NaN;
invalIdx = find(invalFilteredJoints);
for invalIdxi = 1:nnz(invalFilteredJoints)
disp(bodyParts((invalIdx(invalIdxi))))
end
%         figure
%         plot3(jointsPos(1,:),jointsPos(2,:),jointsPos(3,:),'r*');
%         axis vis3d image
pCloud = single(cloudContainer{iFrame});
X   = pCloud(:,:,1);
Y   = pCloud(:,:,2);
Z   = pCloud(:,:,3);
FG  = Z~=0;
switch bboxMode
case 'ratio'
% constant ratio bbox
minX = min(jointsPos(1,:))-offs;
maxX = max(jointsPos(1,:))+offs;
minY = min(jointsPos(2,:))-offs;
maxY = max(jointsPos(2,:))+offs;
deltaX = maxX-minX;
deltaY = maxY-minY;
XZYratio = deltaY/deltaX;
% adjust aspect ratio
if XZYratio<ratio %deltaY too short
increase = ratio*deltaX-deltaY;
minY = minY-increase/2;
maxY = maxY+increase/2;
elseif XZYratio>ratio %deltaX too short
increase = deltaY/ratio-deltaX;
minX = minX-increase/2;
maxX = maxX+increase/2;
else
% nothing
end
minZ = min(jointsPos(3,:))-offs;
maxZ = max(jointsPos(3,:))+offs;
case 'static'
% constant measurements bbox
centerX = (max(jointsPos(1,:))+min(jointsPos(1,:)))/2;
centerY = (max(jointsPos(2,:))+min(jointsPos(2,:)))/2;
centerZ = (max(jointsPos(3,:))+min(jointsPos(3,:)))/2;
minX = centerX - staticBboxEdgLen/2;
maxX = centerX + staticBboxEdgLen/2;
minY = centerY - staticBboxEdgLen/2;
maxY = centerY + staticBboxEdgLen/2;
minZ = centerZ - staticBboxEdgLen/2;
maxZ = centerZ + staticBboxEdgLen/2;
case 'dynamic'
% dynamic bbox tightly fitted to subject
minX = min(jointsPos(1,:))-offs;
maxX = max(jointsPos(1,:))+offs;
minY = min(jointsPos(2,:))-offs;
maxY = max(jointsPos(2,:))+offs;
minZ = min(jointsPos(3,:))-offs;
maxZ = max(jointsPos(3,:))+offs;
end
borders=[[minX;minY;minZ] [maxX;maxY;minZ]];
bordersKinectImgPlane = Intrinsics *...
cat(1,borders,ones(1,2));
bordersKinectImgPlane = ...
bordersKinectImgPlane./...
repmat(bordersKinectImgPlane(4,:),4,1);
minRowmaxZ = round(bordersKinectImgPlane(2,1));
maxRowmaxZ = round(bordersKinectImgPlane(2,2));
minColmaxZ = round(bordersKinectImgPlane(1,1));
maxColmaxZ = round(bordersKinectImgPlane(1,2));
% use box parameters to cancel out incorrect pixels
higherThanMinX = pCloud(:,:,1)>minX;
lowerThanMaxX = pCloud(:,:,1)<maxX;
higherThanMinY = pCloud(:,:,2)>minY;
lowerThanMaxY = pCloud(:,:,2)<maxY;
higherThanMinZ = pCloud(:,:,3)>minZ;
lowerThanMaxZ = pCloud(:,:,3)<maxZ;
validBboxPixels = higherThanMinX&lowerThanMaxX&...
higherThanMinY&lowerThanMaxY&...
higherThanMinZ&lowerThanMaxZ;
% define bounding box size
bbox2D = zeros(maxRowmaxZ-minRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
% define cropping area inside of original depth image
upperRowInOrig = max(1,minRowmaxZ);
lowerRowInOrig = min(nRow,maxRowmaxZ);
leftColInOrig = max(1,minColmaxZ);
rightColInOrig = min(nCol,maxColmaxZ);
% calculate offset
rowOff = upperRowInOrig-minRowmaxZ;
colOff = leftColInOrig-minColmaxZ;
% map original image crop to bounding box image
%         X = (X - minX)/abs(minX-maxX);
%         Y = (Y - minY)/abs(minY-maxY);
%         Z = (Z - minZ)/abs(minZ-maxZ);
if strcmp(bboxMode,'dynamic')
depth = (cat(3,X,Y,Z)*255-255/2).*repmat(FG,1,1,3);
else
depth = cat(3,X,Y,Z).*repmat(validBboxPixels,1,1,3);
end
rawJointsPos(isnan(rawJointsPos))=0;
if seqID == keyfrms(keyfrmIdx,1) && iFrame == keyfrms(keyfrmIdx,2)
savepcd(sprintf('image_kf%d_seq%s_frame%d.pcd',keyfrmIdx,seqname,iFrame),depth);
savepcd(sprintf('markers_kf%d_seq%s_frame%d.pcd',keyfrmIdx,seqname,iFrame),rawJointsPos);
keyfrmIdx=keyfrmIdx+1;
end
%         bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...
%             depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);
%
%         imdb.images.data(:,:,:,sInd) = single(imresize(bbox2D,[cropH cropW]));
%
%         % incorporate bounding box into label and store both in IMDB
%         jointsPos(1,:) = (jointsPos(1,:) - minX)/abs(minX-maxX);
%         jointsPos(2,:) = (jointsPos(2,:) - minY)/abs(minY-maxY);
%         jointsPos(3,:) = (jointsPos(3,:) - minZ)/abs(minZ-maxZ);
%         tempLabels = NaN(3,numJoints);
% %         tempLabels(:,jointIndLookup(validJoints)) = jointsPos;
%         imdb.images.labels(:,:,1:3*numJoints,sInd)     = single(reshape(jointsPos,1, 1, [], 1));
%         imdb.meta.bbox(sInd,:) = 1000*[minX maxX minY maxY minZ maxZ];
%         imdb.meta.sequenceID{sInd} = sequences(iSeq).name;
%         % train/test split
%         imdb.images.set(sInd) = single(2);
%
%         sInd = sInd+1;
%         fprintf('Sequence %s, frame %d/%d.\n',sequences(iSeq).name,iFrame,numFrames)
end % end of frames/sequence
end % end of sequences