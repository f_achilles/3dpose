%% apply network
load('C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat')
net.layers = net.layers(1:end-1);
% gpuDevice(1);
% netG = vl_simplenn_move(net,'gpu');
% result = vl_simplenn_f(net,depthFramesGPU,[],[],opts );
opts.conserveMemory = true;
iC = 1;
for shiftX = -10:5:10
    for shiftY = -10:5:10
        cDepthFrames = circshift(depthFrames,shiftX,2);
        cDepthFrames = circshift(cDepthFrames,shiftY,1);
        if shiftX<=0
            cDepthFrames(:,60+shiftX:60) = 0;
        else
            cDepthFrames(:,1:shiftX) = 0;
        end
        if shiftY<=0
            cDepthFrames(120+shiftY:120,:) = 0;
        else
            cDepthFrames(1:shiftY,:) = 0;
        end
%         depthFramesGPU = gpuArray(cDepthFrames);
%         cresult{iC} = cat(4,vl_simplenn_f(netG,depthFramesGPU(:,:,:,1:90),[],[],opts ),...
%             vl_simplenn_f(netG,depthFramesGPU(:,:,:,91:180),[],[],opts ));
%         cresult_f{iC} = cat(4,vl_simplenn_f(netG,fliplr(depthFramesGPU(:,:,:,1:90)),[],[],opts ),...
%             vl_simplenn_f(netG,fliplr(depthFramesGPU(:,:,:,91:180)),[],[],opts ));
        cresult{iC} = vl_simplenn_f(net,cDepthFrames,[],[],opts);
        cresult_f{iC} = vl_simplenn_f(net,fliplr(cDepthFrames),[],[],opts);
        iC
        iC = iC + 1;
    end
end

%% get mean shape parameters

% which indices represent the shape parameters?
% joints:3*17 + 
% shape: 1*67
shapeInds = 3*17 + (1:67);
shapeParams = zeros(67,180,'single');

for iCollect = 1:numel(cresult)
    shapeParams = shapeParams + ...
        squeeze(gather(cresult{iCollect}(end).x(:,:,shapeInds,:))) + ...
        squeeze(gather(cresult_f{iCollect}(end).x(:,:,shapeInds,:))); %67 x 180
end
shapeParams = shapeParams/(2*numel(cresult));

%% get shape parameter variance

allShapeParams = [];
for iCollect = 1:numel(cresult)
    allShapeParams = cat(3,allShapeParams,...
        squeeze(gather(cresult{iCollect}(end).x(:,:,shapeInds,:))),...
        squeeze(gather(cresult_f{iCollect}(end).x(:,:,shapeInds,:))) );
end
paramVariance = var(allShapeParams,0,3);
paramWeights = 1-(bsxfun(@rdivide,...
    bsxfun(@minus,paramVariance,min(paramVariance,[],1)),...
    max(paramVariance,[],1)-min(paramVariance,[],1)));
%% transform into PCA space
% [coeff,score,eigVal] = pca(shapeParams'); % the first 10 eigenvalues contain all of the information! the rest is just blah
% % shapeParamsT = coeff*score';
% shapeParamsT = score(:,1:10)';

%% compute distance matrix

distMat = zeros(N);

for iCompare = 1:N
    ssdVec = sum(bsxfun(@minus,shapeParams,shapeParams(:,iCompare)).^2,1);
    weightedSsdVec = bsxfun(@rdivide,sum(...
        bsxfun(@times,paramWeights,paramWeights(:,iCompare)).*bsxfun(@minus,shapeParams,shapeParams(:,iCompare)).^2,1),...
    sum(bsxfun(@times,paramWeights,paramWeights(:,iCompare)),1));
    distMat(iCompare,:) = ssdVec; %ssdVec;
end

% convert distance matrix into nearest neighbor matrix, looking for the 9
% nearest neighbors of the k-th sample in row k
nnmat = zeros(size(distMat));
for iCompare = 1:N
    [~,indices] = sort(distMat(iCompare,:));
    nnmat(iCompare,indices(2:10)) = [9:-1:1];
end

% plot
fig = figure; imagesc(-nnmat); title('SSD metric on estimated shape vectors'); axis image
colormap(gray)
dcm_obj = datacursormode(fig);
set(dcm_obj,'UpdateFcn',@myfunction)
showSubjHandle = figure;

%% get some validation metric

% compute 1 vs all error
for iSubj=1:10
    colInds{iSubj} = 1:18+18*(iSubj-1);
end

subjectScores = zeros(1,10);
for iSubj = 1:10
    rowInds = (1:18)+18*(iSubj-1);
    iOtherSubj = 1:10;
    iOtherSubj(iSubj) = [];
    otherSubjCols = [colInds{iOtherSubj}];
    subjectScores(iSubj) = sum(reshape(nnmat(rowInds,rowInds),1,[]))/...
        (sum(reshape(nnmat(rowInds,otherSubjCols),1,[]))/9);
end
fprintf('average score: %f\n',mean(subjectScores))
