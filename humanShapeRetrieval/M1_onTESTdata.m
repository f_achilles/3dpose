dataRoot='E:\dataFelix\peopleRetrieval\Test';
ptclDir = fullfile(dataRoot, 'Clouds');
ptclouds = dir(fullfile(ptclDir,'*.ply'));
N = numel(ptclouds);

depthFrames = zeros(120,60,1,N,'single');

%% read ply data (unfiltered)
parfor iCloud = 1:N
    fprintf('Converting sample %d to depth map...\n',iCloud);
    ccloud = pcread(fullfile(ptclDir,sprintf('%d.ply',iCloud)));
    % % plot
    %     pcshow(ccloud)
    % project to image plane with synthetic intrinsics and apply median on
    % z
    %
    % this will not work as we do not have the real depth...
    %
    % mesh the triangles on xy also does not work, as it is not the
    % projection plane
    %
    % dirty hack!
    % go through volume in 120x60 steps and
    % take median of z value in each cubicle
    minmaxX = ccloud.XLimits;
    minmaxY = ccloud.YLimits;
    minmaxZ = ccloud.ZLimits;
    roibordersX = linspace(minmaxX(1),minmaxX(2),61);
    roibordersY = linspace(minmaxY(1),minmaxY(2),121);
    cdepth = zeros(120,60,'single');
    for iRow = 1:120
        for iCol = 1:60
            croi = [
                    roibordersX(iCol) roibordersX(iCol+1);
                    roibordersY(iRow) roibordersY(iRow+1);
                    minmaxZ(1) minmaxZ(2)
                    ];
            inds = findPointsInROI(ccloud,croi);
            if isempty(inds)
                px = NaN;
            else
                px = median(ccloud.Location(inds,3));
            end
            cdepth(121-iRow,iCol) = px;
        end
    end
       
    % normalize
    cdepth = (cdepth-minmaxZ(1))/(abs(diff(minmaxZ)));

    cdepth(isnan(cdepth)) = 0;
    cdepth = medfilt2(cdepth,[3 3]);
%     % plot
%     figure; surf(cdepth,'edgecolor','none');
%     axis vis3d
%     camlight
%     lighting gouraud
%     figure; imagesc(cdepth); axis image

    depthFrames(:,:,1,iCloud) = cdepth;
    fprintf('...done!\n')
end

save(fullfile(dataRoot, 'Clouds', 'depthFrames.mat'),'depthFrames');
%% visualize input data
figure;
for i = 1:N
    imagesc(depthFrames(:,:,1,i)); axis image
    pause(0.5)
end
%% apply network
load('C:\Projects\3DPoseEstimation\data\networks\Synth\trainOn9views_2M_Z_flip_noise\net-epoch-75.mat')
net.layers = net.layers(1:end-1);
net = vl_simplenn_move(net,'gpu');
depthFramesGPU = gpuArray(depthFrames);
opts.conserveMemory = true;
% which indices represent the shape parameters?
% joints:3*17 + 
% shape: 1*67
shapeInds = 3*17 + (1:67);
% get shape parameters
shapeParams = [];
for i=1:N
    result = vl_simplenn_f(net,depthFramesGPU(:,:,1,i),[],[],opts );
    shapeParams = cat(2,shapeParams,...
        squeeze(gather(result(end).x(:,:,shapeInds,:)))); %67 x 180
    i
end

% %% transform into PCA space
% [coeff,score,eigVal] = pca(shapeParams'); % the first 10 eigenvalues contain all of the information! the rest is just blah
% % shapeParamsT = coeff*score';
% shapeParamsT = score(:,1:10)';

%% compute distance matrix

distMat = zeros(N);

for iCompare = 1:N
    ssdVec = sum(bsxfun(@minus,shapeParams,shapeParams(:,iCompare)).^2,1);
    distMat(iCompare,:) = ssdVec;
end

% convert distance matrix into nearest neighbor matrix, looking for the 9
% nearest neighbors of the k-th sample in row k
nnmat = zeros(size(distMat));
for iCompare = 1:N
    [~,indices] = sort(distMat(iCompare,:));
    nnmat(iCompare,indices(2:100)) = [99:-1:1];
end

% plot
fig = figure; imagesc(-nnmat); title('SSD metric on estimated shape vectors'); axis image
colormap(gray)
dcm_obj = datacursormode(fig);
set(dcm_obj,'UpdateFcn',@myfunction)
showSubjHandle = figure;

%% write distance matrix (dissimilarity matrix) to file
outFormat = repmat('%.12f ',1,N);
outFormat = [outFormat(1:end-1) '\n'];
fid = fopen(fullfile(dataRoot,'BodyNet1_Achilles.txt'),'w');
fprintf(fid,outFormat,distMat);
fclose(fid);
%% test conversion errors
dissimMat = dlmread(fullfile(dataRoot,'BodyNet1_Achilles.txt'),' ');
compare_nnmat = zeros(size(dissimMat));
for iCompare = 1:N
    [~,indices] = sort(dissimMat(iCompare,:));
    compare_nnmat(iCompare,indices(2:100)) = [99:-1:1];
end

if all(all(compare_nnmat == nnmat))
    disp('YAY')
else
    disp([num2str(sum(nnz(compare_nnmat ~= nnmat))) ' positions are different!'])
end

