% parameters
cx = 320;
cy = 240;
fx = 574;
fy = 574; % 525 is too far off! Keisuke uses 574 from the camera factory standards
nRow = 480;
nCol = 640;
[Xinds,Yinds] = meshgrid(1:nCol,1:nRow);
Intrinsics = [ fx 0 cx  0 ;
               0 fy cy  0 ;
               0  0  1  0 ;
               0  0  1  0 ];
nJ = 17;
limbIdx = [11,2,2,1,11,11,9,16,6,13,2,2,4,4,10,17,8,15;3,11,1,4,9,16,6,13,7,14,9,16,10,17,8,15,5,12];
hSkel = [];
firstFrame = true;
makeMovie = false;
if makeMovie
    movObj = VideoWriter('3Dpose_demo4.avi');
    movObj.FrameRate=15;
    open(movObj)
end
figure('position',[0 0 1024 768])
% load('C:\Projects\poseFromRGBD\data\net-epoch-75.mat')
% net.layers = net.layers(1:end-1);

% init device
mxNI(0)
% set resolution to 480x640, @30FPS
mxNI(13, 4)
% image to depth registration off, just in case
mxNI(4)

while true
    % acquire frame
    [Z] = mxNI(2);
    Z = single(Z);
    % compute X and Y
    X = Z .* (Xinds-cx) / fx;
    Y = Z .* (Yinds-cy) / fy;

%     % draw cloud
%     FG = Z==0;
%     Z(FG) = NaN;
%     plot3(X(:),Y(:),Z(:),'b*')
%     axis vis3d image
%     view(3)
%     drawnow

    % clip depth
    validDepth = Z>1800 & Z<2750;
    % clip ground plane
    validHeight = Y>-1000 & Y<950;
    % clip width just as commodity
    validWidth = X>-1300 & X<1300;
    mask = validHeight&validDepth&validWidth;
    
    if ~any(mask(:))
        pause(0.03)
        continue
    end
    
    % get rid of small outliers in the mask
    mask = single(bwareafilt(mask, [1000 640*480]));
    
    X = X.*mask;
    Y = Y.*mask;
    Z = Z.*mask;
    
    % draw clipped image
    if firstFrame
        hImg = imagesc(Z); axis image off; colormap(hot); caxis([1800 2800]);
        firstFrame = false;
    else
        set(hImg,'cdata',Z);
    end

    % calculate extremal points
    minX = min(X(X~=0));
    maxX = max(X(X~=0));
    minY = min(Y(Y~=0));
    maxY = max(Y(Y~=0));
    minZ = min(Z(Z>0));
    if isempty(minZ)
        continue
    end
    maxZ = max(Z(:));

    borders=[[minX;minY;minZ] [maxX;maxY;minZ]];
    bordersKinectImgPlane = Intrinsics *...
        cat(1,borders,ones(1,2));
    bordersKinectImgPlane = ...
        bordersKinectImgPlane./...
        repmat(bordersKinectImgPlane(4,:),4,1);
    minRowmaxZ = round(bordersKinectImgPlane(2,1));
    maxRowmaxZ = round(bordersKinectImgPlane(2,2));
    minColmaxZ = round(bordersKinectImgPlane(1,1));
    maxColmaxZ = round(bordersKinectImgPlane(1,2));
    % define projected bounding box 2D size
    bbox2D = zeros(maxRowmaxZ-minRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
    % define cropping area inside of original depth image
    upperRowInOrig = max(1,minRowmaxZ);
    lowerRowInOrig = min(nRow,maxRowmaxZ);
    leftColInOrig = max(1,minColmaxZ);
    rightColInOrig = min(nCol,maxColmaxZ);
    % calculate offset
    rowOff = upperRowInOrig-minRowmaxZ;
    colOff = leftColInOrig-minColmaxZ;
    
    %%% TODO
    % There is a big error here! we first
    % normalize pointcloud to bounding box dimensions but then... well
    % wait.. Debug anyway!
    %%%
    X = (X - minX)/abs(minX-maxX);
    Y = (Y - minY)/abs(minY-maxY);
    Z = (Z - minZ)/abs(minZ-maxZ);
    
    %%% oh shit! the error is here!!! check in Alex Synth generation
    %%% multiply with mask of course
    depth = bsxfun(@times,cat(3,X,Y,Z),mask);
    % map pointcloud crop to projected bounding box
    bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...   
        depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);
    
    % feed clipped depth image to network
    res = vl_simplenn(net, imresize(bbox2D,[120 60],'nearest'), [], [], ...
          'disableDropout', true, ...
          'conserveMemory', true, ...
          'sync', false) ;
    % scale up estimated joints to un-normalized space
    estJoints = res(end).x(1:3*nJ);
    estJoints = reshape(estJoints,3,[]);
    estJoints = bsxfun(@plus,[minX;minY;minZ],...
        bsxfun(@times,[abs(minX-maxX);abs(minY-maxY);abs(minZ-maxZ)],estJoints));
    % project skeleton onto image plane
    planeJoints = Intrinsics *...
        cat(1,estJoints,ones(1,nJ));
    planeJoints = ...
        planeJoints./...
        repmat(planeJoints(4,:),4,1);
    planeJoints = [planeJoints(1:2,:);zeros(1,nJ)];
    % plot joints
    hold on
    hSkel = drawEvalSkel(reshape(planeJoints',[],1),'*g','3d','Synth17',limbIdx,hSkel);
    hold off
    drawnow
    if makeMovie
        writeVideo(movObj, getframe)
    end

end

% close device
mxNI(1)
if makeMovie
    close(movObj)
end
