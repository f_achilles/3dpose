function Y = vl_skeloss(X,c,dzdy)
% L2-loss with skeleton constraints

%%%
% Version with NaNs in 'c'
%%%
validPts = ~isnan(c);
c_clean = zeros(size(c),'single');
c_clean(validPts) = c(validPts);

limbIdx = [ 11,2,2,1,11,11,9,16,6,13,2,2,4,4,10,17,8,15;
            3,11,1,4,9,16,6,13,7,14,9,16,10,17,8,15,5,12];
numJoints = max(limbIdx(:));

% regular L2 over all joint centers
if nargin < 3
    Y = sum(sum(sum(((X(:,:,1:3*numJoints+18,:)-c_clean(:,:,1:3*numJoints+18,:)).*single(validPts(:,:,1:3*numJoints+18,:))).^2,1),2),3);
else
    Y = dzdy * 2*(X(:,:,1:3*numJoints+18,:)-c_clean(:,:,1:3*numJoints+18,:)).*single(validPts(:,:,1:3*numJoints+18,:));
end
Y =  bsxfun(@rdivide,Y,eps+sum(sum(sum(validPts(:,:,1:3*numJoints+18,:),1),2),3));

% % calculate limb lengths and angles from the vector
% nLimbs = size(limbIdx,2);
% sz_ = [size(X,1) size(X,2) size(X,3) size(X,4)];
% limbLengths = zeros(1,1,nLimbs,sz_(4),'single');
% limbAngles = zeros(1,1,3*nLimbs,sz_(4),'single');
% for iLimb = 1:nLimbs
%     limbVec = ...
%         -X(:,:,(limbIdx(1,iLimb)-1)*3+1:limbIdx(1,iLimb)*3,:)...
%         +X(:,:,(limbIdx(2,iLimb)-1)*3+1:limbIdx(2,iLimb)*3,:);
%     limbLengths(:,:,iLimb,:) = sqrt(limbVec(:,:,1,:).^2+limbVec(:,:,2,:).^2+limbVec(:,:,3,:).^2)+eps;
%     limbAngles((iLimb-1)*3+1:iLimb*3) = bsxfun(@rdivide,limbVec,limbLengths(iLimb));
% end
% 
% % calculate length-loss
% if nargin < 3
%     L=
% else
%     L=
% end
% % calculate angle-loss
% if nargin < 3
%     A=
% else
%     A=
% end
% % calculate symmetry-loss
% if nargin < 3
%     S=
% else
%     S=
% end
% 
% % symmetry-loss
% symmIndicesLeft = 74:2:87;
% symmIndicesRight = symmIndicesLeft+1;
% validSymmLimbs = validPts(:,:,symmIndicesLeft,:) & validPts(:,:,symmIndicesRight,:);
% symDifferences = (X(:,:,symmIndicesLeft,:)-X(:,:,symmIndicesRight,:)) .* ...
%     single(validSymmLimbs);
% if nargin < 3
%     Ysymm = sum(sum(sum(symDifferences.^2,3),2),1);
% else
%     Ysymm = gpuArray(zeros(size(Y),'single'));
%     Ysymm(:,:,symmIndicesLeft,:) = 2*symDifferences;
%     Ysymm(:,:,symmIndicesRight,:) = -2*symDifferences;
% end
% Ysymm = Ysymm/nnz(validSymmLimbs);
% %add the losses/derivatives
% Y = Y+Ysymm;
end