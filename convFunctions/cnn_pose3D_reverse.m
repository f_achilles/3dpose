function [net, info] = cnn_pose3D_reverse(imdb,varargin)

opts.dataDir = fullfile('C:\Projects') ;
opts.expDir = opts.dataDir;
opts.imdbPath = fullfile(opts.expDir);
opts.train.batchSize = 1000 ;
opts.train.numEpochs = 100 ;
opts.train.continue = true ;
opts.train.gpus = 1 ;
opts.train.learningRate = 1e-1;% 1e-2*ones(1,100) 1e-3*ones(1,390)] ;
opts.train.expDir = opts.expDir ;
opts.continue = true;
opts.train.sync = true;
opts.train.numSubBatches = 1 ;
opts.train.backPropDepth = +Inf;
opts.train.conserveMemory = true;
opts.train.errorFunction = 'none' ;
opts = vl_argparse(opts,varargin);

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

% network_pose3D_120x120_medium
% network_pose3D_120x60
network_pose3D_joints_to_120x60
% network_pose3D_120x60_overparam_3xfc
% network_pose3D_120x60_overparam_3xfc_lessWeights
% network_pose3D_480x640

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

[net, info] = cnn_train(net, imdb, @getBatch, ...
    opts.train) ;
end

% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch, flipBool)
% --------------------------------------------------------------------
labels = gpuArray(imdb.images.data(:,:,:,batch)) ;
im = gpuArray(imdb.images.labels(:,:,:,batch)) ;
% labels = imdb.images.labelsOverParam(:,:,:,batch) ;
end


% --------------------------------------------------------------------
function [im, labels] = getBatchImagenet(imdb, batch, flipBool)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch);

if flipBool
    firstHalf = floor(numel(batch)/2);
    % flip the first N/2 images
    im(:,:,:,1:firstHalf) = fliplr(im(:,:,:,1:firstHalf));
    % error('first debug the flipped X image')
    im(:,:,1,1:firstHalf) = single(1)-im(:,:,1,1:firstHalf);
    im(im==1)=0;
end

% add speckle noise
% TODO

% labels = imdb.images.labels(:,:,:,batch) ;
labels = imdb.images.labelsOverParam(:,:,:,batch) ;
if flipBool
    % flip x-coordinate of the first N/2 labels
    labels(:,:,1:3:51,1:firstHalf) =  1-labels(:,:,1:3:51,1:firstHalf);
    % mirror the actual bodypart indices, such that a front-facing person is not
    % warped into a back-facing person
    % {abdomen, chest, head, hips, l,l,l,l,l,l,neck,r,r,r,r,r,r}
    % the l's and the r's are sorted equally! they correspond to elements 5:10
    % and 12:17
    temp = labels(:,:,13:30,1:firstHalf);
    labels(:,:,13:30,1:firstHalf) = labels(:,:,34:51,1:firstHalf);
    labels(:,:,34:51,1:firstHalf) = temp;
    
    % 18 parameters for shape (indices 52-69)
    
    % mirror lengths
    % 17 joints -> 18 limbs: indices 70-87
%     limbs = {...
%     'NeckHead','Thorax','Abdomen','AbdomenLow',...
%     'LCollarbone','RCollarbone','LUpperarm','RUpperarm','LForearm','RForearm',...
%     'LLat','RLat','LPsoas17','RPsoas17','LThigh','RThigh','LShin','RShin'};
    temp = labels(:,:,74:2:87,1:firstHalf);
    labels(:,:,74:2:87,1:firstHalf) = labels(:,:,75:2:87,1:firstHalf);
    labels(:,:,75:2:87,1:firstHalf) = temp;
    
    % flip the angle when they are also used for parameterization
    % only the X-axis part needs to be flipped
    % 17 joints -> 18 limbs -> 18*3=54 elements for angles: indices 88-141
    labels(:,:,88:3:141,1:firstHalf) =  -labels(:,:,88:3:141,1:firstHalf);
    % mirror the indices of the angles of pairwise bodyparts
    angleIndicesLeft = reshape(bsxfun(@plus,100:6:141,[0;1;2]),[],1);
    angleIndicesRight = reshape(bsxfun(@plus,103:6:141,[0;1;2]),[],1);
    temp = labels(:,:,angleIndicesLeft,1:firstHalf);
    labels(:,:,angleIndicesLeft,1:firstHalf) = labels(:,:,angleIndicesRight,1:firstHalf);
    labels(:,:,angleIndicesRight,1:firstHalf) = temp;
    
%     % plot pose for debug
%     limbIdx = [11,2,2,1,11,11,9,16,6,13,2,2,4,4,10,17,8,15;3,11,1,4,9,16,6,13,7,14,9,16,10,17,8,15,5,12];
%     [XYZs,joints] = plotRedundantPose(labels(:,:,:,1),limbIdx);
%     [regParams,Bfit,ErrorStats]=absor(XYZs,joints);
%     hold on
%     drawEvalSkel(reshape(Bfit',[],1),'+g','3d','Synth17',limbIdx);
%     hold off
end
end