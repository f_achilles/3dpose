function Y = vl_euclidloss(X,c,dzdy)
% sz_ = [size(X,1) size(X,2) size(X,3) size(X,4)];
%%%
% Version with NaNs in 'c'
%%%
validPts = ~isnan(c);
% c_clean = gpuArray(zeros(size(c),'single'));
c_clean = zeros(size(c),'single');
c_clean(validPts) = c(validPts);
if nargin < 3
    Y = sum(sum(sum(((X-c_clean).*single(validPts)).^2,1),2),3);
else
    Y = dzdy * 2*(X-c_clean).*single(validPts);
end
Y =  bsxfun(@rdivide,Y,eps+sum(sum(sum(validPts,1),2),3));
%%%
% Version with 3d confidence in c's last 20 elements
%%%
% 
% % scale confidence between 0.5 and 1
% conf = (c(1,1,61:80,:)+0.5)/1.5;
% if nargin < 3
%     Y = 1000*sum(sum(sum((X-c(1,1,1:(sz_(3)),:)).^2,1),2),3);
% else
%     Y = dzdy * 2*(X-c(1,1,1:(sz_(3)),:)) .* cat(3,conf,conf,conf);
% end
% Y =  Y/(sz_(1)*sz_(2)*sz_(3));
end