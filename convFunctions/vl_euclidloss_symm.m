function Y = vl_euclidloss_symm(X,c,dzdy)
% L2-loss

%%%
% Version with NaNs in 'c'
%%%
validPts = ~isnan(c);
c_clean = gpuArray(zeros(size(c),'single'));
c_clean(validPts) = c(validPts);

% sz_ = [size(X,1) size(X,2) size(X,3) size(X,4)];
if nargin < 3
    Y = sum(sum(sum(((X-c_clean).*single(validPts)).^2,1),2),3);
else
    Y = dzdy * 2*(X-c_clean).*single(validPts);
end
Y =  bsxfun(@rdivide,Y,eps+sum(sum(sum(validPts,1),2),3));

%symmetry-loss
symmIndicesLeft = 74:2:87;
symmIndicesRight = symmIndicesLeft+1;
validSymmLimbs = validPts(:,:,symmIndicesLeft,:) & validPts(:,:,symmIndicesRight,:);
symDifferences = (X(:,:,symmIndicesLeft,:)-X(:,:,symmIndicesRight,:)) .* ...
    single(validSymmLimbs);
if nargin < 3
    Ysymm = sum(sum(sum(symDifferences.^2,3),2),1);
else
    Ysymm = gpuArray(zeros(size(Y),'single'));
    Ysymm(:,:,symmIndicesLeft,:) = 2*symDifferences;
    Ysymm(:,:,symmIndicesRight,:) = -2*symDifferences;
end
Ysymm = Ysymm/nnz(validSymmLimbs);
%add the losses/derivatives
Y = Y+Ysymm;
end