function val = testReadIn()
val = 0;
% samples from June 8th
dbpath  = 'C:\Projects\PoseEstimation\data\AlexData_samples_08_06\samples_model_0_and_5';
folders = dir(dbpath);
goodFolders = cellfun(@(x)~isempty(regexp(x,'angle_\d\d\d$','once')),{folders.name});
folders = folders(goodFolders);

% sequences = dir([dbpath filesep '*' filesep '*.bin']);

% normal training
cropH = 120;
cropW = 60;
% comparison to imageNet
% cropH = 224;
% cropW = 224;

ratio = cropH/cropW;
nRow = 480;
nCol = 640;
sampleIdx = 0;
offs = 100; %[mm] %0.1; %[m]
sInd = 1;
staticBboxEdgLen = 2;

numJoints = 18;

% standard intrinsics
cx = 320;
cy = 240;
fx = 525;
fy = 525;
%
Intrinsics = [ fx 0 cx  0 ;
               0 fy cy  0 ;
               0  0  1  0 ;
               0  0  1  0 ];
%
[Xinds,Yinds] = meshgrid(1:nCol,1:nRow);

bboxMode = 'ratio'; % 'ratio', 'constant'

for iView = 1:nnz(goodFolders)
    % each folder represents one camera view
    viewPath = [dbpath filesep folders(iView).name];
    sequences = dir(viewPath);
    for iSeq = 1:numel(sequences)
        seqPath = [viewPath filesep sequences(iSeq).name];
        models = dir(seqPath);
        for iMod = 1:numel(models)
            modPath = [seqPath filesep models(iMod).name];
            jointPosFiles = dir([modPath filesep '*.txt']);
            frames = dir([modPath filesep '*.png']);

            % figure('Position',[1 41 1600 1083])
            for iFrame = 2:numel(frames) %start from frame 2 (exclude T-pose)
                DepthFilename = [modPath filesep frames(iFrame).name];
                Z = single(imread(DepthFilename));
                X = Z .* (Xinds-cx) / fx;
                Y = Z .* (Yinds-cy) / fy;
                FG = Z~=0;
            %     [DepthFrame,Intrinsics,Extrinsics] = loadDepthFrameFromFile(DepthFilename);
            %     [X,Y,Z,FG] = getKinect3DPointCloud(DepthFrame,Intrinsics,Extrinsics);
            %     [jointsPos,PositionsKinectImgPlane] = projectJointsMocapToKinect(JointPositions(iFrame,:,:),Intrinsics,Extrinsics);
                jointPosFilename = [modPath filesep jointPosFiles(iFrame).name];
                jointsPos = readSynthJoints(jointPosFilename,numJoints);

                if any(FG(:))
                switch bboxMode
                    case 'ratio'
                    % constant ratio bbox
                    minX = min(jointsPos(1,:))-offs;
                    maxX = max(jointsPos(1,:))+offs;
                    minY = min(jointsPos(2,:))-offs;
                    maxY = max(jointsPos(2,:))+offs;
                    deltaX = maxX-minX;
                    deltaY = maxY-minY;

                    XZYratio = deltaY/deltaX;
                    % adjust aspect ratio
                    if XZYratio<ratio %deltaY too short
                        increase = ratio*deltaX-deltaY;
                        minY = minY-increase/2;
                        maxY = maxY+increase/2;
                    elseif XZYratio>ratio %deltaX too short
                        increase = deltaY/ratio-deltaX;
                        minX = minX-increase/2;
                        maxX = maxX+increase/2;
                    else
                        % nothing
                    end
                    minZ = min(jointsPos(3,:))-offs;
                    maxZ = max(jointsPos(3,:))+offs;
                    case 'static'
                    % constant measurements bbox
                    centerX = (max(jointsPos(1,:))+min(jointsPos(1,:)))/2;
                    centerY = (max(jointsPos(2,:))+min(jointsPos(2,:)))/2;
                    centerZ = (max(jointsPos(3,:))+min(jointsPos(3,:)))/2;
                    minX = centerX - staticBboxEdgLen/2;
                    maxX = centerX + staticBboxEdgLen/2;
                    minY = centerY - staticBboxEdgLen/2;
                    maxY = centerY + staticBboxEdgLen/2;
                    minZ = centerZ - staticBboxEdgLen/2;
                    maxZ = centerZ + staticBboxEdgLen/2;
                    case 'dynamic'
                    % dynamic bbox tightly fitted to subject
                    minX = min(jointsPos(1,:))-offs;
                    maxX = max(jointsPos(1,:))+offs;
                    minY = min(jointsPos(2,:))-offs;
                    maxY = max(jointsPos(2,:))+offs;
                    minZ = min(jointsPos(3,:))-offs;
                    maxZ = max(jointsPos(3,:))+offs;
                end
            %     X = Z * (Xind-cx)/fx;
            % Xind = X*fx/Z + cx = (X*fx + Z*cx)/Z;
                    borders=[[minX;minY;minZ] [maxX;maxY;minZ]];
            %         borders(2,:) = -borders(2,:);
                    bordersKinectImgPlane = Intrinsics *...
                        cat(1,borders,ones(1,2));
                    bordersKinectImgPlane = ...
                        bordersKinectImgPlane./...
                        repmat(bordersKinectImgPlane(4,:),4,1);
                    minRowmaxZ = round(bordersKinectImgPlane(2,1));
                    maxRowmaxZ = round(bordersKinectImgPlane(2,2));
                    minColmaxZ = round(bordersKinectImgPlane(1,1));
                    maxColmaxZ = round(bordersKinectImgPlane(1,2));
                    % define bounding box size
                    bbox2D = zeros(maxRowmaxZ-minRowmaxZ+1,maxColmaxZ-minColmaxZ+1,3,'single');
                    % define cropping area inside of original depth image
                    upperRowInOrig = max(1,minRowmaxZ);
                    lowerRowInOrig = min(nRow,maxRowmaxZ);
                    leftColInOrig = max(1,minColmaxZ);
                    rightColInOrig = min(nCol,maxColmaxZ);
                    % calculate offset
                    rowOff = upperRowInOrig-minRowmaxZ;
                    colOff = leftColInOrig-minColmaxZ;
                    % map original image crop to bounding box image
                    X = (X - minX)/abs(minX-maxX);
                    Y = (Y - minY)/abs(minY-maxY);
                    Z = (Z - minZ)/abs(minZ-maxZ);

                    if strcmp(bboxMode,'dynamic')
                    depth = (cat(3,X,Y,Z)*255-255/2).*repmat(FG,1,1,3);
                    else
                    depth = cat(3,X,Y,Z).*repmat(FG,1,1,3);
                    end

                    bbox2D((1:(lowerRowInOrig-upperRowInOrig+1))+rowOff,(1:(rightColInOrig-leftColInOrig+1))+colOff,:) = ...   
                        depth(upperRowInOrig:lowerRowInOrig,leftColInOrig:rightColInOrig,:);

                    imdb.images.data(:,:,:,sInd) = single(imresize(bbox2D,[cropH cropW]));

                    % incorporate bounding box into label and store both in IMDB
                    jointsPos(1,:) = (jointsPos(1,:) - minX)/abs(minX-maxX);
                    jointsPos(2,:) = (jointsPos(2,:) - minY)/abs(minY-maxY);
                    jointsPos(3,:) = (jointsPos(3,:) - minZ)/abs(minZ-maxZ);
                    imdb.images.labels(:,:,1:3*numJoints,sInd)     = single(reshape(jointsPos,1, 1, [], 1));
                    imdb.meta.bbox(sInd,:) = [minX maxX minY maxY minZ maxZ];
                    imdb.meta.sequenceID{sInd} = [folders(iView).name '_' sequences(iSeq).name '_' models(iMod).name];
                    % train/test split based on sequence number
                    imdb.images.set(sInd) = 1+uint8(iSeq>21);

                    fprintf('Sequence %s, frame %d/%d.\n',imdb.meta.sequenceID{sInd},iFrame,numel(frames))
                    sInd = sInd+1;

            %         plot3( jointsPos(1,:), jointsPos(2,:), jointsPos(3,:),'ro');
            %         patch('xdata', X(FG), 'ydata',Y(FG),'zdata', Z(FG), ...
            %             'linestyle', 'none', ...
            %             'marker', '.', ...
            %             'FaceColor', 'none',...
            %             'EdgeColor', 'flat',...
            %             'cdata', -Z(FG),...
            %             'markeredgecolor', 'auto');
            %         axis image vis3d
            %         drawnow
                end

            % subplot(1,2,1)

            % subplot(1,2,2)    
            %     imagesc(Z); axis image
            %     hold on
            %     plot(PositionsKinectImgPlane(1,:),PositionsKinectImgPlane(2,:),'r*')
            %     hold off
            %     pause(0.02)
            end %frames
        end %actors
    end %sequences
    disp(['Finished processing view ' folders(iView).name]);
end %views
end